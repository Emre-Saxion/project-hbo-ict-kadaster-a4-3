//package com.kadaster.landmeter.server.swagger;
//
//import com.kadaster.landmeter.server.config.SpringConfig;
//import com.kadaster.landmeter.server.repo.EmployeeRepository;
//import com.kadaster.landmeter.server.service.appointment.AppointmentService;
//import com.kadaster.landmeter.server.service.auth.AuthenticationService;
//import com.kadaster.landmeter.server.service.download.DownloadService;
//import com.kadaster.landmeter.server.service.employee.EmployeeService;
//import com.kadaster.landmeter.server.service.upload.UploadService;
//import org.apache.commons.io.IOUtils;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.context.annotation.Import;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import java.io.File;
//import java.io.FileWriter;
//
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@WebMvcTest
//@AutoConfigureMockMvc
//@Import(SpringConfig.class)
//@RunWith(SpringJUnit4ClassRunner.class)
//@AutoConfigureTestDatabase
//public class SwaggerAPIIntegrationTest {
//    @Autowired
//    private MockMvc mockMvc;
//
//    @MockBean
//    private EmployeeService employeeService;
//
//    @MockBean
//    private AuthenticationService authenticationService;
//
//    @MockBean
//    private AppointmentService appointmentService;
//
//    @MockBean
//    private DownloadService downloadService;
//
//    @MockBean
//    private UploadService uploadService;
//
//    @MockBean
//    private EmployeeRepository employeeRepository;
//
//    @Test
//    public void swaggerJsonExists() throws Exception {
//        var contentAsString = mockMvc
//                .perform(MockMvcRequestBuilders.get("/v2/api-docs")
//                        .accept(MediaType.APPLICATION_JSON)
//                        .param("group", "kadaster-landmeters-api"))
//                .andExpect(status().isOk())
//                .andReturn()
//                .getResponse().getContentAsString();
//        try (var writer = new FileWriter(new File("target/generated-sources/swagger.json"))) {
//            IOUtils.write(contentAsString, writer);
//        }
//    }
//}

package com.kadaster.landmeter.server.controller;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.kadaster.landmeter.server.dto.request.CreateEmployeeRequest;
import com.kadaster.landmeter.server.dto.request.LoginEmployeeRequest;
import com.kadaster.landmeter.server.model.appointment.*;
import com.kadaster.landmeter.server.service.appointment.AppointmentService;
import com.kadaster.landmeter.server.service.employee.EmployeeService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureTestDatabase
public class AdminControllerTest {

    private final MockMvc mockMvc;

    private final EmployeeService employeeService;

    private final AppointmentService appointmentService;

    private final CreateEmployeeRequest createEmployeeRequestAdmin = new CreateEmployeeRequest()
            .setFirstName("Test")
            .setLastName("Last")
            .setEmail("test@email.com")
            .setPhoneNumber("0622222222")
            .setLoginCode("510099")
            .setPassword("password")
            .setAdmin(true);

    private final CreateEmployeeRequest createEmployeeRequestUser = new CreateEmployeeRequest()
            .setFirstName("Test")
            .setLastName("Last")
            .setEmail("test@email.com")
            .setPhoneNumber("0622222222")
            .setLoginCode("510110")
            .setPassword("password")
            .setAdmin(false);

    @Autowired
    public AdminControllerTest(MockMvc mockMvc, EmployeeService employeeService, AppointmentService appointmentService) {
        this.mockMvc = mockMvc;
        this.employeeService = employeeService;
        this.appointmentService = appointmentService;
    }

    private String adminJWTToken;

    private String nonAdminJWTToken;

    @BeforeAll
    void beforeAll() throws Exception {
        employeeService.saveEmployee(createEmployeeRequestAdmin);

        var address2 = new Address().setCountry("Nederland").setMunicipality("Overijssel").setCity("Almelo").setStreet("Teststreet").setZipCode("7000LZ").setNumber("21a");
        appointmentService.saveAppointment(new Appointment()
                .setParcel("LDN03H7497")
                .setMapFile("map.pdf")
                .setCreatedDate(LocalDateTime.now())
                .setAppointmentDate(LocalDateTime.now().plus(3, ChronoUnit.HOURS))
                .setAddress(address2)
                .setMeasurements("")
                .setDeed(new Deed().setPartNumber(20005).setDeedNumber("00888"))
                .setFiles(List.of(
                        new AppointmentFile().setFileName("foto20").setFileExtension("png"),
                        new AppointmentFile().setFileName("foto10").setFileExtension("png"),
                        new AppointmentFile().setFileName("pdf").setFileExtension("pdf")
                ))
                .setAddedFiles(List.of())
                .setStakeholders(List.of(
                        new Stakeholder().setName("Test Stakeholder").setAddress(address2.setStreet("SecondTestStreet")).setEmail("stakeholder@email.com").setPhoneNumber("0611223344"),
                        new Stakeholder().setName("Test Stakeholder 2").setAddress(address2.setStreet("SecondTestStreet")).setEmail("stakeholder@email.com").setPhoneNumber("0611223344")
                ))
        );

        var loginEmployeeRequest = new LoginEmployeeRequest()
                .setLoginCode(createEmployeeRequestAdmin.getLoginCode())
                .setPassword(createEmployeeRequestAdmin.getPassword());

        var gson = new Gson();
        var json = gson.toJson(loginEmployeeRequest);

        var mvcResult = mockMvc.perform(post("/employee/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andReturn();

        var jsonObject = JsonParser.parseString(mvcResult.getResponse().getContentAsString()).getAsJsonObject();

        adminJWTToken = jsonObject.get("token").getAsString();
    }

    @Test
    @Order(1)
    void checkNotNull() {
        assertThat(mockMvc).isNotNull();
        assertThat(employeeService).isNotNull();
    }

    @Test
    @Order(2)
    void testGetAllEmployees() throws Exception {
        var mvcResult = mockMvc.perform(get("/admin/employees/all")
                .header("Authorization", adminJWTToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        var jsonArray = JsonParser.parseString(mvcResult.getResponse().getContentAsString()).getAsJsonArray();

        assertThat(jsonArray.size()).isGreaterThan(1);
    }

    @Test
    @Order(3)
    void testCreateEmployeeNoJWTToken() throws Exception {
        var createEmployeeRequestAdmin = new CreateEmployeeRequest()
                .setFirstName("Test")
                .setLastName("Last")
                .setEmail("test@email.com")
                .setPhoneNumber("0622222222")
                .setLoginCode("888888")
                .setPassword("password")
                .setAdmin(true);

        var gson = new Gson();
        var json = gson.toJson(createEmployeeRequestAdmin);

        mockMvc.perform(post("/admin/employee/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Order(4)
    void testCreateEmployeeWrongJWTToken() throws Exception {
        var createEmployeeRequestAdmin = new CreateEmployeeRequest()
                .setFirstName("Test")
                .setLastName("Last")
                .setEmail("test@email.com")
                .setPhoneNumber("0622222222")
                .setLoginCode("510007")
                .setPassword("password")
                .setAdmin(true);

        var gson = new Gson();
        var json = gson.toJson(createEmployeeRequestAdmin);

        mockMvc.perform(post("/admin/employee/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .header("Authorization", "false"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Order(5)
    void testCreateEmployeeEmptyJWTToken() throws Exception {
        var createEmployeeRequestAdmin = new CreateEmployeeRequest()
                .setFirstName("Test")
                .setLastName("Last")
                .setEmail("test@email.com")
                .setPhoneNumber("0622222222")
                .setLoginCode("510007")
                .setPassword("password")
                .setAdmin(true);

        var gson = new Gson();
        var json = gson.toJson(createEmployeeRequestAdmin);

        mockMvc.perform(post("/admin/employee/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .header("Authorization", ""))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Order(6)
    void testCreateEmployeeCorrectJWTToken() throws Exception {
        var createEmployeeRequestAdmin = new CreateEmployeeRequest()
                .setFirstName("Test")
                .setLastName("Last")
                .setEmail("test@email.com")
                .setPhoneNumber("0622222222")
                .setLoginCode("555555")
                .setPassword("password")
                .setAdmin(true);

        var gson = new Gson();
        var json = gson.toJson(createEmployeeRequestAdmin);

        mockMvc.perform(post("/admin/employee/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .header("Authorization", adminJWTToken))
                .andExpect(status().isCreated());
    }

    @Test
    @Order(7)
    void testCreateEmployeeNoAdminJWTToken() throws Exception {
        employeeService.saveEmployee(createEmployeeRequestUser);

        var loginEmployeeRequest = new LoginEmployeeRequest()
                .setLoginCode(createEmployeeRequestUser.getLoginCode())
                .setPassword(createEmployeeRequestUser.getPassword());

        var gson = new Gson();
        var json = gson.toJson(loginEmployeeRequest);

        var mvcResult = mockMvc.perform(post("/employee/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andReturn();

        var jsonObject = JsonParser.parseString(mvcResult.getResponse().getContentAsString()).getAsJsonObject();

        var token = jsonObject.get("token").getAsString();

        nonAdminJWTToken = token;
        var createEmployeeRequestAdmin = new CreateEmployeeRequest()
                .setFirstName("Test")
                .setLastName("Last")
                .setEmail("test@email.com")
                .setPhoneNumber("0622222222")
                .setLoginCode("435344")
                .setPassword("password")
                .setAdmin(true);

        var employeeRequestJson = gson.toJson(createEmployeeRequestAdmin);

        mockMvc.perform(post("/admin/employee/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(employeeRequestJson)
                .header("Authorization", token))
                .andExpect(status().isForbidden());
    }

    @Test
    @Order(8)
    void testGetAllAppointmentsUnplanned() throws Exception {
        var mvcResult = mockMvc.perform(get("/admin/appointments/unplanned")
                .header("Authorization", adminJWTToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        var jsonArray = JsonParser.parseString(mvcResult.getResponse().getContentAsString()).getAsJsonArray();
        assertThat(jsonArray.size()).isGreaterThanOrEqualTo(1);
    }

    @Test
    @Order(9)
    void testGetAllEmployeesNonAdmin() throws Exception {
        mockMvc.perform(get("/admin/employees/all")
                .header("Authorization", nonAdminJWTToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @Order(10)
    void testGetAllAppointmentsUnplannedNonAdmin() throws Exception {
        mockMvc.perform(get("/admin/appointments/unplanned")
                .header("Authorization", nonAdminJWTToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @Order(11)
    void testGetAllAppointmentsByParcel() throws Exception {
        var mvcResult = mockMvc.perform(get("/admin/appointments/?parcel=LD")
                .header("Authorization", adminJWTToken)
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        var jsonArray = JsonParser.parseString(mvcResult.getResponse().getContentAsString()).getAsJsonArray();
        assertThat(jsonArray.size()).isGreaterThanOrEqualTo(1);
    }
}

package com.kadaster.landmeter.server.service;

import com.kadaster.landmeter.server.dto.request.CreateEmployeeRequest;
import com.kadaster.landmeter.server.dto.request.UpdateAppointmentRequest;
import com.kadaster.landmeter.server.exception.AppointmentNotFoundException;
import com.kadaster.landmeter.server.exception.EmployeeNotFoundException;
import com.kadaster.landmeter.server.exception.NotAllowedException;
import com.kadaster.landmeter.server.model.appointment.*;
import com.kadaster.landmeter.server.service.appointment.AppointmentService;
import com.kadaster.landmeter.server.service.auth.AuthenticationService;
import com.kadaster.landmeter.server.service.employee.EmployeeService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@AutoConfigureTestDatabase
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AppointmentServiceTest {

    private final AppointmentService appointmentService;

    private final EmployeeService employeeService;

    private final AuthenticationService authenticationService;

    @Autowired
    public AppointmentServiceTest(AppointmentService appointmentService, EmployeeService employeeService, AuthenticationService authenticationService) {
        this.appointmentService = appointmentService;
        this.employeeService = employeeService;
        this.authenticationService = authenticationService;
    }

    private String adminJWTToken = "";

    @BeforeAll
    void beforeAll() throws EmployeeNotFoundException {
        employeeService.saveEmployee(new CreateEmployeeRequest()
                .setFirstName("Test")
                .setLastName("Last")
                .setEmail("test@email.com")
                .setPhoneNumber("0622222222")
                .setLoginCode("444444")
                .setPassword("password")
                .setAdmin(true));

        employeeService.saveEmployee(new CreateEmployeeRequest()
                .setFirstName("Test")
                .setLastName("Last")
                .setEmail("test@email.com")
                .setPhoneNumber("0622222222")
                .setLoginCode("444445")
                .setPassword("password")
                .setAdmin(true));

        adminJWTToken = authenticationService.createToken(employeeService.getByLoginCodeChecked("444444"));

        var address = new Address().setCountry("Nederland").setMunicipality("Overijssel").setCity("Almelo").setStreet("Teststreet").setZipCode("7000LZ").setNumber("21a");
        appointmentService.saveAppointment(new Appointment()
                .setParcel("LDN03H7497")
                .setMapFile("map.pdf")
                .setCreatedDate(LocalDateTime.now())
                .setAppointmentDate(LocalDateTime.now().plus(3, ChronoUnit.HOURS))
                .setAddress(address)
                .setMeasurements("")
                .setDeed(new Deed().setPartNumber(20005).setDeedNumber("00888"))
                .setEmployee(employeeService.getByLoginCodeChecked("444444"))
                .setFiles(List.of(
                        new AppointmentFile().setFileName("foto20").setFileExtension("png"),
                        new AppointmentFile().setFileName("foto10").setFileExtension("png"),
                        new AppointmentFile().setFileName("pdf").setFileExtension("pdf")
                ))
                .setAddedFiles(List.of())
                .setStakeholders(List.of(
                        new Stakeholder().setName("Test Stakeholder").setAddress(address.setStreet("SecondTestStreet")).setEmail("stakeholder@email.com").setPhoneNumber("0611223344"),
                        new Stakeholder().setName("Test Stakeholder 2").setAddress(address.setStreet("SecondTestStreet")).setEmail("stakeholder@email.com").setPhoneNumber("0611223344")
                ))
        );

        var address2 = new Address().setCountry("Nederland").setMunicipality("Overijssel").setCity("Almelo").setStreet("Teststreet").setZipCode("7000LZ").setNumber("21a");
        appointmentService.saveAppointment(new Appointment()
                .setParcel("LDN03H7497")
                .setMapFile("map.pdf")
                .setCreatedDate(LocalDateTime.now())
                .setAppointmentDate(LocalDateTime.now().plus(3, ChronoUnit.HOURS))
                .setAddress(address2)
                .setMeasurements("")
                .setDeed(new Deed().setPartNumber(20005).setDeedNumber("00888"))
                .setFiles(List.of(
                        new AppointmentFile().setFileName("foto20").setFileExtension("png"),
                        new AppointmentFile().setFileName("foto10").setFileExtension("png"),
                        new AppointmentFile().setFileName("pdf").setFileExtension("pdf")
                ))
                .setAddedFiles(List.of())
                .setStakeholders(List.of(
                        new Stakeholder().setName("Test Stakeholder").setAddress(address2.setStreet("SecondTestStreet")).setEmail("stakeholder@email.com").setPhoneNumber("0611223344"),
                        new Stakeholder().setName("Test Stakeholder 2").setAddress(address2.setStreet("SecondTestStreet")).setEmail("stakeholder@email.com").setPhoneNumber("0611223344")
                ))
        );

    }

    @Test
    @Order(1)
    void checkNotNull() {
        assertThat(appointmentService).isNotNull();
    }

    @Test
    @Order(2)
    void testGetAppointmentsByLoginCode() throws AppointmentNotFoundException {
        var appointments = appointmentService.getAppointsByEmployeeLoginCode("444444");

        assertThat(appointments.size()).isGreaterThanOrEqualTo(1);
    }

    @Test
    @Order(3)
    void testGetAppointmentsByLoginCodeIdNonFound() {
        assertThrows(AppointmentNotFoundException.class, () -> appointmentService.getAppointsByEmployeeLoginCode("444445"));
    }

    @Test
    @Order(4)
    void testGetAppointmentsById() throws AppointmentNotFoundException, EmployeeNotFoundException {
        var employee = employeeService.getByLoginCodeChecked("444444");
        var appointments = appointmentService.getAppointsByEmployeeId(employee.getId());

        assertThat(appointments.size()).isGreaterThanOrEqualTo(1);
    }

    @Test
    @Order(5)
    void testUpdateAppointment() throws AppointmentNotFoundException {
        var updateAppointmentRequest = new UpdateAppointmentRequest()
                .setNote("Testing")
                .setStakeholderPresent(List.of())
                .setAuthorizers(List.of())
                .setMeasurements("")
                .setAddedFiles(List.of())
                .setUnanimous(false)
                .setCompatible(false);

        var appointment = appointmentService.getAppointsByEmployeeLoginCode("444444").get(0);
        appointmentService.updateAppointment(appointment.getId(), updateAppointmentRequest);

        var newAppointment = appointmentService.getAppointsByEmployeeLoginCode("444444").get(0);
        assertThat(newAppointment.getNote()).isEqualTo("Testing");
    }

    @Test
    @Order(6)
    void testGetAppointmentsUnplanned() throws AppointmentNotFoundException, NotAllowedException {
        var appointments = appointmentService.getAppointmentsNoEmployee(adminJWTToken);
        assertThat(appointments.size()).isGreaterThanOrEqualTo(1);
    }

    @Test
    @Order(7)
    void testGetAppointmentsByParcel() throws NotAllowedException, AppointmentNotFoundException {
        var appointments = appointmentService.getAppointmentsByParcel("ld", adminJWTToken);
        assertThat(appointments.size()).isGreaterThanOrEqualTo(1);
    }
}

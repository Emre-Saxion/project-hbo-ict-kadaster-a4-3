package com.kadaster.landmeter.server.controller;

import com.google.gson.JsonParser;
import com.kadaster.landmeter.server.dto.request.CreateEmployeeRequest;
import com.kadaster.landmeter.server.exception.EmployeeNotFoundException;
import com.kadaster.landmeter.server.model.appointment.*;
import com.kadaster.landmeter.server.service.appointment.AppointmentService;
import com.kadaster.landmeter.server.service.auth.AuthenticationService;
import com.kadaster.landmeter.server.service.employee.EmployeeService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureTestDatabase
public class AppointmentControllerTest {

    private final MockMvc mockMvc;

    private final AppointmentService appointmentService;

    private final AuthenticationService authenticationService;

    private final EmployeeService employeeService;

    @Autowired
    public AppointmentControllerTest(MockMvc mockMvc, AppointmentService appointmentService, AuthenticationService authenticationService, EmployeeService employeeService) {
        this.mockMvc = mockMvc;
        this.appointmentService = appointmentService;
        this.authenticationService = authenticationService;
        this.employeeService = employeeService;
    }

    private String jwtToken = "";

    private String jwtTokenNoAppointments = "";

    @BeforeAll
    void beforeAll() throws EmployeeNotFoundException {
        employeeService.saveEmployee(new CreateEmployeeRequest()
                .setFirstName("Test")
                .setLastName("Last")
                .setEmail("test@email.com")
                .setPhoneNumber("0622222222")
                .setLoginCode("321222")
                .setPassword("password")
                .setAdmin(false));

        employeeService.saveEmployee(new CreateEmployeeRequest()
                .setFirstName("Test")
                .setLastName("Last")
                .setEmail("test@email.com")
                .setPhoneNumber("0622222222")
                .setLoginCode("321223")
                .setPassword("password")
                .setAdmin(false));

        var employee = employeeService.getByLoginCodeChecked("321222");

        var address = new Address().setCountry("Nederland").setMunicipality("Overijssel").setCity("Almelo").setStreet("Teststreet").setZipCode("7000LZ").setNumber("21a");
        appointmentService.saveAppointment(new Appointment()
                .setParcel("LDN03H7497")
                .setMapFile("map.pdf")
                .setCreatedDate(LocalDateTime.now())
                .setAppointmentDate(LocalDateTime.now().plus(3, ChronoUnit.HOURS))
                .setAddress(address)
                .setMeasurements("")
                .setDeed(new Deed().setPartNumber(20005).setDeedNumber("00888"))
                .setEmployee(employee)
                .setFiles(List.of(
                        new AppointmentFile().setFileName("foto20").setFileExtension("png"),
                        new AppointmentFile().setFileName("foto10").setFileExtension("png"),
                        new AppointmentFile().setFileName("pdf").setFileExtension("pdf")
                ))
                .setAddedFiles(List.of())
                .setStakeholders(List.of(
                        new Stakeholder().setName("Test Stakeholder").setAddress(address.setStreet("SecondTestStreet")).setEmail("stakeholder@email.com").setPhoneNumber("0611223344"),
                        new Stakeholder().setName("Test Stakeholder 2").setAddress(address.setStreet("SecondTestStreet")).setEmail("stakeholder@email.com").setPhoneNumber("0611223344")
                ))
        );

        jwtToken = authenticationService.createToken(employee);

        var noAppointmentsEmployee = employeeService.getByLoginCodeChecked("321223");
        jwtTokenNoAppointments = authenticationService.createToken(noAppointmentsEmployee);
    }

    @Test
    @Order(1)
    void checkNotNull() {
        assertThat(mockMvc).isNotNull();
        assertThat(appointmentService).isNotNull();
        assertThat(authenticationService).isNotNull();
        assertThat(employeeService).isNotNull();
    }

    @Test
    @Order(2)
    void testAllAppointments() throws Exception {
        var mvcResult = mockMvc.perform(get("/appointment/all")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", jwtToken))
                .andReturn();

        var jsonArray = JsonParser.parseString(mvcResult.getResponse().getContentAsString()).getAsJsonArray();

        assertThat(jsonArray.size()).isGreaterThanOrEqualTo(1);
    }

    @Test
    @Order(3)
    void testAllAppointmentsNonFound() throws Exception {
        mockMvc.perform(get("/appointment/all")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", jwtTokenNoAppointments))
                .andExpect(status().isNotFound());
    }

}

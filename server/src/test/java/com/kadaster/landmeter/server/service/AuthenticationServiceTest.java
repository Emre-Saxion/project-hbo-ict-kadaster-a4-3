package com.kadaster.landmeter.server.service;

import com.kadaster.landmeter.server.exception.EmployeeNotFoundException;
import com.kadaster.landmeter.server.exception.InvalidCredentialsException;
import com.kadaster.landmeter.server.model.Employee;
import com.kadaster.landmeter.server.repo.EmployeeRepository;
import com.kadaster.landmeter.server.service.auth.AuthenticationService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.parameters.P;
import org.springframework.security.crypto.bcrypt.BCrypt;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@AutoConfigureTestDatabase
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AuthenticationServiceTest {

    private final AuthenticationService authenticationService;

    private final EmployeeRepository employeeRepository;

    private final Employee adminEmployee = new Employee()
            .setFirstName("Test")
            .setLastName("Last")
            .setEmail("test@email.com")
            .setPhoneNumber("0622222222")
            .setLoginCode("510001")
            .setPasswordHash("password")
            .setAdmin(true);

    @Autowired
    public AuthenticationServiceTest(AuthenticationService authenticationService, EmployeeRepository employeeRepository) {
        this.authenticationService = authenticationService;
        this.employeeRepository = employeeRepository;
    }

    private String token;

    @BeforeAll
    void beforeAll() {
        var salt = BCrypt.gensalt();
        var hashed = BCrypt.hashpw(adminEmployee.getPasswordHash(), salt);
        adminEmployee.setPasswordHash(hashed);

        employeeRepository.save(adminEmployee);
        token = authenticationService.createToken(adminEmployee);
    }

    @Test
    @Order(1)
    void testAuthenticateJWTToken() throws EmployeeNotFoundException {
        Employee employee = authenticationService.getEmployeeFromToken(token);
        assertThat(employee.getFirstName()).isEqualTo("Test");
    }

    @Test
    @Order(2)
    void testIsAdmin() {
        assertThat(authenticationService.isAdmin(token)).isTrue();
    }

    @Test
    @Order(3)
    void testWrongCredentials() {
        assertThrows(InvalidCredentialsException.class, () -> authenticationService.authenticate("510001", "wrong"));
    }

    @Test
    @Order(4)
    void testNonExistingEmployee() {
        assertThrows(EmployeeNotFoundException.class, () -> authenticationService.authenticate("123453", adminEmployee.getPasswordHash()));
    }

    @Test
    @Order(5)
    void testNullJWTToken() {
        assertThrows(NullPointerException.class, () -> authenticationService.getEmployeeFromToken(null));
    }

    @Test
    @Order(6)
    void testIdFromToken() {
        assertThat(authenticationService.getEmployeeIdFromToken(token)).isGreaterThanOrEqualTo(1L);
    }
}

package com.kadaster.landmeter.server.service;

import com.kadaster.landmeter.server.exception.FileNotFoundException;
import com.kadaster.landmeter.server.service.download.DownloadService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureTestDatabase
public class DownloadServiceTest {

    private final DownloadService downloadService;

    @Autowired
    public DownloadServiceTest(DownloadService downloadService) {
        this.downloadService = downloadService;
    }

    private final Path directory = Path.of("files/" + Long.MAX_VALUE);

    private final Path file = directory.resolve("test.txt");

    private final Path profileDirectory = Path.of("files/employees/123456789");

    private final Path profileFile = profileDirectory.resolve("picture.png");

    @BeforeAll
    void beforeAll() throws IOException {
        Files.createDirectory(directory);
        Files.createFile(file);

        Files.createDirectory(profileDirectory);
        Files.createFile(profileFile);
    }

    @AfterAll
    void afterAll() throws IOException {
        Files.delete(file);
        Files.delete(directory);

        Files.delete(profileFile);
        Files.delete(profileDirectory);
    }

    @Test
    @Order(1)
    void testGetFilePath() throws FileNotFoundException {
        var result = downloadService.getFilePath(Long.MAX_VALUE, "test.txt");

        assertThat(result).isEqualTo(file.toString());
    }

    @Test
    @Order(2)
    void testGetFileFileNotExists() {
        assertThrows(FileNotFoundException.class, () -> downloadService.getFilePath(Long.MAX_VALUE - 1, "test.txt"));
    }

    @Test
    @Order(3)
    void testGetProfilePath() throws FileNotFoundException {
        var result = downloadService.getProfileFilePath("123456789");

        assertThat(result).isEqualTo(profileFile.toString());
    }

    @Test
    @Order(4)
    void testGetProfilePathFileNotExists() {
        assertThrows(FileNotFoundException.class, () -> downloadService.getProfileFilePath("ewirwerw"));
    }
}

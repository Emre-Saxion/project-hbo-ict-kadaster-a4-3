package com.kadaster.landmeter.server.service;

import com.auth0.jwt.exceptions.JWTDecodeException;
import com.kadaster.landmeter.server.dto.request.CreateEmployeeRequest;
import com.kadaster.landmeter.server.exception.DuplicateEmployeeCodeException;
import com.kadaster.landmeter.server.exception.EmployeeNotFoundException;
import com.kadaster.landmeter.server.exception.NotAllowedException;
import com.kadaster.landmeter.server.service.auth.AuthenticationService;
import com.kadaster.landmeter.server.service.employee.EmployeeService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureTestDatabase
public class EmployeeServiceTest {

    private final EmployeeService employeeService;

    private final AuthenticationService authenticationService;

    private final CreateEmployeeRequest createEmployeeRequestEmployee = new CreateEmployeeRequest()
            .setFirstName("Test")
            .setLastName("Last")
            .setEmail("test@email.com")
            .setPhoneNumber("0622222222")
            .setLoginCode("512222")
            .setPassword("password")
            .setAdmin(false);

    private final CreateEmployeeRequest createEmployeeRequestAdmin = new CreateEmployeeRequest()
            .setFirstName("Test")
            .setLastName("Last")
            .setEmail("test@email.com")
            .setPhoneNumber("0622222222")
            .setLoginCode("510000")
            .setPassword("password")
            .setAdmin(true);

    @Autowired
    public EmployeeServiceTest(EmployeeService employeeService, AuthenticationService authenticationService) {
        this.employeeService = employeeService;
        this.authenticationService = authenticationService;
    }

    private String adminJWTToken = "";


    @BeforeAll
    void beforeAll() {
        var employee = employeeService.saveEmployee(createEmployeeRequestAdmin);
        adminJWTToken = authenticationService.createToken(employee);
    }

    @Test
    @Order(1)
    void testCreateEmployee() throws DuplicateEmployeeCodeException, NotAllowedException {
        employeeService.createEmployee(createEmployeeRequestEmployee, adminJWTToken);
        assertThat(employeeService.getByLoginCode(createEmployeeRequestEmployee.getLoginCode()).isPresent()).isTrue();
    }

    @Test
    @Order(2)
    void testDeleteEmployeeByLoginCode() throws EmployeeNotFoundException, NotAllowedException {
        employeeService.deleteEmployeeByLoginCode(createEmployeeRequestEmployee.getLoginCode(), adminJWTToken);

        assertThat(employeeService.getByLoginCode(createEmployeeRequestEmployee.getLoginCode()).isEmpty()).isTrue();
    }

    @Test
    @Order(3)
    void testCreateEmployeeDuplicateException() throws DuplicateEmployeeCodeException, NotAllowedException {
        employeeService.createEmployee(createEmployeeRequestEmployee, adminJWTToken);

        assertThrows(DuplicateEmployeeCodeException.class, () -> employeeService.createEmployee(createEmployeeRequestEmployee, adminJWTToken));
    }

    @Test
    @Order(4)
    void testDeleteEmployeeNotFoundException() {
        assertThrows(EmployeeNotFoundException.class, () -> employeeService.deleteEmployeeByLoginCode("111111", adminJWTToken));
    }

    @Test
    @Order(5)
    void testCreateEmployeeInvalidTokenException() {
        assertThrows(JWTDecodeException.class, () -> employeeService.createEmployee(createEmployeeRequestEmployee, "qweqwewq"));
    }

    @Test
    @Order(6)
    void testCreateEmployeeNullTokenException() {
        assertThrows(NullPointerException.class, () -> employeeService.createEmployee(createEmployeeRequestEmployee, null));
    }

    @Test
    @Order(7)
    void testListAllEmployees() throws NotAllowedException {
        assertThat(employeeService.getAllEmployees(adminJWTToken).size()).isGreaterThan(1);
    }

    @Test
    @Order(8)
    void testListAllEmployeesNoAdminRights() {
        var employeeToken = authenticationService.createToken(employeeService.getByLoginCode("512222").orElse(null));
        assertThrows(NotAllowedException.class, () -> employeeService.getAllEmployees(employeeToken));
    }
}

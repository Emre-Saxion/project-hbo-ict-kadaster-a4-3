package com.kadaster.landmeter.server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.kadaster.landmeter.server.dto.request.AvailableDateRequest;
import com.kadaster.landmeter.server.dto.request.CreateEmployeeRequest;
import com.kadaster.landmeter.server.dto.request.LoginEmployeeRequest;
import com.kadaster.landmeter.server.model.AvailableDate;
import com.kadaster.landmeter.server.service.employee.EmployeeService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureTestDatabase
public class EmployeeControllerTest {

    private final MockMvc mockMvc;

    private final EmployeeService employeeService;

    private final CreateEmployeeRequest createEmployeeRequestAdmin = new CreateEmployeeRequest()
            .setFirstName("Test")
            .setLastName("Last")
            .setEmail("test@email.com")
            .setPhoneNumber("0622222222")
            .setLoginCode("510006")
            .setPassword("password")
            .setAdmin(true);

    @Autowired
    public EmployeeControllerTest(MockMvc mockMvc, EmployeeService employeeService) {
        this.mockMvc = mockMvc;
        this.employeeService = employeeService;
    }

    private String adminJWTToken;

    @Test
    @Order(1)
    void checkNotNull() {
        assertThat(mockMvc).isNotNull();
        assertThat(employeeService).isNotNull();
    }

    @Test
    @Order(2)
    void testLogInWrongCredentials() throws Exception {
        var loginEmployeeRequest = new LoginEmployeeRequest()
                .setLoginCode("123455")
                .setPassword("password");

        var gson = new Gson();
        var json = gson.toJson(loginEmployeeRequest);

        mockMvc.perform(post("/employee/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isNotFound());
    }

    @Test
    @Order(3)
    void testLogInWrongRequirements() throws Exception {
        var loginEmployeeRequest = new LoginEmployeeRequest()
                .setLoginCode("12345")
                .setPassword("password");

        var gson = new Gson();
        var json = gson.toJson(loginEmployeeRequest);

        mockMvc.perform(post("/employee/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Order(4)
    void testLogInCorrectRequirements() throws Exception {
        employeeService.saveEmployee(createEmployeeRequestAdmin);

        var loginEmployeeRequest = new LoginEmployeeRequest()
                .setLoginCode(createEmployeeRequestAdmin.getLoginCode())
                .setPassword(createEmployeeRequestAdmin.getPassword());

        var gson = new Gson();
        var json = gson.toJson(loginEmployeeRequest);

        var mvcResult = mockMvc.perform(post("/employee/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andReturn();

        var jsonObject = JsonParser.parseString(mvcResult.getResponse().getContentAsString()).getAsJsonObject();

        var token = jsonObject.get("token").getAsString();

        adminJWTToken = token;
        assertThat(token).isNotBlank();
    }

    @Test
    @Transactional
    @Order(5)
    void testUpdateAvailableDate() throws Exception {
        var availableDateRequest = new AvailableDateRequest()
                .setAvailableDates(List.of(new AvailableDate()
                        .setDate(LocalDateTime.now())
                        .setAvailable(true).setExtra("")));

        var objectMapper = new ObjectMapper();

        var json = objectMapper.writeValueAsString(availableDateRequest);

        mockMvc.perform(post("/employee/dates")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .header("Authorization", adminJWTToken))
                .andExpect(status().isOk());

        var employee = employeeService.getByLoginCodeChecked(createEmployeeRequestAdmin.getLoginCode());
        assertThat(employee.getAvailableDates().size()).isGreaterThanOrEqualTo(1);
    }

    @Test
    @Transactional
    @Order(6)
    void testUpdateAvailableDatesEmpty() throws Exception {
        var availableDateRequest = new AvailableDateRequest()
                .setAvailableDates(List.of());

        var gson = new Gson();

        var json = gson.toJson(availableDateRequest);

        mockMvc.perform(post("/employee/dates")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .header("Authorization", adminJWTToken))
                .andExpect(status().isOk());

        var employee = employeeService.getByLoginCodeChecked(createEmployeeRequestAdmin.getLoginCode());
        assertThat(employee.getAvailableDates().size()).isLessThanOrEqualTo(0);
    }
}

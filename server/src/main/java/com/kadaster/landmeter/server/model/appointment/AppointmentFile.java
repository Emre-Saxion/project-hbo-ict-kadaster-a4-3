package com.kadaster.landmeter.server.model.appointment;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class AppointmentFile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String fileName;

    private String fileExtension;

    public AppointmentFile() {
    }

    public long getId() {
        return id;
    }

    public AppointmentFile setId(long id) {
        this.id = id;
        return this;
    }

    public String getFileName() {
        return fileName;
    }

    public AppointmentFile setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public AppointmentFile setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
        return this;
    }
}

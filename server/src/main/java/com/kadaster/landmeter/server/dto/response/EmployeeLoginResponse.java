package com.kadaster.landmeter.server.dto.response;

import com.kadaster.landmeter.server.model.AvailableDate;

import java.util.List;

public class EmployeeLoginResponse {

    private String token;

    private String loginCode;

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private String email;

    private boolean admin;

    private List<AvailableDate> availableDates;

    public EmployeeLoginResponse(String token, String loginCode, String firstName, String lastName, String phoneNumber, String email, boolean admin, List<AvailableDate> availableDates) {
        this.token = token;
        this.loginCode = loginCode;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.admin = admin;
        this.availableDates = availableDates;
    }

    public String getToken() {
        return token;
    }

    public EmployeeLoginResponse setToken(String token) {
        this.token = token;
        return this;
    }

    public String getLoginCode() {
        return loginCode;
    }

    public EmployeeLoginResponse setLoginCode(String loginCode) {
        this.loginCode = loginCode;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public EmployeeLoginResponse setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public EmployeeLoginResponse setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public EmployeeLoginResponse setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public EmployeeLoginResponse setEmail(String email) {
        this.email = email;
        return this;
    }

    public boolean isAdmin() {
        return admin;
    }

    public EmployeeLoginResponse setAdmin(boolean admin) {
        this.admin = admin;
        return this;
    }

    public List<AvailableDate> getAvailableDates() {
        return availableDates;
    }

    public EmployeeLoginResponse setAvailableDates(List<AvailableDate> availableDates) {
        this.availableDates = availableDates;
        return this;
    }
}

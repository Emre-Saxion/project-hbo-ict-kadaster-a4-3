package com.kadaster.landmeter.server.dto.request;

import com.kadaster.landmeter.server.model.appointment.AppointmentFile;
import com.kadaster.landmeter.server.model.appointment.Authorizer;

import javax.validation.constraints.NotNull;
import java.util.List;

public class UpdateAppointmentRequest {

    @NotNull
    private String note;

    @NotNull
    private List<Boolean> stakeholderPresent;

    @NotNull
    private List<Authorizer> authorizers;

    @NotNull
    private String measurements;

    @NotNull
    private List<AppointmentFile> addedFiles;

    @NotNull
    private boolean unanimous;

    @NotNull
    private boolean compatible;

    public UpdateAppointmentRequest() {
    }

    public String getNote() {
        return note;
    }

    public UpdateAppointmentRequest setNote(String note) {
        this.note = note;
        return this;
    }

    public List<Boolean> getStakeholderPresent() {
        return stakeholderPresent;
    }

    public UpdateAppointmentRequest setStakeholderPresent(List<Boolean> stakeholderPresent) {
        this.stakeholderPresent = stakeholderPresent;
        return this;
    }

    public List<Authorizer> getAuthorizers() {
        return authorizers;
    }

    public UpdateAppointmentRequest setAuthorizers(List<Authorizer> authorizers) {
        this.authorizers = authorizers;
        return this;
    }

    public String getMeasurements() {
        return measurements;
    }

    public UpdateAppointmentRequest setMeasurements(String measurements) {
        this.measurements = measurements;
        return this;
    }

    public List<AppointmentFile> getAddedFiles() {
        return addedFiles;
    }

    public UpdateAppointmentRequest setAddedFiles(List<AppointmentFile> addedFiles) {
        this.addedFiles = addedFiles;
        return this;
    }

    public boolean isUnanimous() {
        return unanimous;
    }

    public UpdateAppointmentRequest setUnanimous(boolean unanimous) {
        this.unanimous = unanimous;
        return this;
    }

    public boolean isCompatible() {
        return compatible;
    }

    public UpdateAppointmentRequest setCompatible(boolean compatible) {
        this.compatible = compatible;
        return this;
    }
}

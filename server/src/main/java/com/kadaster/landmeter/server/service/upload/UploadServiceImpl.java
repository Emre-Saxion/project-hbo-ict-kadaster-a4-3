package com.kadaster.landmeter.server.service.upload;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.kadaster.landmeter.server.service.auth.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

@Service
public class UploadServiceImpl implements UploadService {

    private final AuthenticationService authenticationService;

    @Autowired
    public UploadServiceImpl(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @Override
    public void uploadFile(long id, MultipartFile multipartFile) throws IOException {
        var directory = createDirectory("files/" + id);
        var path = directory.resolve(Objects.requireNonNull(multipartFile.getOriginalFilename()));

        Files.write(path, multipartFile.getBytes());
    }

    @Override
    public void uploadMap(long id, MultipartFile multipartFile) throws IOException, DocumentException {
        var directory = createDirectory("files/" + id);
        var originalMap = directory.resolve(Objects.requireNonNull(multipartFile.getOriginalFilename()));

        Files.write(originalMap, multipartFile.getBytes());

        var pdfImage = Image.getInstance(originalMap.toFile().getAbsolutePath());

        var A4 = PageSize.A4;

        var scalePortrait = Math.min(A4.getWidth() / pdfImage.getWidth(),
                A4.getHeight() / pdfImage.getHeight());

        var scaleLandscape = Math.min(A4.getHeight() / pdfImage.getWidth(),
                A4.getWidth() / pdfImage.getHeight());

        var isLandscape = scaleLandscape > scalePortrait;

        var width = 0.0F;
        var height = 0.0F;
        if (isLandscape) {
            A4 = A4.rotate();
            width = pdfImage.getWidth() * scaleLandscape;
            height = pdfImage.getHeight() * scaleLandscape;
        } else {
            width = pdfImage.getWidth() * scalePortrait;
            height = pdfImage.getHeight() * scalePortrait;
        }

        var documentToMake = new Document(A4, 10, 10, 10, 10);
        PdfWriter.getInstance(documentToMake, new FileOutputStream("files/" + id + "/drawn.pdf"));
        documentToMake.open();

        pdfImage.scaleAbsolute(width, height);
        var posH = (A4.getHeight() - height) / 2;
        var posW = (A4.getWidth() - width) / 2;

        pdfImage.setAbsolutePosition(posW, posH);
        pdfImage.setBorder(Image.NO_BORDER);
        pdfImage.setBorderWidth(0);

        documentToMake.add(pdfImage);

        documentToMake.close();
        Files.delete(originalMap);
    }

    @Override
    public void uploadImage(String token, MultipartFile multipartFile) throws IOException {
        var employeeId = authenticationService.getEmployeeIdFromToken(token);
        var directory = createDirectory("files/employees/" + employeeId);
        var path = directory.resolve("picture.png");

        Files.write(path, multipartFile.getBytes());
    }

    private Path createDirectory(String path) throws IOException {
        var directory = Path.of(path);
        if (!Files.exists(directory)) {
            Files.createDirectory(directory);
        }
        return directory;
    }

}

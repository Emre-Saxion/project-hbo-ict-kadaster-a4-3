package com.kadaster.landmeter.server.repo;

import com.kadaster.landmeter.server.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Optional<Employee> findFirstByLoginCode(String loginCode);
}

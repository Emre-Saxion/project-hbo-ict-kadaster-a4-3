package com.kadaster.landmeter.server.service.upload;

import com.itextpdf.text.DocumentException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface UploadService {

    /**
     * Uploading a file to a folder (files/{@param id}/filename)
     *
     * @param id            The appointment id
     * @param multipartFile The file sent from the client
     * @throws IOException If unable to write to file
     */
    void uploadFile(long id, MultipartFile multipartFile) throws IOException;

    /**
     * Uploading the updated map to the files folder (files/{@param id}/drawn.pdf)
     *
     * @param id            The appointment id
     * @param multipartFile The map sent from the client as an image
     * @throws IOException       If unable to write to file
     * @throws DocumentException If unable to generate a pdf
     */
    void uploadMap(long id, MultipartFile multipartFile) throws IOException, DocumentException;

    /**
     * Uploading a profile picture
     *
     * @param token The JWT Token
     * @param file  The file send
     * @throws IOException
     */
    void uploadImage(String token, MultipartFile file) throws IOException;
}

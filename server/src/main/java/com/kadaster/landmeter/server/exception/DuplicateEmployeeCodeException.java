package com.kadaster.landmeter.server.exception;

public class DuplicateEmployeeCodeException extends Exception {

    public DuplicateEmployeeCodeException(String message) {
        super(message);
    }

    public DuplicateEmployeeCodeException(String message, Throwable cause) {
        super(message, cause);
    }
}

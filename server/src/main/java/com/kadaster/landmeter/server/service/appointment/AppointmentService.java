package com.kadaster.landmeter.server.service.appointment;

import com.kadaster.landmeter.server.dto.request.CreateAppointmentRequest;
import com.kadaster.landmeter.server.dto.request.UpdateAppointmentRequest;
import com.kadaster.landmeter.server.exception.AppointmentNotFoundException;
import com.kadaster.landmeter.server.exception.NotAllowedException;
import com.kadaster.landmeter.server.model.appointment.Appointment;
import com.kadaster.landmeter.server.repo.EmployeeRepository;
import com.kadaster.landmeter.server.service.employee.EmployeeService;

import java.util.List;

public interface AppointmentService {

    /**
     * Returns all appointments for an employee
     *
     * @param id The employee id
     * @return All appointments for the employee with id {@param id}
     * @throws AppointmentNotFoundException If no appointments are found
     */
    List<Appointment> getAppointsByEmployeeId(long id) throws AppointmentNotFoundException;

    /**
     * Returns all appointments for an employee
     *
     * @param loginCode The login code of the employee
     * @return All appointments for the employee with login code {@param loginCode}
     * @throws AppointmentNotFoundException If no appointments are found
     */
    List<Appointment> getAppointsByEmployeeLoginCode(String loginCode) throws AppointmentNotFoundException;

    /**
     * Returns all appointments that have no employee assigned
     *
     * @param token The token of the admin
     * @return All appointments with no employee
     * @throws AppointmentNotFoundException If no appointments are found
     * @throws NotAllowedException          If not allowed
     */
    List<Appointment> getAppointmentsNoEmployee(String token) throws AppointmentNotFoundException, NotAllowedException;

    /**
     * Returns all appointments with a parcel containing {@param token}
     *
     * @param parcel The parcel to check for
     * @param token  The token of the admin
     * @return All appointments with a parcel containing {@param token}
     * @throws AppointmentNotFoundException If no appointments found
     * @throws NotAllowedException          If not allowed
     */
    List<Appointment> getAppointmentsByParcel(String parcel, String token) throws AppointmentNotFoundException, NotAllowedException;

    Appointment createAppointment(CreateAppointmentRequest createAppointmentRequest, String token) throws NotAllowedException, AppointmentNotFoundException;

    /**
     * Saving an appointment to the database
     * <p>
     * This function should only be used at {@link com.kadaster.landmeter.server.ServerApplication#dummyData(EmployeeService, AppointmentService, EmployeeRepository)}
     *
     * @param appointment The appointment to save
     */
    void saveAppointment(Appointment appointment);

    /**
     * Updating an appointment with new values
     *
     * @param id                       The appointment id
     * @param updateAppointmentRequest The new values of the appointment
     * @throws AppointmentNotFoundException If no appointment has been found
     */
    void updateAppointment(long id, UpdateAppointmentRequest updateAppointmentRequest) throws AppointmentNotFoundException;
}

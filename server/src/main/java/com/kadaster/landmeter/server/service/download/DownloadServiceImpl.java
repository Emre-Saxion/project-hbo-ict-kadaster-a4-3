package com.kadaster.landmeter.server.service.download;

import com.kadaster.landmeter.server.exception.FileNotFoundException;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class DownloadServiceImpl implements DownloadService {
    @Override
    public String getFilePath(long id, String fileName) throws FileNotFoundException {
        var file = Paths.get("files/" + id + "/" + fileName);

        if (!Files.exists(file)) {
            throw new FileNotFoundException("No file with name " + fileName + " found at" + file.toAbsolutePath());
        }
        return file.toString();
    }

    @Override
    public String getProfileFilePath(String loginCode) throws FileNotFoundException {
        var file = Paths.get("files/employees/" + loginCode + "/" + "picture.png");

        if (!Files.exists(file)) {
            throw new FileNotFoundException("No file found at " + file.toAbsolutePath());
        }
        return file.toString();
    }
}

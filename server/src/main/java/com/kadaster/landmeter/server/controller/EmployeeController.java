package com.kadaster.landmeter.server.controller;

import com.kadaster.landmeter.server.dto.request.AvailableDateRequest;
import com.kadaster.landmeter.server.dto.request.LoginEmployeeRequest;
import com.kadaster.landmeter.server.dto.response.EmployeeLoginResponse;
import com.kadaster.landmeter.server.exception.EmployeeNotFoundException;
import com.kadaster.landmeter.server.exception.InvalidCredentialsException;
import com.kadaster.landmeter.server.service.auth.AuthenticationService;
import com.kadaster.landmeter.server.service.employee.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

    private final AuthenticationService authenticationService;

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(AuthenticationService authenticationService, EmployeeService employeeService) {
        this.authenticationService = authenticationService;
        this.employeeService = employeeService;
    }

    @PostMapping(value = "/login", consumes = "application/json", produces = "application/json")
    @ResponseBody
    public EmployeeLoginResponse login(@Valid @RequestBody LoginEmployeeRequest employeeRequest) throws InvalidCredentialsException, EmployeeNotFoundException {
        var employee = authenticationService.authenticate(employeeRequest.getLoginCode(), employeeRequest.getPassword());
        var createdToken = authenticationService.createToken(employee);

        return new EmployeeLoginResponse(createdToken, employee.getLoginCode(), employee.getFirstName(), employee.getLastName(),
                employee.getPhoneNumber(), employee.getEmail(), employee.isAdmin(), employee.getAvailableDates());
    }

    @PostMapping(value = "/dates", consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public void updateDates(@Valid @RequestBody AvailableDateRequest availableDateRequest, @RequestHeader("Authorization") String token) throws EmployeeNotFoundException {
        employeeService.updateDates(availableDateRequest, token);
    }
}

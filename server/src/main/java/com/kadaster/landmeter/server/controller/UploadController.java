package com.kadaster.landmeter.server.controller;

import com.itextpdf.text.DocumentException;
import com.kadaster.landmeter.server.service.upload.UploadService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;

@Controller
@RequestMapping("/upload")
public class UploadController {

    private final UploadService uploadService;

    @Autowired
    public UploadController(UploadService uploadService) {
        this.uploadService = uploadService;
    }

    @PostMapping(value = "/map", consumes = "multipart/form-data")
    @ResponseStatus(HttpStatus.CREATED)
    public void uploadMap(@ApiParam("Appointment id") @Valid @RequestHeader("id") long id, @RequestParam("file") MultipartFile file) throws IOException, DocumentException {
        uploadService.uploadMap(id, file);
    }

    @PostMapping(value = "/single", consumes = "multipart/form-data")
    @ResponseStatus(HttpStatus.CREATED)
    public void uploadSingleFile(@ApiParam("Appointment id") @Valid @RequestHeader("id") long id, @RequestParam("file") MultipartFile file) throws IOException {
        uploadService.uploadFile(id, file);
    }

    @PostMapping(value = "/multi", consumes = "multipart/form-data")
    @ResponseStatus(HttpStatus.CREATED)
    public void uploadMultipleFiles(@ApiParam("Appointment id") @Valid @RequestHeader("id") long id, @RequestParam("files") MultipartFile[] files) throws IOException {
        for (MultipartFile file : files) {
            uploadService.uploadFile(id, file);
        }
    }

    @PostMapping(value = "/profile", consumes = "multipart/form-data")
    @ResponseStatus(HttpStatus.CREATED)
    public void uploadProfilePicture(@RequestHeader("Authorization") String token, @RequestParam("file") MultipartFile file) throws IOException {
        uploadService.uploadImage(token, file);
    }
}


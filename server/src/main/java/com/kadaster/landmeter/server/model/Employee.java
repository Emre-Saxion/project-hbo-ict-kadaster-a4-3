package com.kadaster.landmeter.server.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private String email;

    private String loginCode;

    private String passwordHash;

    private boolean admin;

    @OneToMany(cascade = CascadeType.ALL)
    private List<AvailableDate> availableDates;

    public Employee() {

    }

    public long getId() {
        return id;
    }

    public Employee setId(long id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Employee setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Employee setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Employee setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Employee setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getLoginCode() {
        return loginCode;
    }

    public Employee setLoginCode(String loginCode) {
        this.loginCode = loginCode;
        return this;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public Employee setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
        return this;
    }

    public boolean isAdmin() {
        return admin;
    }

    public Employee setAdmin(boolean admin) {
        this.admin = admin;
        return this;
    }

    public List<AvailableDate> getAvailableDates() {
        return availableDates;
    }

    public Employee setAvailableDates(List<AvailableDate> availableDates) {
        this.availableDates = availableDates;
        return this;
    }
}

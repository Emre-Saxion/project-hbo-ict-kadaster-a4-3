package com.kadaster.landmeter.server.util;

import com.kadaster.landmeter.server.exception.NotAllowedException;
import com.kadaster.landmeter.server.service.auth.AuthenticationService;

public class AdminCheck {

    public static void checkAdmin(AuthenticationService service, String token) throws NotAllowedException {
        if (!service.isAdmin(token)) {
            throw new NotAllowedException("You aren't allowed to do this");
        }
    }
}

package com.kadaster.landmeter.server.dto.response;

public class GetEmployeeResponse {

    private String firstName;

    private String lastName;

    private String loginCode;

    private String phoneNumber;

    private String email;

    public GetEmployeeResponse(String firstName, String lastName, String loginCode, String phoneNumber, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.loginCode = loginCode;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public GetEmployeeResponse setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public GetEmployeeResponse setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getLoginCode() {
        return loginCode;
    }

    public GetEmployeeResponse setLoginCode(String loginCode) {
        this.loginCode = loginCode;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public GetEmployeeResponse setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public GetEmployeeResponse setEmail(String emails) {
        this.email = emails;
        return this;
    }
}

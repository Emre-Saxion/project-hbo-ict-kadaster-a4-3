package com.kadaster.landmeter.server.dto.request;

import com.kadaster.landmeter.server.model.appointment.Stakeholder;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

public class CreateAppointmentRequest {

    @NotNull
    private List<Stakeholder> stakeholders;

    @NotNull
    private String note;

    @NotNull
    private LocalDateTime appointmentDate;

    @NotNull
    private String parcel;

    public CreateAppointmentRequest() {

    }

    public String getNote() {
        return note;
    }

    public CreateAppointmentRequest setNote(String note) {
        this.note = note;
        return this;
    }

    public LocalDateTime getAppointmentDate() {
        return appointmentDate;
    }

    public CreateAppointmentRequest setAppointmentDate(LocalDateTime appointmentDate) {
        this.appointmentDate = appointmentDate;
        return this;
    }

    public String getParcel() {
        return parcel;
    }

    public CreateAppointmentRequest setParcel(String parcel) {
        this.parcel = parcel;
        return this;
    }

    public List<Stakeholder> getStakeholders() {
        return stakeholders;
    }

    public CreateAppointmentRequest setStakeholders(List<Stakeholder> stakeholders) {
        this.stakeholders = stakeholders;
        return this;
    }
}

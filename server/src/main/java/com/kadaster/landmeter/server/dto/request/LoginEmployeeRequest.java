package com.kadaster.landmeter.server.dto.request;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

public class LoginEmployeeRequest {

    @NotBlank
    @Length(min = 6, max = 6)
    private String loginCode;

    @NotBlank
    @Length(min = 8)
    private String password;

    public LoginEmployeeRequest() {

    }

    public String getLoginCode() {
        return loginCode;
    }

    public LoginEmployeeRequest setLoginCode(String loginCode) {
        this.loginCode = loginCode;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public LoginEmployeeRequest setPassword(String password) {
        this.password = password;
        return this;
    }
}

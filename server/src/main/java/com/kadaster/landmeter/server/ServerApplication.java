package com.kadaster.landmeter.server;

import com.kadaster.landmeter.server.dto.request.CreateEmployeeRequest;
import com.kadaster.landmeter.server.model.AvailableDate;
import com.kadaster.landmeter.server.model.appointment.*;
import com.kadaster.landmeter.server.repo.EmployeeRepository;
import com.kadaster.landmeter.server.service.appointment.AppointmentService;
import com.kadaster.landmeter.server.service.employee.EmployeeService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@SpringBootApplication
public class ServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServerApplication.class, args);
    }

    /**
     * This is what gets called when the server starts.
     *
     * @param employeeService {@link EmployeeService}
     * @return args
     */
    @Bean
    public CommandLineRunner dummyData(EmployeeService employeeService, AppointmentService appointmentService, EmployeeRepository employeeRepository) {
        return args -> {
            employeeService.saveEmployee(new CreateEmployeeRequest()
                    .setFirstName("Test")
                    .setLastName("Last")
                    .setEmail("test@email.com")
                    .setPhoneNumber("0622222222")
                    .setLoginCode("123456")
                    .setPassword("password")
                    .setAdmin(true));

            employeeService.saveEmployee(new CreateEmployeeRequest()
                    .setFirstName("Test")
                    .setLastName("Last")
                    .setEmail("test@email.com")
                    .setPhoneNumber("0622222222")
                    .setLoginCode("654321")
                    .setPassword("password")
                    .setAdmin(false));

            var adminEmployee = employeeService.getByLoginCodeChecked("123456");
            if (adminEmployee != null) {
                adminEmployee.setAvailableDates(List.of(
                        new AvailableDate()
                                .setDate(LocalDateTime.now().truncatedTo(ChronoUnit.DAYS))
                                .setAvailable(true)
                                .setExtra(""),
                        new AvailableDate()
                                .setDate(LocalDateTime.now().plus(1, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS))
                                .setAvailable(true)
                                .setExtra(""),
                        new AvailableDate()
                                .setDate(LocalDateTime.now().plus(2, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS))
                                .setAvailable(true)
                                .setExtra("test")
                ));
                employeeRepository.save(adminEmployee);
            }

            var address = new Address()
                    .setCountry("Nederland")
                    .setMunicipality("Overijssel")
                    .setCity("Almelo")
                    .setStreet("Teststreet")
                    .setZipCode("7000LZ")
                    .setNumber("21a");
            appointmentService.saveAppointment(new Appointment()
                    .setParcel("LDN03H7497")
                    .setMapFile("map.pdf")
                    .setCreatedDate(LocalDateTime.now())
                    .setAppointmentDate(LocalDateTime.now().plus(1, ChronoUnit.HOURS))
                    .setAppointmentEndDate(LocalDateTime.now().plus(2, ChronoUnit.HOURS))
                    .setAddress(address)
                    .setMeasurements("")
                    .setType(1)
                    .setUnanimous(false)
                    .setCompatible(false)
                    .setDescription("Het betreft een voormalige huurwoning. De grens tussen de huisnumers 33 en 35 dient bepaald te worden. Dit is de woningscheidende wand en dan vrijwel recht naar voren en recht naar achter.")
                    .setDeed(new Deed()
                            .setPartNumber(20005)
                            .setDeedNumber("00888"))
                    .setEmployee(employeeService.getByLoginCodeChecked("123456"))
                    .setFiles(List.of(
                            new AppointmentFile()
                                    .setFileName("foto22")
                                    .setFileExtension("png"),
                            new AppointmentFile()
                                    .setFileName("foto11")
                                    .setFileExtension("png"),
                            new AppointmentFile()
                                    .setFileName("pdf2")
                                    .setFileExtension("pdf")
                    ))
                    .setAddedFiles(List.of())
                    .setStakeholders(List.of(
                            new Stakeholder()
                                    .setName("Henk de Notaris")
                                    .setAddress(address.setStreet("SecondTestStreet"))
                                    .setEmail("stakeholder@email.com")
                                    .setPhoneNumber("0611223344")
                                    .setBirthDate(LocalDateTime.now())
                                    .setTitle("Verkrijger")
                                    .setMunacipility(false),
                            new Stakeholder().setName("Piet Boer")
                                    .setAddress(address.setStreet("SecondTestStreet"))
                                    .setEmail("stakeholder@email.com")
                                    .setPhoneNumber("0611223344")
                                    .setBirthDate(LocalDateTime.now())
                                    .setTitle("Vervreemder")
                                    .setMunacipility(false)
                    ))
            );

            var address2 = new Address()
                    .setCountry("Nederland")
                    .setMunicipality("Overijssel")
                    .setCity("Almelo")
                    .setStreet("Teststreet")
                    .setZipCode("7000LZ")
                    .setNumber("21a");
            appointmentService.saveAppointment(new Appointment()
                    .setParcel("LDN03H7497")
                    .setMapFile("map.pdf")
                    .setCreatedDate(LocalDateTime.now())
                    .setAppointmentDate(LocalDateTime.now().plus(3, ChronoUnit.HOURS))
                    .setAppointmentEndDate(LocalDateTime.now().plus(4, ChronoUnit.HOURS))
                    .setAddress(address2)
                    .setMeasurements("")
                    .setDeed(new Deed()
                            .setPartNumber(20005)
                            .setDeedNumber("00888"))
                    .setType(1)
                    .setUnanimous(false)
                    .setCompatible(false)
                    .setDescription("Het betreft een voormalige huurwoning. De grens tussen de huisnumers 33 en 35 dient bepaald te worden. Dit is de woningscheidende wand en dan vrijwel recht naar voren en recht naar achter.")
                    .setFiles(List.of(
                            new AppointmentFile()
                                    .setFileName("foto21")
                                    .setFileExtension("png"),
                            new AppointmentFile()
                                    .setFileName("foto10")
                                    .setFileExtension("png"),
                            new AppointmentFile()
                                    .setFileName("pdf2")
                                    .setFileExtension("pdf")
                    ))
                    .setAddedFiles(List.of())
                    .setStakeholders(List.of(
                            new Stakeholder()
                                    .setName("Henk de Notaris")
                                    .setAddress(address2.setStreet("SecondTestStreet"))
                                    .setEmail("stakeholder@email.com")
                                    .setPhoneNumber("0611223344")
                                    .setBirthDate(LocalDateTime.now())
                                    .setTitle("Verkrijger")
                                    .setMunacipility(false),
                            new Stakeholder()
                                    .setName("Piet Boer").
                                    setAddress(address2.setStreet("SecondTestStreet"))
                                    .setEmail("stakeholder@email.com")
                                    .setPhoneNumber("0611223344")
                                    .setBirthDate(LocalDateTime.now())
                                    .setTitle("Vervreemder")
                                    .setMunacipility(false)
                    ))
            );

            var address3 = new Address()
                    .setCountry("Nederland")
                    .setMunicipality("Overijssel")
                    .setCity("Hengelo")
                    .setStreet("Enschedesestraat")
                    .setZipCode("7552CK")
                    .setNumber("144");
            appointmentService.saveAppointment(new Appointment()
                    .setParcel("WIJ02-E-798")
                    .setMapFile("map.pdf")
                    .setCreatedDate(LocalDateTime.now())
                    .setAppointmentDate(LocalDateTime.now().plus(4, ChronoUnit.DAYS))
                    .setAppointmentEndDate(LocalDateTime.now().plus(4, ChronoUnit.DAYS).plus(1, ChronoUnit.HOURS))
                    .setAddress(address3)
                    .setMeasurements("")
                    .setDeed(new Deed()
                            .setPartNumber(10002)
                            .setDeedNumber("00912"))
                    .setEmployee(employeeService.getByLoginCodeChecked("123456"))
                    .setType(2)
                    .setUnanimous(false)
                    .setCompatible(false)
                    .setDescription("Het betreft een voormalige huurwoning. De grens tussen de huisnumers 33 en 35 dient bepaald te worden. Dit is de woningscheidende wand en dan vrijwel recht naar voren en recht naar achter.")
                    .setFiles(List.of(
                            new AppointmentFile()
                                    .setFileName("foto1")
                                    .setFileExtension("png"),
                            new AppointmentFile()
                                    .setFileName("foto2")
                                    .setFileExtension("png"),
                            new AppointmentFile()
                                    .setFileName("voorbeeld")
                                    .setFileExtension("pdf")
                    ))
                    .setAddedFiles(List.of())
                    .setStakeholders(List.of(
                            new Stakeholder()
                                    .setName("Gijs de Jong")
                                    .setAddress(address3.setStreet("Enschedesestraat"))
                                    .setEmail("stakeholder@email.com")
                                    .setPhoneNumber("0615337678")
                                    .setBirthDate(LocalDateTime.now())
                                    .setTitle("Verkrijger")
                                    .setMunacipility(false),
                            new Stakeholder()
                                    .setName("Henk de Vries")
                                    .setAddress(address3.setStreet("Enschedesestraat"))
                                    .setEmail("stakeholder@email.com")
                                    .setPhoneNumber("0615337678")
                                    .setBirthDate(LocalDateTime.now())
                                    .setTitle("Vervreemder")
                                    .setMunacipility(false)
                    ))
            );

            var address4 = new Address()
                    .setCountry("Nederland")
                    .setMunicipality("Overijssel")
                    .setCity("Hengelo")
                    .setStreet("Enschedesestraat")
                    .setZipCode("7552CZ")
                    .setNumber("144");
            appointmentService.saveAppointment(new Appointment()
                    .setParcel("WIJ02-E-798")
                    .setMapFile("map.pdf")
                    .setCreatedDate(LocalDateTime.now())
                    .setAppointmentDate(LocalDateTime.now().plus(4, ChronoUnit.HOURS))
                    .setAppointmentEndDate(LocalDateTime.now().plus(5, ChronoUnit.HOURS))
                    .setAddress(address4)
                    .setMeasurements("")
                    .setDeed(new Deed()
                            .setPartNumber(10002)
                            .setDeedNumber("00912"))
                    .setEmployee(employeeService.getByLoginCodeChecked("123456"))
                    .setType(2)
                    .setUnanimous(false)
                    .setCompatible(false)
                    .setDescription("Het betreft een voormalige huurwoning. De grens tussen de huisnumers 33 en 35 dient bepaald te worden. Dit is de woningscheidende wand en dan vrijwel recht naar voren en recht naar achter.")
                    .setFiles(List.of(
                            new AppointmentFile()
                                    .setFileName("foto1")
                                    .setFileExtension("png"),
                            new AppointmentFile()
                                    .setFileName("foto2")
                                    .setFileExtension("png"),
                            new AppointmentFile()
                                    .setFileName("voorbeeld")
                                    .setFileExtension("pdf")
                    ))
                    .setAddedFiles(List.of())
                    .setStakeholders(List.of(
                            new Stakeholder()
                                    .setName("Gijs de Jong")
                                    .setAddress(address4.setStreet("Enschedesestraat"))
                                    .setEmail("stakeholder@email.com")
                                    .setPhoneNumber("0615337678")
                                    .setBirthDate(LocalDateTime.now())
                                    .setTitle("Verkrijger")
                                    .setMunacipility(false),
                            new Stakeholder()
                                    .setName("Gemeente Hengelo")
                                    .setAddress(address4.setStreet("Enschedesestraat"))
                                    .setEmail("stakeholder@email.com")
                                    .setPhoneNumber("0615337678")
                                    .setBirthDate(LocalDateTime.now())
                                    .setTitle("Vervreemder")
                                    .setMunacipility(true)
                    ))
            );
        };
    }
}

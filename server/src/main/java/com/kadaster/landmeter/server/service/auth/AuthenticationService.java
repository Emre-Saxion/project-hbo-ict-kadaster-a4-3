package com.kadaster.landmeter.server.service.auth;

import com.kadaster.landmeter.server.exception.EmployeeNotFoundException;
import com.kadaster.landmeter.server.exception.InvalidCredentialsException;
import com.kadaster.landmeter.server.exception.InvalidTokenException;
import com.kadaster.landmeter.server.model.Employee;

public interface AuthenticationService {

    /**
     * Generating a JWT token
     *
     * @param employee The employee to generate it for
     * @return a JWT token
     */
    String createToken(Employee employee);

    /**
     * Grabbing the Employee from the JWT token, using a database request
     *
     * @param token The JWT token
     * @return An employee if found
     * @throws EmployeeNotFoundException if no employee is found
     */
    Employee getEmployeeFromToken(String token) throws EmployeeNotFoundException;

    /**
     * Grabbomg The employee id from the jwt token subject
     *
     * @param token The JWT token
     * @return The employee id in the jwt subject
     */
    long getEmployeeIdFromToken(String token);

    /**
     * Checking if the employee is an admin based on the claims in the JWT token
     *
     * @param token The jwt token
     * @return true if admin otherwise false
     */
    boolean isAdmin(String token);

    /**
     * Authenticating an employee based on loginCode and password using bcrypt
     *
     * @param loginCode The logincode given
     * @param password  The password given in plaintext
     * @return Employee if authenticated
     * @throws EmployeeNotFoundException   If no employee found based on {@param loginCode}
     * @throws InvalidCredentialsException If wrong password
     */
    Employee authenticate(String loginCode, String password) throws EmployeeNotFoundException, InvalidCredentialsException;

    void verify(String token) throws InvalidTokenException;
}

package com.kadaster.landmeter.server.service.auth;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.kadaster.landmeter.server.exception.EmployeeNotFoundException;
import com.kadaster.landmeter.server.exception.InvalidCredentialsException;
import com.kadaster.landmeter.server.exception.InvalidTokenException;
import com.kadaster.landmeter.server.model.Employee;
import com.kadaster.landmeter.server.repo.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private final EmployeeRepository employeeRepository;

    private final String jwtSecret;

    @Autowired
    public AuthenticationServiceImpl(EmployeeRepository employeeRepository, @Value("${jwt.secret}") String jwtSecret) {
        this.employeeRepository = employeeRepository;
        this.jwtSecret = jwtSecret;
    }

    @Override
    public String createToken(Employee employee) {
        var now = Instant.now();
        var expiration = now.plus(3650, ChronoUnit.DAYS);
        return JWT.create()
                .withIssuer("server")
                .withSubject(String.valueOf(employee.getId()))
                .withIssuedAt(Date.from(now))
                .withNotBefore(Date.from(now))
                .withClaim("admin", employee.isAdmin())
                .withExpiresAt(Date.from(expiration))
                .sign(Algorithm.HMAC256(jwtSecret));
    }

    @Override
    public Employee getEmployeeFromToken(String token) throws EmployeeNotFoundException {
        var employeeId = Long.parseLong(JWT.decode(token).getSubject());
        return employeeRepository.findById(employeeId)
                .orElseThrow(() -> new EmployeeNotFoundException("Employee with code " + employeeId + " doesn't exist"));
    }

    @Override
    public long getEmployeeIdFromToken(String token) {
        return Long.parseLong(JWT.decode(token).getSubject());
    }

    @Override
    public boolean isAdmin(String token) {
        return JWT.decode(token).getClaim("admin").asBoolean();
    }

    @Override
    public Employee authenticate(String loginCode, String password) throws EmployeeNotFoundException, InvalidCredentialsException {
        var employee = employeeRepository.findFirstByLoginCode(loginCode)
                .orElseThrow(() -> new EmployeeNotFoundException("Employee with code " + loginCode + " doesn't exist"));

        var valid = BCrypt.checkpw(password, employee.getPasswordHash());
        if (!valid) {
            throw new InvalidCredentialsException("Invalid credentials");
        }
        return employee;
    }

    @Override
    public void verify(String token) throws InvalidTokenException {
        var verifier = JWT.require(Algorithm.HMAC256(jwtSecret)).build();
        try {
            verifier.verify(token);
        } catch (Exception e) {
            throw new InvalidTokenException("Invalid token");
        }
    }
}

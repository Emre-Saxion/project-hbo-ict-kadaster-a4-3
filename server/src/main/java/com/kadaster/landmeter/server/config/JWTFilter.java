package com.kadaster.landmeter.server.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JWTFilter extends GenericFilterBean {

    private final String jwtSecret;

    @Autowired
    public JWTFilter(@Value("${jwt.secret}") String jwtSecret) {
        this.jwtSecret = jwtSecret;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        var res = (HttpServletResponse) servletResponse;
        res.reset();

        var token = ((HttpServletRequest) servletRequest).getHeader("Authorization");
        try {
            if (token == null) {
                filterChain.doFilter(servletRequest, servletResponse);
                return;
            }

            token = extractJwt(token);

            var verifier = JWT.require(Algorithm.HMAC256(jwtSecret)).build();

            verifier.verify(token);

            filterChain.doFilter(servletRequest, servletResponse);
        } catch (JWTDecodeException ex) {
            logger.error("Failed to decode JWT", ex);
            res.sendError(HttpServletResponse.SC_BAD_REQUEST, "The token you provided does not have the correct format.");
        } catch (JWTVerificationException ex) {
            logger.error("Unauthorized access", ex);
            res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "You are not authorized to access this content.");
        }
    }

    private String extractJwt(String token) {
        if (token.contains(" ")) {
            token = token.split(" ")[1];
        }
        return token;
    }
}

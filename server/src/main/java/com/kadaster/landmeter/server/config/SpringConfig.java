//package com.kadaster.landmeter.server.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import springfox.documentation.builders.ApiInfoBuilder;
//import springfox.documentation.builders.PathSelectors;
//import springfox.documentation.service.ApiInfo;
//import springfox.documentation.spi.DocumentationType;
//import springfox.documentation.spring.web.plugins.Docket;
//import springfox.documentation.swagger2.annotations.EnableSwagger2;
//
//@Configuration
//@EnableScheduling
//@EnableSwagger2
//public class SpringConfig {
//
////    @Bean
////    public TaskScheduler taskScheduler() {
////        return new ThreadPoolTaskScheduler();
////    }
//
//    @Bean
//    public Docket api(ApiInfo apiInfo) {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .groupName("kadaster-landmeters-api")
//                .apiInfo(apiInfo)
//                .select()
//                .paths(PathSelectors.any())
//                .build();
//    }
//
//    @Bean
//    public ApiInfo apiInfo() {
//        return new ApiInfoBuilder()
//                .title("Kadaster Landmeters API")
//                .description("API for kadaster landmeters")
//                .version("1.0.0")
//                .build();
//    }
//}

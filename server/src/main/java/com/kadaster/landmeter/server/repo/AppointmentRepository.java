package com.kadaster.landmeter.server.repo;

import com.kadaster.landmeter.server.model.appointment.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {

    Optional<List<Appointment>> findAppointmentsByEmployeeIdOrderByAppointmentDate(long id);

    Optional<List<Appointment>> findAppointmentsByEmployeeLoginCodeOrderByAppointmentDate(String loginCode);

    Optional<List<Appointment>> findAppointmentsByEmployeeIsNull();

    Optional<List<Appointment>> findAppointmentsByParcelContainingIgnoreCase(String parcel);

    Optional<Appointment> findAppointmentByParcel(String parcel);
}

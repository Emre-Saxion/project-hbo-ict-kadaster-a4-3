package com.kadaster.landmeter.server.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class AvailableDate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime date;

    private boolean available;

    private String extra;

    public AvailableDate() {
    }

    public long getId() {
        return id;
    }

    public AvailableDate setId(long id) {
        this.id = id;
        return this;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public AvailableDate setDate(LocalDateTime date) {
        this.date = date;
        return this;
    }

    public boolean isAvailable() {
        return available;
    }

    public AvailableDate setAvailable(boolean available) {
        this.available = available;
        return this;
    }

    public String getExtra() {
        return extra;
    }

    public AvailableDate setExtra(String extra) {
        this.extra = extra;
        return this;
    }
}

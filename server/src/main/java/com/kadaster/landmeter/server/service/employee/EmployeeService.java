package com.kadaster.landmeter.server.service.employee;

import com.kadaster.landmeter.server.dto.request.AvailableDateRequest;
import com.kadaster.landmeter.server.dto.request.CreateEmployeeRequest;
import com.kadaster.landmeter.server.exception.DuplicateEmployeeCodeException;
import com.kadaster.landmeter.server.exception.EmployeeNotFoundException;
import com.kadaster.landmeter.server.exception.NotAllowedException;
import com.kadaster.landmeter.server.model.Employee;
import com.kadaster.landmeter.server.repo.EmployeeRepository;
import com.kadaster.landmeter.server.service.appointment.AppointmentService;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {

    /**
     * Grabbing an employee by login code
     *
     * @param loginCode The login code given
     * @return Optional of employee
     */
    Optional<Employee> getByLoginCode(String loginCode);

    /**
     * Grabbing an employee by login code checked
     *
     * @param loginCode The login code to search for
     * @return Employee if found
     * @throws EmployeeNotFoundException If no employee found
     */
    Employee getByLoginCodeChecked(String loginCode) throws EmployeeNotFoundException;

    /**
     * Grabbing an employee by id
     *
     * @param id The unique id of the employee
     * @return employee with id {@param id}
     */
    Employee getById(long id) throws EmployeeNotFoundException;

    /**
     * Saving an employee to {@link com.kadaster.landmeter.server.repo.EmployeeRepository}
     *
     * @param request The {@link CreateEmployeeRequest} request which the server will create an employee from
     * @return the added employee
     */
    Employee saveEmployee(CreateEmployeeRequest request);

    /**
     * Wrapper around {@link #saveEmployee(CreateEmployeeRequest)}
     * but will throw an exception if an employee with the same login code exists or the user isn't an admin
     *
     * @param request The {@link CreateEmployeeRequest} request which the server will create an employee from
     * @param token   The jwt token of the administrator
     * @throws DuplicateEmployeeCodeException If an employee with the same login code exists
     */
    void createEmployee(CreateEmployeeRequest request, String token) throws DuplicateEmployeeCodeException, NotAllowedException;

    /**
     * First deletes the employee with a login code in {@param request}
     * and then calls {@link #saveEmployee(CreateEmployeeRequest)}
     * <p>
     * This function should only be used at {@link com.kadaster.landmeter.server.ServerApplication#dummyData(EmployeeService, AppointmentService, EmployeeRepository)}
     *
     * @param token   The jwt token of the administrator
     * @param request The {@link CreateEmployeeRequest} request which the server will create an employee from
     */
    void deleteAndCreateEmployee(CreateEmployeeRequest request, String token) throws NotAllowedException;

    /**
     * Deleting an employee based on login code
     *
     * @param loginCode The login code supplied
     * @param token     The jwt token of the administrator
     * @throws EmployeeNotFoundException If no employee with {@param loginCode} exists
     */
    void deleteEmployeeByLoginCode(String loginCode, String token) throws EmployeeNotFoundException, NotAllowedException;


    /**
     * Grabbing all employees
     *
     * @param token The jwt token of the administrator
     * @return All employees
     */
    List<Employee> getAllEmployees(String token) throws NotAllowedException;

    /**
     * Updating the available dates of the employee
     *
     * @param availableDateRequest The {@link AvailableDateRequest} request which will contain all the correct dates
     * @param token                The jwt token of the employee
     * @throws EmployeeNotFoundException If no employee found
     */
    void updateDates(AvailableDateRequest availableDateRequest, String token) throws EmployeeNotFoundException;
}

package com.kadaster.landmeter.server.controller;

import com.kadaster.landmeter.server.dto.request.UpdateAppointmentRequest;
import com.kadaster.landmeter.server.dto.response.GetAppointmentResponse;
import com.kadaster.landmeter.server.exception.AppointmentNotFoundException;
import com.kadaster.landmeter.server.service.appointment.AppointmentService;
import com.kadaster.landmeter.server.service.auth.AuthenticationService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/appointment")
public class AppointmentController {

    private final AuthenticationService authenticationService;

    private final AppointmentService appointmentService;

    @Autowired
    public AppointmentController(AuthenticationService authenticationService, AppointmentService appointmentService) {
        this.authenticationService = authenticationService;
        this.appointmentService = appointmentService;
    }

    @GetMapping(value = "/all", produces = "application/json")
    @ResponseBody
    public List<GetAppointmentResponse> allAppointments(@RequestHeader("Authorization") String token) throws AppointmentNotFoundException {
        var employeeId = authenticationService.getEmployeeIdFromToken(token);

        return appointmentService.getAppointsByEmployeeId(employeeId)
                .stream()
                .map(appointment -> new GetAppointmentResponse(appointment.getId(), appointment.getParcel(), appointment.getMapFile(), appointment.getFiles(), appointment.getAddedFiles(),
                        appointment.getAppointmentDate(), appointment.getAppointmentEndDate(), appointment.getUpdatedDate(), appointment.getNote(), appointment.getAddress(), appointment.getMeasurements(),
                        appointment.getDeed(), appointment.getStakeholders(), appointment.getType(), appointment.getDescription(), appointment.isUnanimous(), appointment.isCompatible()))
                .collect(Collectors.toList());
    }

    @PostMapping(value = "/update/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public void updateAppointment(@ApiParam("Appointment id") @PathVariable("id") long id, @Valid @RequestBody UpdateAppointmentRequest updateAppointmentRequest) throws AppointmentNotFoundException {
        appointmentService.updateAppointment(id, updateAppointmentRequest);
    }
}

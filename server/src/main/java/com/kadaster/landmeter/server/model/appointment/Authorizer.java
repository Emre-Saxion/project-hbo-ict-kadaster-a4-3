package com.kadaster.landmeter.server.model.appointment;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class Authorizer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;

    private String acquaintance;

    private LocalDateTime birthDate;

    public Authorizer() {
    }

    public String getName() {
        return name;
    }

    public String getAcquaintance() {
        return acquaintance;
    }

    public LocalDateTime getBirthDate() {
        return birthDate;
    }

    public Authorizer setName(String name) {
        this.name = name;
        return this;
    }

    public Authorizer setAcquaintance(String acquaintance) {
        this.acquaintance = acquaintance;
        return this;
    }

    public Authorizer setBirthDate(LocalDateTime birthDate) {
        this.birthDate = birthDate;
        return this;
    }
}

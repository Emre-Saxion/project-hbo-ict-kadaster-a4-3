package com.kadaster.landmeter.server.model.appointment;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Deed {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private int partNumber;

    private String deedNumber;

    private String deedText;

    public Deed() {
    }

    public long getId() {
        return id;
    }

    public Deed setId(long id) {
        this.id = id;
        return this;
    }

    public int getPartNumber() {
        return partNumber;
    }

    public Deed setPartNumber(int partNumber) {
        this.partNumber = partNumber;
        return this;
    }

    public String getDeedNumber() {
        return deedNumber;
    }

    public Deed setDeedNumber(String deedNumber) {
        this.deedNumber = deedNumber;
        return this;
    }

    public String getDeedText() {
        return deedText;
    }

    public Deed setDeedText(String deedText) {
        this.deedText = deedText;
        return this;
    }
}

package com.kadaster.landmeter.server.controller;

import com.kadaster.landmeter.server.dto.request.CreateAppointmentRequest;
import com.kadaster.landmeter.server.dto.request.CreateEmployeeRequest;
import com.kadaster.landmeter.server.dto.response.GetAppointmentResponse;
import com.kadaster.landmeter.server.dto.response.GetEmployeeResponse;
import com.kadaster.landmeter.server.exception.AppointmentNotFoundException;
import com.kadaster.landmeter.server.exception.DuplicateEmployeeCodeException;
import com.kadaster.landmeter.server.exception.NotAllowedException;
import com.kadaster.landmeter.server.model.appointment.Appointment;
import com.kadaster.landmeter.server.service.appointment.AppointmentService;
import com.kadaster.landmeter.server.service.employee.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private final EmployeeService employeeService;

    private final AppointmentService appointmentService;

    @Autowired
    public AdminController(EmployeeService employeeService, AppointmentService appointmentService) {
        this.employeeService = employeeService;
        this.appointmentService = appointmentService;
    }

    @GetMapping(value = "/employees/all", produces = "application/json")
    @ResponseBody
    public List<GetEmployeeResponse> allEmployees(@RequestHeader("Authorization") String token) throws NotAllowedException {
        return employeeService.getAllEmployees(token)
                .stream()
                .map(e -> new GetEmployeeResponse(e.getFirstName(), e.getLastName(), e.getLoginCode(), e.getPhoneNumber(), e.getEmail()))
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/appointments/unplanned", produces = "application/json")
    @ResponseBody
    public List<GetAppointmentResponse> allAppointmentsUnplanned(@RequestHeader("Authorization") String token) throws AppointmentNotFoundException, NotAllowedException {
        return appointmentService.getAppointmentsNoEmployee(token)
                .stream()
                .map(this::fromAppointment)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/appointments/")
    @ResponseBody
    public List<GetAppointmentResponse> allAppointmentsAddress(@RequestParam("parcel") String parcel, @RequestHeader("Authorization") String token) throws AppointmentNotFoundException, NotAllowedException {
        return appointmentService.getAppointmentsByParcel(parcel, token)
                .stream()
                .map(this::fromAppointment)
                .collect(Collectors.toList());
    }

    @PostMapping(value = "/employee/create", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public void createEmployee(@Valid @RequestBody CreateEmployeeRequest employeeRequest, @RequestHeader("Authorization") String token)
            throws DuplicateEmployeeCodeException, NotAllowedException {
        employeeService.createEmployee(employeeRequest, token);
    }

    @PostMapping(value = "/appointment/create", consumes = "application/json", produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public GetAppointmentResponse createAppointment(@Valid @RequestBody CreateAppointmentRequest createAppointmentRequest, @RequestHeader("Authorization") String token) throws NotAllowedException, AppointmentNotFoundException {
        var appointment = appointmentService.createAppointment(createAppointmentRequest, token);
        return fromAppointment(appointment);
    }

    private GetAppointmentResponse fromAppointment(Appointment appointment) {
        return new GetAppointmentResponse(appointment.getId(), appointment.getParcel(),
                appointment.getMapFile(), appointment.getFiles(), appointment.getAddedFiles(),
                appointment.getAppointmentDate(), appointment.getAppointmentEndDate(), appointment.getUpdatedDate(),
                appointment.getNote(), appointment.getAddress(), appointment.getMeasurements(), appointment.getDeed(),
                appointment.getStakeholders(), appointment.getType(), appointment.getDescription(), appointment.isUnanimous(),
                appointment.isCompatible());
    }
}

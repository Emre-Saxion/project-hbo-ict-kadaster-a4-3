package com.kadaster.landmeter.server.dto.response;

import com.kadaster.landmeter.server.model.appointment.Address;
import com.kadaster.landmeter.server.model.appointment.AppointmentFile;
import com.kadaster.landmeter.server.model.appointment.Deed;
import com.kadaster.landmeter.server.model.appointment.Stakeholder;

import java.time.LocalDateTime;
import java.util.List;

public class GetAppointmentResponse {

    private long id;

    private String parcel;

    private String mapFile;

    private List<AppointmentFile> files;

    private List<AppointmentFile> addedFiles;

    private LocalDateTime appointmentDate;

    private LocalDateTime appointmentEndDate;

    private LocalDateTime updatedDate;

    private String note;

    private Address address;

    private String measurements;

    private Deed deed;

    private List<Stakeholder> stakeholders;

    private int type;

    private String description;

    private boolean unanimous;

    private boolean compatible;

    public GetAppointmentResponse(long id, String parcel, String mapFile, List<AppointmentFile> files,
                                  List<AppointmentFile> addedFiles, LocalDateTime appointmentDate,
                                  LocalDateTime appointmentEndDate, LocalDateTime updatedDate, String note, Address address,
                                  String measurements, Deed deed, List<Stakeholder> stakeholders, int type,
                                  String description, boolean unanimous, boolean compatible) {
        this.id = id;
        this.parcel = parcel;
        this.mapFile = mapFile;
        this.files = files;
        this.addedFiles = addedFiles;
        this.appointmentDate = appointmentDate;
        this.appointmentEndDate = appointmentEndDate;
        this.updatedDate = updatedDate;
        this.note = note;
        this.address = address;
        this.measurements = measurements;
        this.deed = deed;
        this.stakeholders = stakeholders;
        this.type = type;
        this.description = description;
        this.unanimous = unanimous;
        this.compatible = compatible;
    }

    public long getId() {
        return id;
    }

    public GetAppointmentResponse setId(long id) {
        this.id = id;
        return this;
    }

    public String getParcel() {
        return parcel;
    }

    public GetAppointmentResponse setParcel(String parcel) {
        this.parcel = parcel;
        return this;
    }

    public String getMapFile() {
        return mapFile;
    }

    public GetAppointmentResponse setMapFile(String mapFile) {
        this.mapFile = mapFile;
        return this;
    }

    public List<AppointmentFile> getFiles() {
        return files;
    }

    public GetAppointmentResponse setFiles(List<AppointmentFile> files) {
        this.files = files;
        return this;
    }

    public List<AppointmentFile> getAddedFiles() {
        return addedFiles;
    }

    public GetAppointmentResponse setAddedFiles(List<AppointmentFile> addedFiles) {
        this.addedFiles = addedFiles;
        return this;
    }

    public LocalDateTime getAppointmentDate() {
        return appointmentDate;
    }

    public GetAppointmentResponse setAppointmentDate(LocalDateTime appointmentDate) {
        this.appointmentDate = appointmentDate;
        return this;
    }

    public LocalDateTime getAppointmentEndDate() {
        return appointmentEndDate;
    }

    public GetAppointmentResponse setAppointmentEndDate(LocalDateTime appointmentEndDate) {
        this.appointmentEndDate = appointmentEndDate;
        return this;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public GetAppointmentResponse setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public String getNote() {
        return note;
    }

    public GetAppointmentResponse setNote(String note) {
        this.note = note;
        return this;
    }

    public Address getAddress() {
        return address;
    }

    public GetAppointmentResponse setAddress(Address address) {
        this.address = address;
        return this;
    }

    public String getMeasurements() {
        return measurements;
    }

    public GetAppointmentResponse setMeasurements(String measurements) {
        this.measurements = measurements;
        return this;
    }

    public Deed getDeed() {
        return deed;
    }

    public GetAppointmentResponse setDeed(Deed deed) {
        this.deed = deed;
        return this;
    }

    public List<Stakeholder> getStakeholders() {
        return stakeholders;
    }

    public GetAppointmentResponse setStakeholders(List<Stakeholder> stakeholders) {
        this.stakeholders = stakeholders;
        return this;
    }

    public int getType() {
        return type;
    }

    public GetAppointmentResponse setType(int type) {
        this.type = type;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public GetAppointmentResponse setDescription(String description) {
        this.description = description;
        return this;
    }

    public boolean getUnanimous() {
        return unanimous;
    }

    public GetAppointmentResponse setUnanimous(boolean unanimous) {
        this.unanimous = unanimous;
        return this;
    }

    public boolean getCompatible() {
        return compatible;
    }

    public GetAppointmentResponse setCompatible(boolean compatible) {
        this.compatible = compatible;
        return this;
    }
}

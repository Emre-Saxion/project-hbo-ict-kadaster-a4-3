package com.kadaster.landmeter.server.dto.request;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class CreateEmployeeRequest {

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @NotBlank
    @Email
    private String email;

    @NotBlank
    @Pattern(regexp = "(^$|[0-9]{10})")
    private String phoneNumber;

    @NotBlank
    @Length(min = 6, max = 6)
    private String loginCode;

    @NotBlank
    @Length(min = 8)
    private String password;

    @NotNull
    private boolean admin;

    public CreateEmployeeRequest() {

    }

    public String getFirstName() {
        return firstName;
    }

    public CreateEmployeeRequest setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public CreateEmployeeRequest setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public CreateEmployeeRequest setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public CreateEmployeeRequest setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getLoginCode() {
        return loginCode;
    }

    public CreateEmployeeRequest setLoginCode(String loginCode) {
        this.loginCode = loginCode;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public CreateEmployeeRequest setPassword(String password) {
        this.password = password;
        return this;
    }

    public boolean isAdmin() {
        return admin;
    }

    public CreateEmployeeRequest setAdmin(boolean admin) {
        this.admin = admin;
        return this;
    }
}

package com.kadaster.landmeter.server.service.download;

import com.kadaster.landmeter.server.exception.FileNotFoundException;

public interface DownloadService {

    /**
     * Returning the correct file path based on {@param id} and {@param fileName}
     *
     * @param id       The appointment id
     * @param fileName The file name of the file to download
     * @return The correct file path
     * @throws FileNotFoundException If no file found
     */
    String getFilePath(long id, String fileName) throws FileNotFoundException;

    /**
     * Returning the correct file path based on {param loginCode}
     *
     * @param loginCode The login code of the employee
     * @return The correct file path
     * @throws FileNotFoundException If no file found
     */
    String getProfileFilePath(String loginCode) throws FileNotFoundException;
}

package com.kadaster.landmeter.server.service.appointment;

import com.kadaster.landmeter.server.dto.request.CreateAppointmentRequest;
import com.kadaster.landmeter.server.dto.request.UpdateAppointmentRequest;
import com.kadaster.landmeter.server.exception.AppointmentNotFoundException;
import com.kadaster.landmeter.server.exception.NotAllowedException;
import com.kadaster.landmeter.server.model.appointment.Appointment;
import com.kadaster.landmeter.server.repo.AppointmentRepository;
import com.kadaster.landmeter.server.service.auth.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

import static com.kadaster.landmeter.server.util.AdminCheck.checkAdmin;

@Service
public class AppointmentServiceImpl implements AppointmentService {

    private final AppointmentRepository appointmentRepository;

    private final AuthenticationService authenticationService;

    @Autowired
    public AppointmentServiceImpl(AppointmentRepository appointmentRepository, AuthenticationService authenticationService) {
        this.appointmentRepository = appointmentRepository;
        this.authenticationService = authenticationService;
    }

    private List<Appointment> getCorrectAppointments(List<Appointment> appointments) {
        var dateToCheck = LocalDateTime.now().minus(7, ChronoUnit.DAYS);

        return appointments
                .stream()
                .filter(a -> a.getUpdatedDate() == null || a.getUpdatedDate().isAfter(dateToCheck))
                .collect(Collectors.toUnmodifiableList());
    }

    @Override
    public List<Appointment> getAppointsByEmployeeId(long id) throws AppointmentNotFoundException {
        var appointments = appointmentRepository.findAppointmentsByEmployeeIdOrderByAppointmentDate(id)
                .orElseThrow(() -> new AppointmentNotFoundException("No appointments found for " + id));
        return getCorrectAppointments(appointments);
    }

    @Override
    public List<Appointment> getAppointsByEmployeeLoginCode(String loginCode) throws AppointmentNotFoundException {
        var appointments = appointmentRepository.findAppointmentsByEmployeeLoginCodeOrderByAppointmentDate(loginCode)
                .orElseThrow(() -> new AppointmentNotFoundException("No appointments found for " + loginCode));
        return getCorrectAppointments(appointments);
    }

    @Override
    public List<Appointment> getAppointmentsNoEmployee(String token) throws AppointmentNotFoundException, NotAllowedException {
        checkAdmin(authenticationService, token);

        return appointmentRepository.findAppointmentsByEmployeeIsNull()
                .orElseThrow(() -> new AppointmentNotFoundException("No appointments found"));
    }

    @Override
    public List<Appointment> getAppointmentsByParcel(String parcel, String token) throws AppointmentNotFoundException, NotAllowedException {
        checkAdmin(authenticationService, token);

        return appointmentRepository.findAppointmentsByParcelContainingIgnoreCase(parcel)
                .orElseThrow(() -> new AppointmentNotFoundException("No appointments found"));
    }

    @Override
    public Appointment createAppointment(CreateAppointmentRequest createAppointmentRequest, String token) throws NotAllowedException, AppointmentNotFoundException {
        checkAdmin(authenticationService, token);
        appointmentRepository.save(new Appointment()
                .setParcel(createAppointmentRequest.getParcel())
                .setNote(createAppointmentRequest.getNote())
                .setAppointmentDate(createAppointmentRequest.getAppointmentDate()))
                .setStakeholders(createAppointmentRequest.getStakeholders());

        return appointmentRepository.findAppointmentByParcel(createAppointmentRequest.getParcel())
                .orElseThrow(() -> new AppointmentNotFoundException("No appointment found"));
    }

    @Override
    public void saveAppointment(Appointment appointment) {
        appointmentRepository.save(appointment);
    }

    @Override
    public void updateAppointment(long id, UpdateAppointmentRequest updateAppointmentRequest) throws AppointmentNotFoundException {
        var appointment = appointmentRepository.findById(id)
                .orElseThrow(() -> new AppointmentNotFoundException("No appointment with " + id + " found"));

        if (appointment.getUpdatedDate() == null) {
            appointment.setUpdatedDate(LocalDateTime.now());
        }
        appointment.setNote(updateAppointmentRequest.getNote());
        appointment.setMeasurements(updateAppointmentRequest.getMeasurements());

        var stakeholders = appointment.getStakeholders();
        for (var i = 0; i < updateAppointmentRequest.getStakeholderPresent().size(); i++) {
            stakeholders.get(i).setPresent(updateAppointmentRequest.getStakeholderPresent().get(i));
        }

        for (var i = 0; i < updateAppointmentRequest.getAuthorizers().size(); i++) {
            stakeholders.get(i).setAuthorizer(updateAppointmentRequest.getAuthorizers().get(i));
        }

        appointment.setStakeholders(stakeholders);
        appointment.setAddedFiles(updateAppointmentRequest.getAddedFiles());

        appointment.setUnanimous(updateAppointmentRequest.isUnanimous());
        appointment.setCompatible(updateAppointmentRequest.isCompatible());

        appointmentRepository.save(appointment);
    }
}

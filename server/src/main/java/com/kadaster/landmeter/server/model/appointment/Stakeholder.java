package com.kadaster.landmeter.server.model.appointment;

import javax.persistence.*;

import java.time.LocalDateTime;
import java.util.Date;


@Entity
public class Stakeholder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;

    private String email;

    private String phoneNumber;

    private LocalDateTime birthDate;

    @OneToOne(cascade = CascadeType.ALL)
    private Address address;

    @OneToOne(cascade = CascadeType.ALL)
    private Authorizer authorizer;

    private boolean present;

    private String title;

    private boolean munacipility;

    public Stakeholder() {
    }

    public long getId() {
        return id;
    }

    public Stakeholder setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Stakeholder setName(String name) {
        this.name = name;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Stakeholder setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Stakeholder setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public LocalDateTime getBirthDate() {
        return birthDate;
    }

    public Stakeholder setBirthDate(LocalDateTime birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public Address getAddress() {
        return address;
    }

    public Stakeholder setAddress(Address address) {
        this.address = address;
        return this;
    }

    public boolean isPresent() {
        return present;
    }

    public Stakeholder setPresent(boolean present) {
        this.present = present;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Stakeholder setTitle(String title) {
        this.title = title;
        return this;
    }

    public boolean isMunacipility() {
        return munacipility;
    }

    public Stakeholder setMunacipility(boolean munacipility) {
        this.munacipility = munacipility;
        return this;
    }

    public Authorizer getAuthorizer() {
        return authorizer;
    }

    public Stakeholder setAuthorizer(Authorizer authorizer) {
        this.authorizer = authorizer;
        return this;
    }
}

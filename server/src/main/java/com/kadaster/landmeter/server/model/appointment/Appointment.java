package com.kadaster.landmeter.server.model.appointment;

import com.kadaster.landmeter.server.model.Employee;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class Appointment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String parcel;

    private String mapFile;

    @OneToMany(cascade = CascadeType.ALL)
    private List<AppointmentFile> files;

    @OneToMany(cascade = CascadeType.ALL)
    private List<AppointmentFile> addedFiles;

    private LocalDateTime createdDate;

    private LocalDateTime appointmentDate;

    private LocalDateTime appointmentEndDate;

    private LocalDateTime updatedDate;

    private String note;

    @OneToOne(cascade = CascadeType.ALL)
    private Address address;

    private String measurements;

    @OneToOne(cascade = CascadeType.ALL)
    private Deed deed;

    @OneToOne
    private Employee employee;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Stakeholder> stakeholders;

    private int type;

    private String description;

    private boolean unanimous;

    private boolean compatible;

    public Appointment() {

    }

    public long getId() {
        return id;
    }

    public Appointment setId(long id) {
        this.id = id;
        return this;
    }

    public String getParcel() {
        return parcel;
    }

    public Appointment setParcel(String parcel) {
        this.parcel = parcel;
        return this;
    }

    public String getMapFile() {
        return mapFile;
    }

    public Appointment setMapFile(String mapFile) {
        this.mapFile = mapFile;
        return this;
    }

    public List<AppointmentFile> getFiles() {
        return files;
    }

    public Appointment setFiles(List<AppointmentFile> files) {
        this.files = files;
        return this;
    }

    public List<AppointmentFile> getAddedFiles() {
        return addedFiles;
    }

    public Appointment setAddedFiles(List<AppointmentFile> addedFiles) {
        this.addedFiles = addedFiles;
        return this;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public Appointment setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public LocalDateTime getAppointmentDate() {
        return appointmentDate;
    }

    public Appointment setAppointmentDate(LocalDateTime appointmentDate) {
        this.appointmentDate = appointmentDate;
        return this;
    }

    public LocalDateTime getAppointmentEndDate() {
        return appointmentEndDate;
    }

    public Appointment setAppointmentEndDate(LocalDateTime appointmentEndDate) {
        this.appointmentEndDate = appointmentEndDate;
        return this;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public Appointment setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public String getNote() {
        return note;
    }

    public Appointment setNote(String note) {
        this.note = note;
        return this;
    }

    public Address getAddress() {
        return address;
    }

    public Appointment setAddress(Address address) {
        this.address = address;
        return this;
    }

    public String getMeasurements() {
        return measurements;
    }

    public Appointment setMeasurements(String measurements) {
        this.measurements = measurements;
        return this;
    }

    public Deed getDeed() {
        return deed;
    }

    public Appointment setDeed(Deed deed) {
        this.deed = deed;
        return this;
    }

    public Employee getEmployee() {
        return employee;
    }

    public Appointment setEmployee(Employee employee) {
        this.employee = employee;
        return this;
    }

    public List<Stakeholder> getStakeholders() {
        return stakeholders;
    }

    public Appointment setStakeholders(List<Stakeholder> stakeholders) {
        this.stakeholders = stakeholders;
        return this;
    }

    public int getType() {
        return type;
    }

    public Appointment setType(int type) {
        this.type = type;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Appointment setDescription(String description) {
        this.description = description;
        return this;
    }

    public boolean isUnanimous() {
        return unanimous;
    }

    public Appointment setUnanimous(boolean unanimous) {
        this.unanimous = unanimous;
        return this;
    }

    public boolean isCompatible() {
        return compatible;
    }

    public Appointment setCompatible(boolean compatible) {
        this.compatible = compatible;
        return this;
    }
}

package com.kadaster.landmeter.server.controller;

import com.kadaster.landmeter.server.exception.FileNotFoundException;
import com.kadaster.landmeter.server.exception.InvalidTokenException;
import com.kadaster.landmeter.server.service.auth.AuthenticationService;
import com.kadaster.landmeter.server.service.download.DownloadService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/download")
public class DownloadController {

    private final DownloadService downloadService;

    private final AuthenticationService authenticationService;

    @Autowired
    public DownloadController(DownloadService downloadService, AuthenticationService authenticationService) {
        this.downloadService = downloadService;
        this.authenticationService = authenticationService;
    }

    @GetMapping(value = "/file/{file_name}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource getFile(@Valid @PathVariable("file_name") String fileName, @ApiParam("Appointment id") @Valid @RequestHeader("id") long id, @RequestHeader("Authorization") String token) throws FileNotFoundException, InvalidTokenException {
        authenticationService.verify(token);
        return new FileSystemResource(downloadService.getFilePath(id, fileName));
    }

    @GetMapping(value = "/profile/{employee_code}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public FileSystemResource getProfile(@Valid @PathVariable("employee_code") String employeeCode, @RequestHeader("Authorization") String token) throws FileNotFoundException, InvalidTokenException {
        authenticationService.verify(token);
        return new FileSystemResource(downloadService.getProfileFilePath(employeeCode));
    }
}

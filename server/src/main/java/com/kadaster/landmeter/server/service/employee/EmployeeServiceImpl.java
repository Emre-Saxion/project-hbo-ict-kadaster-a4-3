package com.kadaster.landmeter.server.service.employee;

import com.kadaster.landmeter.server.dto.request.AvailableDateRequest;
import com.kadaster.landmeter.server.dto.request.CreateEmployeeRequest;
import com.kadaster.landmeter.server.exception.DuplicateEmployeeCodeException;
import com.kadaster.landmeter.server.exception.EmployeeNotFoundException;
import com.kadaster.landmeter.server.exception.NotAllowedException;
import com.kadaster.landmeter.server.model.Employee;
import com.kadaster.landmeter.server.repo.EmployeeRepository;
import com.kadaster.landmeter.server.service.auth.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.kadaster.landmeter.server.util.AdminCheck.checkAdmin;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    private final AuthenticationService authenticationService;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository, AuthenticationService authenticationService) {
        this.employeeRepository = employeeRepository;
        this.authenticationService = authenticationService;
    }

    @Override
    public Optional<Employee> getByLoginCode(String loginCode) {
        return employeeRepository.findFirstByLoginCode(loginCode);
    }

    @Override
    public Employee getByLoginCodeChecked(String loginCode) throws EmployeeNotFoundException {
        return getByLoginCode(loginCode).orElseThrow(() -> new EmployeeNotFoundException("No employee with code " + loginCode + " found"));
    }

    @Override
    public Employee getById(long id) throws EmployeeNotFoundException {
        return employeeRepository.findById(id)
                .orElseThrow(() -> new EmployeeNotFoundException("Employee with id " + id + " doesn't exist"));
    }

    @Override
    public Employee saveEmployee(CreateEmployeeRequest request) {
        var salt = BCrypt.gensalt();
        var hashed = BCrypt.hashpw(request.getPassword(), salt);

        var employeeToAdd = new Employee()
                .setFirstName(request.getFirstName())
                .setLastName(request.getLastName())
                .setEmail(request.getEmail())
                .setPhoneNumber(request.getPhoneNumber())
                .setLoginCode(request.getLoginCode())
                .setPasswordHash(hashed)
                .setAdmin(request.isAdmin());

        employeeRepository.save(employeeToAdd);
        return employeeToAdd;
    }

    @Override
    public void createEmployee(CreateEmployeeRequest request, String token) throws DuplicateEmployeeCodeException, NotAllowedException {
        checkAdmin(authenticationService, token);

        var employeeFound = getByLoginCode(request.getLoginCode());
        if (employeeFound.isPresent()) {
            throw new DuplicateEmployeeCodeException("Employee with code " + request.getLoginCode() + " already exists");
        }

        saveEmployee(request);
    }

    @Override
    public void deleteAndCreateEmployee(CreateEmployeeRequest request, String token) throws NotAllowedException {
        checkAdmin(authenticationService, token);

        getByLoginCode(request.getLoginCode()).ifPresent(employeeRepository::delete);
        saveEmployee(request);
    }

    @Override
    public void deleteEmployeeByLoginCode(String loginCode, String token) throws EmployeeNotFoundException, NotAllowedException {
        checkAdmin(authenticationService, token);

        var employeeFound = getByLoginCodeChecked(loginCode);
        employeeRepository.delete(employeeFound);
    }

    @Override
    public List<Employee> getAllEmployees(String token) throws NotAllowedException {
        checkAdmin(authenticationService, token);

        return employeeRepository.findAll();
    }

    @Override
    public void updateDates(AvailableDateRequest availableDateRequest, String token) throws EmployeeNotFoundException {
        var employeeId = authenticationService.getEmployeeIdFromToken(token);

        var employee = getById(employeeId);
        employee.setAvailableDates(availableDateRequest.getAvailableDates());
        employeeRepository.save(employee);
    }
}

package com.kadaster.landmeter.server.config;

import com.kadaster.landmeter.server.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(DuplicateEmployeeCodeException.class)
    public ResponseEntity<Object> handleDuplicateEmployeeCode(DuplicateEmployeeCodeException e) {
        return createWith(HttpStatus.CONFLICT, e.getMessage());
    }

    @ExceptionHandler(EmployeeNotFoundException.class)
    public ResponseEntity<Object> handleEmployeeNotFound(EmployeeNotFoundException e) {
        return createWith(HttpStatus.NOT_FOUND, e.getMessage());
    }

    @ExceptionHandler(NotAllowedException.class)
    public ResponseEntity<Object> handleNotAllowedException(NotAllowedException e) {
        return createWith(HttpStatus.FORBIDDEN, e.getMessage());
    }

    @ExceptionHandler(InvalidCredentialsException.class)
    public ResponseEntity<Object> handleInvalidCredentialsException(InvalidCredentialsException e) {
        return createWith(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage());
    }

    @ExceptionHandler(AppointmentNotFoundException.class)
    public ResponseEntity<Object> handleAppointmentNotFoundException(AppointmentNotFoundException e) {
        return createWith(HttpStatus.NOT_FOUND, e.getMessage());
    }

    @ExceptionHandler(FileNotFoundException.class)
    public ResponseEntity<Object> handleFileNotFoundException(FileNotFoundException e) {
        return createWith(HttpStatus.NOT_FOUND, e.getMessage());
    }

    @ExceptionHandler(InvalidTokenException.class)
    public ResponseEntity<Object> handleInvalidTokenException(InvalidTokenException e) {
        return createWith(HttpStatus.UNAUTHORIZED, e.getMessage());
    }

    private ResponseEntity<Object> createWith(HttpStatus status, Object message) {
        return ResponseEntity.status(status).body(message);
    }
}

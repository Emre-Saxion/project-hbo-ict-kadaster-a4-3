package com.kadaster.landmeter.server.dto.request;

import com.kadaster.landmeter.server.model.AvailableDate;

import javax.validation.constraints.NotNull;
import java.util.List;

public class AvailableDateRequest {

    @NotNull
    private List<AvailableDate> availableDates;

    public AvailableDateRequest() {

    }

    public List<AvailableDate> getAvailableDates() {
        return availableDates;
    }

    public AvailableDateRequest setAvailableDates(List<AvailableDate> availableDates) {
        this.availableDates = availableDates;
        return this;
    }
}

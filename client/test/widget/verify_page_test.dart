import 'package:client/model/user.dart';
import 'package:client/pages/verify_code_page.dart';
import 'package:client/storage/storage_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';

Widget _testableVerificationCodePage() {
  return MaterialApp(
    home: VerifyCodePage(),
  );
}

final user = User("token", "123456", "first", "last", "062099999",
    "test@email.com", false, []);

void main() {
  setUpAll(() async {
    await initStore();

    final userStore = StorageManager.getInstance().getStore("user");
    userStore.insert(user);
  });

  testWidgets("test shows full name", (tester) async {
    await tester.pumpWidget(_testableVerificationCodePage());
    await tester.pumpAndSettle();

    expect(
        find.text("Hallo ${user.firstName} ${user.lastName}"), findsOneWidget);
  });

  testWidgets("test wrong login code entered", (tester) async {
    await tester.pumpWidget(_testableVerificationCodePage());

    final verificationCodeInputField =
        find.byKey(Key("verification_code_field"));
    await tester.enterText(verificationCodeInputField, '222222');
    await tester.pumpAndSettle();

    final errorWrongCode = find.byKey(Key("alert_dialog"));
    expect(errorWrongCode, findsOneWidget);
  });

  testWidgets("test correct login code entered", (tester) async {
    await tester.pumpWidget(_testableVerificationCodePage());

    final verificationCodeInputField =
        find.byKey(Key("verification_code_field"));
    await tester.enterText(verificationCodeInputField, user.loginCode);
    await tester.pump(const Duration(milliseconds: 1500));

    final okIcon = find.byIcon(Icons.check_circle);
    expect(okIcon, findsOneWidget);
  });

  testWidgets("test shows dialog", (tester) async {
    await tester.pumpWidget(_testableVerificationCodePage());

    final logOffButton = find.byKey(Key("verify_code_log_off"));
    await tester.tap(logOffButton);
    await tester.pumpAndSettle();

    final logOffDialog = find.byType(AlertDialog);
    expect(logOffDialog, findsOneWidget);
  });

  testWidgets("test closes dialog", (tester) async {
    await tester.pumpWidget(_testableVerificationCodePage());

    final logOffButton = find.byKey(Key("verify_code_log_off"));
    await tester.tap(logOffButton);
    await tester.pumpAndSettle();

    final closeButton = find.text("Sluiten");
    await tester.tap(closeButton);
    await tester.pumpAndSettle();

    expect(find.byType(AlertDialog), findsNothing);
  });

  testWidgets("test logs off", (tester) async {
    await tester.pumpWidget(_testableVerificationCodePage());

    final logOffButton = find.byKey(Key("verify_code_log_off"));
    await tester.tap(logOffButton);
    await tester.pumpAndSettle();

    final okButton = find.text("Oke");
    await tester.tap(okButton);
    await tester.pumpAndSettle();

    expect(StorageManager.getInstance().getStore("user").get(), null);
  });
}

Future<void> initStore() async {
  TestWidgetsFlutterBinding.ensureInitialized();
  SharedPreferences.setMockInitialValues({});
  var preferences = await SharedPreferences.getInstance();
  StorageManager.getInstance().preferences = preferences;
}

import 'package:client/constants/kadaster_colors.dart';
import 'package:client/model/available_date.dart';
import 'package:client/model/user.dart';
import 'package:client/pages/available_page.dart';
import 'package:client/storage/storage_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';

Widget _testableAvailablePage() {
  return MaterialApp(
    home: AvailablePage(),
  );
}

final now = DateTime.now();

final user = User(
  "token",
  "123456",
  "first",
  "last",
  "062099999",
  "test@email.com",
  false,
  [
    AvailableDate(
        DateTime(now.year, now.month, now.day, 0, 0, 0, 0, 0), true, ""),
    AvailableDate(
        DateTime(now.year, now.month, now.day + 1, 0, 0, 0, 0, 0), false, ""),
    AvailableDate(DateTime(now.year, now.month, now.day + 2, 0, 0, 0, 0, 0),
        false, "test"),
  ],
);

void main() {
  setUpAll(() async {
    await initStore();

    final userStore = StorageManager.getInstance().getStore("user");
    userStore.insert(user);
  });

  testWidgets("test shows calendar", (tester) async {
    await tester.pumpWidget(_testableAvailablePage());

    final calendar = find.byType(CalendarCarousel);
    expect(calendar, findsOneWidget);
  });

  testWidgets("test shows unavailable", (tester) async {
    await tester.pumpWidget(_testableAvailablePage());

    final unavailableButton = find.byKey(Key("unavailable_button"));
    expect(unavailableButton, findsOneWidget);
  });

  testWidgets("test shows available", (tester) async {
    await tester.pumpWidget(_testableAvailablePage());

    final availableButton = find.byKey(Key("available_button"));
    expect(availableButton, findsOneWidget);
  });

  testWidgets("test click unavailable", (tester) async {
    await tester.pumpWidget(_testableAvailablePage());

    final unavailableButton = find.byKey(Key("unavailable_button"));
    await tester.tap(unavailableButton);
    await tester.pumpAndSettle();

    expect((unavailableButton.evaluate().single.widget as FlatButton).textColor,
        KadasterColors.mainColor);
  });

  testWidgets("test click available", (tester) async {
    await tester.pumpWidget(_testableAvailablePage());

    final unavailableButton = find.byKey(Key("unavailable_button"));
    await tester.tap(unavailableButton);
    await tester.pumpAndSettle();

    final availableButton = find.byKey(Key("available_button"));
    await tester.tap(availableButton);
    await tester.pumpAndSettle();

    expect((availableButton.evaluate().single.widget as FlatButton).textColor,
        KadasterColors.mainColor);
  });
}

Future<void> initStore() async {
  TestWidgetsFlutterBinding.ensureInitialized();
  SharedPreferences.setMockInitialValues({});
  var preferences = await SharedPreferences.getInstance();
  StorageManager.getInstance().preferences = preferences;
}

import 'package:client/model/user.dart';
import 'package:client/storage/storage_manager.dart';
import 'package:client/widgets/appointments/appointments_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';

final scaffoldKey = GlobalKey<ScaffoldState>();

Widget _testableAppointmentsDrawer() {
  return MaterialApp(
    home: Scaffold(
      key: scaffoldKey,
      drawer: AppointmentsDrawer(),
    ),
  );
}

final user = User(
    "token", "123456", "first", "last", "062099999", "test@email.com", false, []);
final userAdmin = User(
    "token", "12345678", "first", "last", "062099999", "test@email.com", true, []);

void main() {
  setUpAll(() async {
    await initStore();

    final userStore = StorageManager.getInstance().getStore("user");
    userStore.insert(user);
  });
  testWidgets("test shows full name", (tester) async {
    await tester.pumpWidget(_testableAppointmentsDrawer());

    scaffoldKey.currentState.openDrawer();
    await tester.pump();
    await tester.pumpAndSettle();

    expect(find.text("${user.firstName} ${user.lastName}"), findsOneWidget);
  });
  testWidgets("test shows log off dialog", (tester) async {
    await tester.pumpWidget(_testableAppointmentsDrawer());

    scaffoldKey.currentState.openDrawer();
    await tester.pump();
    await tester.pumpAndSettle();

    final logOffListTile = find.byKey(Key("log_off_list_tile"));
    await tester.tap(logOffListTile);
    await tester.pumpAndSettle();

    final logOffDialog = find.byType(AlertDialog);
    expect(logOffDialog, findsOneWidget);
  });

  testWidgets("test shows closes log off dialog", (tester) async {
    await tester.pumpWidget(_testableAppointmentsDrawer());

    scaffoldKey.currentState.openDrawer();
    await tester.pump();
    await tester.pumpAndSettle();

    final logOffListTile = find.byKey(Key("log_off_list_tile"));
    await tester.tap(logOffListTile);
    await tester.pumpAndSettle();

    final closeButton = find.text("Sluiten");
    await tester.tap(closeButton);
    await tester.pumpAndSettle();

    expect(find.byType(AlertDialog), findsNothing);
  });

  testWidgets("test actual log off", (tester) async {
    await tester.pumpWidget(_testableAppointmentsDrawer());

    scaffoldKey.currentState.openDrawer();
    await tester.pump();
    await tester.pumpAndSettle();

    final logOffListTile = find.byKey(Key("log_off_list_tile"));
    await tester.tap(logOffListTile);
    await tester.pumpAndSettle();

    final logOffButton = find.text("Oke");
    await tester.tap(logOffButton);
    await tester.pumpAndSettle();

    expect(StorageManager.getInstance().getStore("user").get(), null);
  });
}

Future<void> initStore() async {
  TestWidgetsFlutterBinding.ensureInitialized();
  SharedPreferences.setMockInitialValues({});
  var preferences = await SharedPreferences.getInstance();
  StorageManager.getInstance().preferences = preferences;
}

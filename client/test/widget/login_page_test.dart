import 'package:client/pages/login_page.dart';
import 'package:client/widgets/login/login_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

Widget _testableLoginPage() {
  return MaterialApp(
    home: LoginPage(),
  );
}

void main() {
  testWidgets('test empty credentials login', (tester) async {
    await tester.pumpWidget(_testableLoginPage());

    final loginButton = find.byKey(Key("login_button"));
    await tester.tap(loginButton);
    await tester.pumpAndSettle();

    final errorTextLogin = find.text(LoginCodeValidation.validate(""));
    final errorTextPassword = find.text(PasswordValidation.validate(""));
    expect(errorTextLogin, findsOneWidget);
    expect(errorTextPassword, findsOneWidget);
  });

  testWidgets('test login code not 6 characters', (tester) async {
    await tester.pumpWidget(_testableLoginPage());

    const code = "12345";

    final loginCodeTextFormField = find.byKey(Key("login_code_field"));
    await tester.enterText(loginCodeTextFormField, code);

    final loginButton = find.byKey(Key("login_button"));
    await tester.tap(loginButton);
    await tester.pumpAndSettle();

    final errorTextLogin = find.text(LoginCodeValidation.validate(code));
    expect(errorTextLogin, findsOneWidget);
  });

  testWidgets('test login code not numeric', (tester) async {
    await tester.pumpWidget(_testableLoginPage());

    const code = "yes123";

    final loginCodeTextFormField = find.byKey(Key("login_code_field"));
    await tester.enterText(loginCodeTextFormField, code);

    final loginButton = find.byKey(Key("login_button"));
    await tester.tap(loginButton);
    await tester.pumpAndSettle();

    final errorTextLogin = find.text(LoginCodeValidation.validate(code));
    expect(errorTextLogin, findsNothing);
  });

  testWidgets('test password not 6 characters', (tester) async {
    await tester.pumpWidget(_testableLoginPage());

    const password = "qwert";

    final passwordTextFormField = find.byKey(Key("password_field"));
    await tester.enterText(passwordTextFormField, password);

    final loginButton = find.byKey(Key("login_button"));
    await tester.tap(loginButton);
    await tester.pumpAndSettle();

    final errorTextPassword = find.text(PasswordValidation.validate(password));
    expect(errorTextPassword, findsOneWidget);
  });
}

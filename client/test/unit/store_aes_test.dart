import 'package:client/storage/store_aes.dart';
import 'package:encrypt/encrypt.dart';
import 'package:flutter_test/flutter_test.dart';

const toEncrypt = "test";

void main() {
  test("does encrypt", () {
    final StoreAES storeAES = StoreAES(Key.fromUtf8("1pHoUCffNvaUmNB4l7zo30FyA26TVAET"));
    expect(storeAES.encrypt(toEncrypt), equals("/9/EsNr+rNZsFj4UUN0rzw=="));
  });

  test("does decrypt", () {
    final StoreAES storeAES = StoreAES(Key.fromUtf8("1pHoUCffNvaUmNB4l7zo30FyA26TVAET"));
    expect(storeAES.decrypted("/9/EsNr+rNZsFj4UUN0rzw=="), "test");
  });
}

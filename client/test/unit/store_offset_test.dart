import 'package:client/model/offset_info.dart';
import 'package:client/storage/storage_manager.dart';
import 'package:client/storage/store_offset.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';

final offsetInfo = [
  OffsetInfo(1, Offset(1.0, 1.0), Colors.blue, 3.0, false),
  OffsetInfo(1, Offset(1.0, 1.0), Colors.blue, 2.0, false)
];

void main() {
  setUpAll(() async {
    await initStore();
  });

  test("Stores offsets", () async {
    StoreOffsetManager.getInstance().storeOffsets(1, offsetInfo);

    expect(StoreOffsetManager.getInstance().getOffsets(1)[0].range, 3.0);
    expect(StoreOffsetManager.getInstance().getOffsets(1)[1].range, 2.0);
  });

  test("Deletes offsets", () async {
    StoreOffsetManager.getInstance().storeOffsets(1, offsetInfo);

    StoreOffsetManager.getInstance().deleteOffsets(1);
    expect(StoreOffsetManager.getInstance().getOffsets(1), []);
  });
}

Future<void> initStore() async {
  TestWidgetsFlutterBinding.ensureInitialized();
  SharedPreferences.setMockInitialValues({});
  var preferences = await SharedPreferences.getInstance();
  StorageManager.getInstance().preferences = preferences;
}

import 'package:client/model/user.dart';
import 'package:client/storage/storage_manager.dart';
import 'package:client/widgets/verify/verify_code_input.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';

final user = User(
    "token", "123456", "first", "last", "062099999", "test@email.com", false, []);

void main() {
  setUpAll(() async {
    await initStore();
  });

  test("test invalid code", () async {
    final instance = StorageManager.getInstance();
    instance.getStore("user").insert(user);

    expect(VerifyCodeInputValidator.validate("123455", user), false);
  });

  test("test valid code", () async {
    final instance = StorageManager.getInstance();
    instance.getStore("user").insert(user);

    expect(VerifyCodeInputValidator.validate("123456", user), true);
  });
}

Future<void> initStore() async {
  TestWidgetsFlutterBinding.ensureInitialized();
  SharedPreferences.setMockInitialValues({});
  var preferences = await SharedPreferences.getInstance();
  StorageManager.getInstance().preferences = preferences;
}

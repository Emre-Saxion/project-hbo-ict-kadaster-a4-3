import 'package:client/model/appointment/address.dart';
import 'package:client/model/appointment/appointment.dart';
import 'package:client/model/appointment/deed.dart';
import 'package:client/model/user.dart';
import 'package:client/storage/storage_manager.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';

final user = User(
    "token", "code", "first", "last", "062099999", "test@email.com", false, []);

final appointments = [
  Appointment(1, "", "", [], [], DateTime.now(), DateTime.now(), DateTime.now(), "",
      Address("", "", "", "", "", ""), "", Deed(1, "", ""), [], 0, "", false, false),
  Appointment(2, "", "", [], [], DateTime.now(), DateTime.now(), DateTime.now(), "",
      Address("", "", "", "", "", ""), "", Deed(1, "", ""), [], 0, "", false, false),
];

void main() {
  setUpAll(() async {
    await initStore();
  });

  test("test saves user", () async {
    final instance = StorageManager.getInstance();
    instance.getStore("user").insert(user);

    expect(instance.getStore("user").get().token, user.token);
  });

  test("test deletes user", () async {
    final instance = StorageManager.getInstance();
    instance.getStore("user").insert(user);
    instance.getStore("user").delete();

    expect(instance.getStore("user").get(), null);
  });

  test("test saves appointments", () async {
    final instance = StorageManager.getInstance();
    instance.getStore("appointment").insert(appointments);

    expect(instance.getStore("appointment").get()[0].id, 1);
    expect(instance.getStore("appointment").get()[1].id, 2);
  });
}

Future<void> initStore() async {
  TestWidgetsFlutterBinding.ensureInitialized();
  SharedPreferences.setMockInitialValues({});
  var preferences = await SharedPreferences.getInstance();
  StorageManager.getInstance().preferences = preferences;
}

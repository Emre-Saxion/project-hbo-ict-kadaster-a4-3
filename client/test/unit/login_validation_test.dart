import 'package:client/widgets/login/login_form.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test("test empty login code", () {
    final result = LoginCodeValidation.validate("");
    expect(result, "Gebruikersnaam mag niet leeg zijn");
  });

  test("test only numbers allowed", () {
    final result = LoginCodeValidation.validate("qwerty");
    expect(result, "Gebruikersnaam mag alleen getallen bevatten");
  });

  test("test length does not equal 6 numbers", () {
    final result = LoginCodeValidation.validate("12345");
    expect(result, "Gebruikersnaam moet minimaal 6 cijfers lang zijn");
  });

  test("test correct login code", () {
    final result = LoginCodeValidation.validate("123456");
    expect(result, null);
  });

  test("test empty password", () {
    final result = PasswordValidation.validate("");
    expect(result, "Wachtwoord mag niet leeg zijn");
  });

  test("test password is less than 8 characters", () {
    final result = PasswordValidation.validate("qwert");
    expect(result, "Wachtwoord moet minimaal 8 letters lang zijn");
  });

  test("test correct password", () {
    final result = PasswordValidation.validate("qwertyui");
    expect(result, null);
  });
}

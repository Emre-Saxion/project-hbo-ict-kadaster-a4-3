import 'package:background_fetch/background_fetch.dart';
import 'package:client/main_app.dart';
import 'package:client/storage/storage_manager.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'constants/constants.dart';
import 'file/file_manager.dart';
import 'model/appointment/appointment_file.dart';
import 'model/request.dart';
import 'model/user.dart';

void backgroundFetchHeadlessTask(String taskId) async {
  print("[BackgroundFetch] Headless event received: $taskId");

  SharedPreferences preferences = await SharedPreferences.getInstance();
  StorageManager.getInstance().preferences = preferences;

  final List<Request> requests =
  StorageManager.getInstance().getStore("request").get();

  print("Found ${requests.length} old requests");

  final User user = StorageManager.getInstance().getStore("user").get();

  try {
    if (requests.length > 0 && user != null) {
      for (Request request in requests) {
        await _sendUpdateAppointment(request, user);

        if (request.addedFiles.length > 0) {
          await _uploadAddedFiles(request, request.addedFiles, user);
        }

        if (!await FileManager.getInstance()
            .appointmentMapFileExistsById(request.id)) {
          continue;
        }

        await _sendMap(request.id, user);
      }
    }

    StorageManager.getInstance().getStore("request").delete();
  } catch(e) {

  }

  BackgroundFetch.finish(taskId);
}

Future<void> _sendUpdateAppointment(Request request, User user) async {
  await Dio(
      BaseOptions(connectTimeout: Constants.CONNECTION_TIME_OUT_MS))
      .post(
    Constants.URL + "/appointment/update/${request.id}",
    data: {
      "note": request.note,
      "stakeholderPresent": request.stakeholderPresent,
      "authorizers": request.authorizers,
      "measurements": request.measurements,
      "addedFiles": request.addedFiles.map((e) => e.toJson()).toList(),
      "unanimous": request.unanimous,
      "compatible": request.compatible,
    },
    options: Options(
      headers: {
        "authorization": user.token,
      },
    ),
  );
}

Future<void> _uploadAddedFiles(Request request, List<AppointmentFile> files, User user) async {
  final List<String> paths = [];
  for (AppointmentFile appointmentFile in files) {
    final String path = await FileManager.getInstance()
        .appointmentFileLocationById(request.id, appointmentFile);
    paths.add(path);
  }

  final formData = FormData();
  formData.files.addAll(paths
      .map((e) => MapEntry("files", MultipartFile.fromFileSync(e)))
      .toList());

  await Dio(BaseOptions(
      connectTimeout: Constants.FILE_UPLOAD_CONNECTION_TIME_OUT_MS))
      .post(
    Constants.URL + "/upload/multi",
    data: formData,
    options: Options(
      headers: {"authorization": user.token, "id": request.id},
    ),
  );
}

Future<void> _sendMap(int id, User user) async {
  final path =
  await FileManager.getInstance().appointmentMapFileLocationById(id);
  final file = await MultipartFile.fromFile(path);

  final formData = FormData();
  formData.files.add(MapEntry("file", file));

  await Dio(BaseOptions(
      connectTimeout: Constants.FILE_UPLOAD_CONNECTION_TIME_OUT_MS))
      .post(
    Constants.URL + "/upload/map",
    data: formData,
    options: Options(
      headers: {"authorization": user.token, "id": id},
    ),
  );
}

void main() async {
  //This is required because the main function is async
  WidgetsFlutterBinding.ensureInitialized();

  //Initializing the storage manager
  final preferences = await SharedPreferences.getInstance();
  StorageManager.getInstance().preferences = preferences;

  //Initializing the logger, all logs will be put in the listen function
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((record) {
    print('[${record.loggerName}] ${record.level.name}: ${record.time}: ${record.message}');
  });

  runApp(MainApp());

  BackgroundFetch.registerHeadlessTask(backgroundFetchHeadlessTask);
}

import 'package:client/bloc/map/map_drawing_bloc.dart';
import 'package:client/bloc/map/map_input_bloc.dart';
import 'package:client/constants/constants.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:client/file/file_manager.dart';
import 'package:client/model/appointment/appointment.dart';
import 'package:client/model/appointment/appointment_file.dart';
import 'package:client/model/offset_info.dart';
import 'package:client/model/request.dart';
import 'package:client/model/user.dart';
import 'package:client/pages/appointments_page.dart';
import 'package:client/pages/history_page.dart';
import 'package:client/storage/storage_manager.dart';
import 'package:client/util/alert_util.dart';
import 'package:client/util/error_message_http.dart';
import 'package:client/util/internet_connection.dart';
import 'package:client/util/no_inkwell_scroll.dart';
import 'package:client/util/screen_util.dart';
import 'package:client/widgets/map/input/map_add_files.dart';
import 'package:client/widgets/map/input/map_files.dart';
import 'package:client/widgets/map/input/map_info.dart';
import 'package:client/widgets/map/input/map_measurements.dart';
import 'package:client/widgets/map/input/map_notes.dart';
import 'package:client/widgets/map/input/map_preview.dart';
import 'package:client/widgets/map/input/map_review.dart';
import 'package:client/widgets/map/input/map_stakeholders.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

class MapPage extends StatefulWidget {
  final Appointment _appointment;

  final MapDrawingBloc _mapDrawingBloc;

  final MapInputBloc _mapInputBloc;

  final bool _history;

  MapPage(this._appointment, this._mapDrawingBloc, this._mapInputBloc,
      this._history);

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> with WidgetsBindingObserver {
  final Logger _logger = Logger("MapPage");

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  bool _saved = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  Orientation _previousOrientation;

  @override
  void didChangeMetrics() {
    final ScreenUtil screenUtil = ScreenUtil(context);
    //TODO FIX THIS
    if (screenUtil.isPortrait() &&
        _previousOrientation != Orientation.portrait) {
      _logger.info("Detected new orientation, orientation=portrait");
      for (OffsetInfo info in widget._mapDrawingBloc.offsetInfo) {
        info.offset = Offset(info.offset.dx - 5, info.offset.dy - 5);
      }
      _previousOrientation = Orientation.portrait;
    } else if (screenUtil.isLandScape() &&
        _previousOrientation != Orientation.landscape) {
      _logger.info("Detected new orientation, orientation=landscape");
      for (OffsetInfo info in widget._mapDrawingBloc.offsetInfo) {
        info.offset = Offset(info.offset.dx + 5, info.offset.dy + 5);
      }
      _previousOrientation = Orientation.portrait;
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_previousOrientation == null) {
      _previousOrientation = MediaQuery.of(context).orientation;
    }
    final ScreenUtil screenUtil = ScreenUtil(context);
    return WillPopScope(
      onWillPop: () async {
        if (!_saved) {
          await AlertUtil.sendConfirmationAlert(context, "Opslaan",
              "De afspraak is niet opgeslagen weet u zeker dat u terug wil gaan?",
              (context) {
            _pushReplacements(context);
            return true;
          }, close: "Annuleren");
          return false;
        }
        _pushReplacements(context);
        return true;
      },
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () async {
              if (!_saved) {
                await AlertUtil.sendConfirmationAlert(context, "Opslaan",
                    "De afspraak is niet opgeslagen weet u zeker dat u terug wil gaan?",
                    (context) {
                  _pushReplacements(context);
                }, close: "Annuleren");
                return;
              }
              _pushReplacements(context);
            },
          ),
          title: Text(widget._appointment.parcel),
        ),
        body: MultiProvider(
          providers: [
            ChangeNotifierProvider.value(
              value: widget._mapDrawingBloc,
            ),
            ChangeNotifierProvider.value(
              value: widget._mapInputBloc,
            ),
          ],
          child: OrientationBuilder(
            builder: (BuildContext context, Orientation orientation) {
              if (orientation == Orientation.landscape) {
                _logger.info("Loading landscape layout");
                return _landScape(screenUtil);
              }
              _logger.info("Loading portrait layout");
              return _portrait(screenUtil);
            },
          ),
        ),
      ),
    );
  }

  Widget _landScape(ScreenUtil screenUtil) {
    return ScrollConfiguration(
      behavior: NoInkWellScroll(),
      child: SingleChildScrollView(
        child: Column(
          children: [
            MapInfo(widget._appointment),
            MapStakeholders(widget._appointment, _scaffoldKey),
            MapFiles(widget._appointment),
            MapPreview(widget._appointment),
            MapMeasurements(),
            MapAddFiles(widget._appointment),
            MapNotes(),
            MapReview(widget._appointment),
            Center(
              child: _sendMapButton(),
            ),
          ],
        ),
      ),
    );
//    return Row(
//      children: [
//
//        VerticalDivider(),
//        Container(
//          width: screenUtil.getScreenWidthPercentage(50),
//          child: Stack(
//            children: [
//              RepaintBoundary(
//                key: _repaintBoundaryKey,
//                child: MapWidget(widget._appointment),
//              ),
//              Padding(
//                padding: const EdgeInsets.only(
//                    top: 8, bottom: 12, right: 12, left: 12),
//                child: MapButtons(),
//              ),
//              _mapButtons(),
//            ],
//          ),
//        ),
//      ],
//    );
  }

  Widget _portrait(ScreenUtil screenUtil) {
    return ScrollConfiguration(
      behavior: NoInkWellScroll(),
      child: ListView(
        shrinkWrap: true,
        children: [
          MapInfo(widget._appointment),
          MapStakeholders(widget._appointment, _scaffoldKey),
          MapFiles(widget._appointment),
          MapPreview(widget._appointment),
          MapMeasurements(),
          MapAddFiles(widget._appointment),
          MapNotes(),
          MapReview(widget._appointment),
          Center(
            child: _sendMapButton(),
          ),
        ],
      ),
    );
//    return Column(
//      crossAxisAlignment: CrossAxisAlignment.start,
//      children: [
//        Container(
//          height: screenUtil.getScreenHeightPercentage(50),
//          child: Stack(
//            children: [
//              RepaintBoundary(
//                key: _repaintBoundaryKey,
//                child: MapWidget(widget._appointment),
//              ),
//              Padding(
//                padding: const EdgeInsets.only(
//                    top: 8, bottom: 12, right: 12, left: 12),
//                child: MapButtons(),
//              ),
//              _mapButtons(),
//            ],
//          ),
//        ),
//        Divider(),
//        Expanded(
//          child:
//        ),
//      ],
//    );
  }

  Widget _sendMapButton() {
    return Consumer2(
      builder: (BuildContext context, MapDrawingBloc mapDrawingBloc,
          MapInputBloc mapInputBloc, Widget child) {
        return Column(
          children: [
            Padding(
              padding:
                  const EdgeInsets.only(left: 24.0, top: 24.0, right: 24.0),
              child: RaisedButton(
                onPressed: () async {
                  if (mapInputBloc.sendingMap) {
                    return;
                  }
                  _logger.info("Sending updated appointment to server");
                  mapInputBloc.sendingMap = true;

                  Future.delayed(const Duration(milliseconds: 50))
                      .then((t) async {
                    bool connected = await InternetConnection.connected();

                    if (connected) {
                      await _sendMap();
                      await _updateAppointment(mapInputBloc);
                      await _uploadAddedFiles(mapInputBloc);
                    } else {
                      _updateAppointmentLocal(mapInputBloc);
                      _storeRequestLocal(mapInputBloc);
                      _sendUpdatedAlert(mapInputBloc, "Geupdate",
                          "De afspraak wordt zo snel mogelijk geupdate");
                    }
                    _saved = true;
                  });
                },
                color: KadasterColors.secondaryColor,
                child: Text(
                  "Opslaan",
                  style: TextStyle(color: KadasterColors.WHITE_COLOR),
                ),
              ),
            ),
            if (mapInputBloc.sendingMap)
              Padding(
                padding: const EdgeInsets.only(top: 12.0),
                child: CircularProgressIndicator(
                  valueColor:
                      AlwaysStoppedAnimation<Color>(KadasterColors.mainColor),
                ),
              ),
            SizedBox(
              height: 36.0,
            ),
          ],
        );
      },
    );
  }

  Future<void> _uploadAddedFiles(MapInputBloc mapInputBloc) async {
    _logger.info(
        "Uploading added files found ${mapInputBloc.addedFiles.length} files");

    final List<String> paths = [];
    for (AppointmentFile appointmentFile in mapInputBloc.addedFiles) {
      final String path = await FileManager.getInstance()
          .appointmentFileLocation(widget._appointment, appointmentFile);
      paths.add(path);
    }

    final formData = FormData();
    formData.files.addAll(paths
        .map((e) => MapEntry("files", MultipartFile.fromFileSync(e)))
        .toList());

    try {
      final User user = StorageManager.getInstance().getStore("user").get();

      await Dio(BaseOptions(
              connectTimeout: Constants.FILE_UPLOAD_CONNECTION_TIME_OUT_MS))
          .post(
        Constants.URL + "/upload/multi",
        data: formData,
        options: Options(
          headers: {"authorization": user.token, "id": widget._appointment.id},
        ),
      );
    } catch (e, stacktrace) {
      _logger.severe("Error sending map message=${ErrorMessageHttp.message(e)}",
          e, stacktrace);
    }
  }

  Future<void> _sendMap() async {
    if (!await FileManager.getInstance()
        .appointmentMapFileExists(widget._appointment)) {
      _logger.info("No map found");
      return;
    }
    _logger.info("Sending map");
    if (!await FileManager.getInstance()
        .appointmentMapFileExists(widget._appointment)) {
      _logger.info("No map found");
      return;
    }
    final path = await FileManager.getInstance()
        .appointmentMapFileLocation(widget._appointment);
    final file = await MultipartFile.fromFile(path);

    final formData = FormData();
    formData.files.add(MapEntry("file", file));

    try {
      final User user = StorageManager.getInstance().getStore("user").get();

      await Dio(BaseOptions(
              connectTimeout: Constants.FILE_UPLOAD_CONNECTION_TIME_OUT_MS))
          .post(
        Constants.URL + "/upload/map",
        data: formData,
        options: Options(
          headers: {"authorization": user.token, "id": widget._appointment.id},
        ),
      );
    } catch (e, stacktrace) {
      _logger.severe("Error sending map message=${ErrorMessageHttp.message(e)}",
          e, stacktrace);
    }
  }

  Future<void> _updateAppointment(MapInputBloc mapInputBloc) async {
    _logger.info("Sending updated appointment");
    try {
      final User user = StorageManager.getInstance().getStore("user").get();

      await Dio(BaseOptions(connectTimeout: Constants.CONNECTION_TIME_OUT_MS))
          .post(
        Constants.URL + "/appointment/update/${widget._appointment.id}",
        data: {
          "note": mapInputBloc.noteText,
          "stakeholderPresent": mapInputBloc.present,
          "authorizers": mapInputBloc.authorizers,
          "measurements": mapInputBloc.measurementText,
          "addedFiles": mapInputBloc.addedFiles.map((e) => e.toJson()).toList(),
          "unanimous": mapInputBloc.unanimous,
          "compatible": mapInputBloc.compatible,
        },
        options: Options(
          headers: {
            "authorization": user.token,
          },
        ),
      );

      _updateAppointmentLocal(mapInputBloc);
      _sendUpdatedAlert(mapInputBloc, "Geupdate", "De afspraak is geupdate");
    } catch (e, stacktrace) {
      _logger.severe(
          "Error sending updated appointment message=${ErrorMessageHttp.message(e)}",
          e,
          stacktrace);
      _updateAppointmentLocal(mapInputBloc);
      _storeRequestLocal(mapInputBloc);
      _sendUpdatedAlert(mapInputBloc, "Geupdate",
          "De afspraak wordt zo snel mogelijk geupdate");
    }
  }

  void _updateAppointmentLocal(MapInputBloc mapInputBloc) {
    _logger.info("Updating appointment ${widget._appointment.id} in local");
    final List<Appointment> appointments =
        StorageManager.getInstance().getStore("appointment").get();
    int index = appointments
        .indexWhere((element) => element.id == widget._appointment.id);
    if (appointments[index].updatedDate == null) {
      appointments[index].updatedDate = DateTime.now();
    }
    appointments[index].note = mapInputBloc.noteText;
    appointments[index].measurements = mapInputBloc.measurementText;
    appointments[index].addedFiles = mapInputBloc.addedFiles;
    for (int j = 0; j < appointments[index].stakeholders.length; j++) {
      appointments[index].stakeholders[j].present = mapInputBloc.present[j];
      appointments[index].stakeholders[j].authorizer = mapInputBloc.authorizers[j];
    }

    appointments[index].unanimous = mapInputBloc.unanimous;
    appointments[index].compatible = mapInputBloc.compatible;

    StorageManager.getInstance().getStore("appointment").insert(appointments);
  }

  void _storeRequestLocal(MapInputBloc mapInputBloc) {
    final List<Request> requests =
        StorageManager.getInstance().getStore("request").get();

    final Request newRequest = Request(
        widget._appointment.id,
        mapInputBloc.noteText,
        mapInputBloc.present,
        mapInputBloc.authorizers,
        mapInputBloc.measurementText,
        mapInputBloc.addedFiles,
        mapInputBloc.unanimous,
        mapInputBloc.compatible);

    requests.add(newRequest);

    StorageManager.getInstance().getStore("request").insert(requests);
  }

  void _sendUpdatedAlert(
      MapInputBloc mapInputBloc, String title, String description) async {
    mapInputBloc.sendingMap = false;
    await AlertUtil.sendInfoAlert(
        context, title, description, (context) {
      _pushReplacements(context);
    }).then((value) => _pushReplacements(context));
  }

  void _pushReplacements(BuildContext context) {
    _deleteOldAddedFiles();
    if (widget._history) {
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (BuildContext context) {
        return HistoryPage();
      }));
      return;
    }
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (BuildContext context) {
      return AppointmentsPage();
    }));
  }

  void _deleteOldAddedFiles() async {
    final MapInputBloc mapInputBloc = widget._mapInputBloc;

    final Set<AppointmentFile> oldAddedFiles =
        widget._appointment.addedFiles.toSet();
    final Set<AppointmentFile> newAddedFiles = mapInputBloc.addedFiles.toSet();

    oldAddedFiles.removeAll(newAddedFiles);

    oldAddedFiles.forEach((element) {
      FileManager.getInstance()
          .deleteAppointmentFile(widget._appointment, element);
    });
  }
}

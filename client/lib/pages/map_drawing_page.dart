import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:client/bloc/map/map_drawing_bloc.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:client/file/file_manager.dart';
import 'package:client/model/appointment/appointment.dart';
import 'package:client/storage/store_offset.dart';
import 'package:client/util/alert_util.dart';
import 'package:client/util/screen_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

import '../widgets/map/drawing/map_buttons.dart';
import '../widgets/map/drawing/map_color_buttons.dart';
import '../widgets/map/drawing/map_range_slider.dart';
import '../widgets/map/drawing/map_widget.dart';

class MapDrawingPage extends StatefulWidget {
  final Appointment _appointment;

  MapDrawingPage(this._appointment);

  @override
  _MapDrawingPageState createState() => _MapDrawingPageState();
}

class _MapDrawingPageState extends State<MapDrawingPage> {
  final Logger _logger = Logger("MapDrawingPageState");

  final GlobalKey _repaintBoundaryKey = GlobalKey();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  bool _saved = false;

  @override
  Widget build(BuildContext context) {
    final ScreenUtil screenUtil = ScreenUtil(context);
    return WillPopScope(
      onWillPop: () async {
        if (!_saved) {
          await AlertUtil.sendConfirmationAlert(context, "Opslaan",
              "De kaart is niet opgeslagen weet u zeker dat u terug wil gaan?",
              (context) {
            Navigator.of(context).pop();
            final MapDrawingBloc mapDrawingBloc =
                Provider.of<MapDrawingBloc>(context, listen: false);
            mapDrawingBloc.matrix4 = Matrix4.identity();
            Navigator.of(context).pop();
            return true;
          }, close: "Annuleren");
          return false;
        }
        final MapDrawingBloc mapDrawingBloc =
            Provider.of<MapDrawingBloc>(context, listen: false);
        mapDrawingBloc.matrix4 = Matrix4.identity();
        Navigator.of(context).pop();
        return true;
      },
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(
            "Kaart - ${widget._appointment.parcel}",
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () async {
              if (!_saved) {
                await AlertUtil.sendConfirmationAlert(context, "Opslaan",
                    "De kaart is niet opgeslagen weet u zeker dat u terug wil gaan?",
                    (context) {
                  Navigator.of(context).pop();
                  final MapDrawingBloc mapDrawingBloc =
                      Provider.of<MapDrawingBloc>(context, listen: false);
                  mapDrawingBloc.matrix4 = Matrix4.identity();
                  Navigator.of(context).pop();
                }, close: "Annuleren");
                return;
              }
              final MapDrawingBloc mapDrawingBloc =
                  Provider.of<MapDrawingBloc>(context, listen: false);
              mapDrawingBloc.matrix4 = Matrix4.identity();
              Navigator.of(context).pop();
            },
          ),
          actions: [
            IconButton(
              onPressed: () async {
                _saved = true;
                final MapDrawingBloc mapDrawingBloc =
                    Provider.of<MapDrawingBloc>(context, listen: false);
                mapDrawingBloc.matrix4 = Matrix4.identity();
                mapDrawingBloc.disabled = true;
                Future.delayed(const Duration(milliseconds: 200))
                    .then((value) async {
                  final bytes = await _capturePng();
                  await _saveFile(bytes);
                  await _saveOffsets(mapDrawingBloc);
                  mapDrawingBloc.disabled = false;
                  mapDrawingBloc.zooming = true;
                });
              },
              icon: Icon(Icons.save),
            ),
          ],
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 8, bottom: 12),
              child: MapButtons(),
            ),
            SizedBox(
              height: screenUtil.getScreenHeightPercentage(
                  screenUtil.isLandScape() && screenUtil.getScreenHeight() < 450
                      ? 34
                      : 60),
              width: screenUtil.getScreenWidth(),
              child: RepaintBoundary(
                key: _repaintBoundaryKey,
                child: MapWidget(widget._appointment),
              ),
            ),
            _mapButtons(),
          ],
        ),
      ),
    );
  }

  Future<Uint8List> _capturePng() async {
    _logger.info("Capturing image of map");
    final RenderRepaintBoundary boundary =
        _repaintBoundaryKey.currentContext.findRenderObject();
    final ui.Image image = await boundary.toImage(pixelRatio: 3.0);
    final ByteData byteData =
        await image.toByteData(format: ui.ImageByteFormat.png);

    return byteData.buffer.asUint8List();
  }

  Future<void> _saveOffsets(MapDrawingBloc mapDrawingBloc) async {
    Future.delayed(const Duration(milliseconds: 100)).then((t) {
      _logger.info("Saving offsets");
      StoreOffsetManager.getInstance()
          .storeOffsets(widget._appointment.id, mapDrawingBloc.offsetInfo);

      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(
          "Kaart is opgeslagen",
          style: TextStyle(
            color: KadasterColors.okColor,
          ),
        ),
      ));
    });
  }

  Future<String> _saveFile(Uint8List bytes) async {
    return await FileManager.getInstance()
        .saveFile(bytes, "${widget._appointment.id}", "map.png");
  }

  Widget _mapButtons() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Consumer<MapDrawingBloc>(
        builder: (BuildContext context, MapDrawingBloc value, Widget child) {
          if (value.zooming || value.disabled) {
            return SizedBox.shrink();
          }
          final colors = Padding(
            padding: const EdgeInsets.all(4.0),
            child: MapColorButtons(),
          );

          final range = Padding(
            padding: const EdgeInsets.all(4.0),
            child: MapRangeSlider(),
          );

          if (value.drawing || value.lines) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                range,
                SizedBox(
                  height: 12,
                ),
                colors,
              ],
            );
          }

          return range;
        },
      ),
    );
  }
}

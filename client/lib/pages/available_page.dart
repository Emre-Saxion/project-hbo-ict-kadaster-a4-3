import 'package:client/bloc/available_page_bloc.dart';
import 'package:client/model/available_date.dart';
import 'package:client/model/user.dart';
import 'package:client/storage/storage_manager.dart';
import 'package:client/widgets/available/available_page_body.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

class AvailablePage extends StatelessWidget {
  final Logger _logger = Logger("AvailablePage");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Beschikbaarheid"),
      ),
      body: ChangeNotifierProvider.value(
        value: _availablePageBloc(),
        child: AvailablePageBody(),
      ),
    );
  }

  ///Initializing the [AvailablePageBloc] by adding the correct dates if the user has any
  AvailablePageBloc _availablePageBloc() {
    final User user = StorageManager.getInstance().getStore("user").get();

    final AvailablePageBloc availablePageBloc = AvailablePageBloc();
    if (user.availableDates != null && user.availableDates.length > 0) {
      _logger.info("Adding ${user.availableDates.length} dates to AvailablePageBloc");
      for (AvailableDate date in user.availableDates) {
        availablePageBloc.addDate(date);
      }
    }

    return availablePageBloc;
  }
}

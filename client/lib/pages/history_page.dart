import 'package:client/bloc/appointments_bloc.dart';
import 'package:client/widgets/appointments/appointments_body.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'appointments_page.dart';

class HistoryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (BuildContext context) {
          return AppointmentsPage();
        }));
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (BuildContext context) {
                return AppointmentsPage();
              }));
            },
          ),
          title: Text("Geschiedenis"),
        ),
        body: ChangeNotifierProvider(
          create: (_) => AppointmentsBloc(),
          child: AppointmentsBody(true),
        ),
      ),
    );
  }
}

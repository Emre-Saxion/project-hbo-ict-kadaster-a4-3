import 'package:client/bloc/appointments_bloc.dart';
import 'package:client/widgets/appointments/appointments_body.dart';
import 'package:client/widgets/appointments/appointments_drawer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AppointmentsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Afspraken"),
      ),
      body: ChangeNotifierProvider(
        create: (_) => AppointmentsBloc(),
        child: AppointmentsBody(false),
      ),
      drawer: AppointmentsDrawer(),
    );
  }
}

import 'package:client/bloc/verify_bloc.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:client/storage/storage_manager.dart';
import 'package:client/util/alert_util.dart';
import 'package:client/util/no_inkwell_scroll.dart';
import 'package:client/widgets/login/login_kadaster_logo.dart';
import 'package:client/widgets/verify/verify_code_input.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

import 'login_page.dart';

class VerifyCodePage extends StatelessWidget {

  final Logger _logger = Logger("VerifyCodePage");

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        AlertUtil.sendConfirmationAlert(
            context,
            "Uitloggen",
            "Weet je zeker dat je wil uitloggen?",
                (context) => _logOff(context),
            barrierColor: Colors.black54);
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            key: Key("verify_code_log_off"),
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              AlertUtil.sendConfirmationAlert(
                  context,
                  "Uitloggen",
                  "Weet je zeker dat je wil uitloggen?",
                  (context) => _logOff(context),
                  barrierColor: Colors.black54);
            },
          ),
          title: Text("Verifieer"),
          backgroundColor: KadasterColors.mainColor,
        ),
        body: ScrollConfiguration(
          behavior: NoInkWellScroll(),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 24,
                ),
                Center(
                  child: Column(
                    children: [
                      LoginKadasterLogo(),
                      SizedBox(
                        height: 12,
                      ),
                      Text(
                        _title(),
                        style:
                            TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 72,
                ),
                Text(
                  "Log in met je code",
                  style: TextStyle(fontSize: 16),
                ),
                SizedBox(
                  height: 12,
                ),
                ChangeNotifierProvider(
                  create: (_) => VerifyBloc(),
                  child: VerifyCodeInput(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  ///The correct title for the user
  ///
  ///It looks like "Hallo FIRSTNAME LASTNAME"
  String _title() {
    final user = StorageManager.getInstance().getStore("user").get();
    return "Hallo ${user.firstName} ${user.lastName}";
  }

  ///Logging the user off
  ///
  ///The user gets logged off by deleting the user in [StorageManager]
  void _logOff(BuildContext context) {
    _logger.info("Logging off");
    StorageManager.getInstance().getStore("user").delete();
    StorageManager.getInstance().getStore("appointment").delete();
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
  }
}

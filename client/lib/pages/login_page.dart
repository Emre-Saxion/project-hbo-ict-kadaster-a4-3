import 'package:client/bloc/login_bloc.dart';
import 'package:client/util/no_inkwell_scroll.dart';
import 'package:client/util/screen_util.dart';
import 'package:client/widgets/login/login_form.dart';
import 'package:client/widgets/login/login_kadaster_logo.dart';
import 'package:client/widgets/login/login_mountain_container.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ScreenUtil screenUtil = ScreenUtil(context);

    return Scaffold(
      body: ScrollConfiguration(
        behavior: NoInkWellScroll(),
        child: SingleChildScrollView(
          child: Stack(
            children: [
              LoginMountainContainer(),
              Align(
                alignment: Alignment(0.0, 1.0),
                child: Column(
                  children: [
                    SizedBox(
                      height: screenUtil.getScreenHeightPercentage(25),
                    ),
                    LoginKadasterLogo(),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      "Kadaster",
                      style:
                          TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    ChangeNotifierProvider(
                      create: (_) => LoginBloc(),
                      child: LoginForm(),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

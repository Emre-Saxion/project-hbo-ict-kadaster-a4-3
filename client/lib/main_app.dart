import 'package:background_fetch/background_fetch.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:client/main.dart';
import 'package:client/pages/login_page.dart';
import 'package:client/pages/verify_code_page.dart';
import 'package:client/storage/storage_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:logging/logging.dart';

class MainApp extends StatelessWidget {
  final Logger _logger = Logger("MainApp");

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale("nl", "NL"),
      ],
      title: 'Kadaster Landmeter',
      theme: ThemeData(
        primarySwatch: KadasterColors.mainColor,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        scaffoldBackgroundColor: KadasterColors.SCAFFOLD_BACKGROUND_COLOR,
      ),
      home: _homePage(),
    );
  }

  ///Loading the correct home page based on if the user is logged in
  ///
  ///We check if the user is logged in by checking the user store in [StorageManager]
  Widget _homePage() {
    BackgroundFetch.configure(
        BackgroundFetchConfig(
          minimumFetchInterval: 15,
          forceAlarmManager: false,
          stopOnTerminate: false,
          startOnBoot: true,
          enableHeadless: true,
          requiresBatteryNotLow: false,
          requiresCharging: false,
          requiresStorageNotLow: false,
          requiresDeviceIdle: false,
          requiredNetworkType: NetworkType.ANY,
        ),
        backgroundFetchHeadlessTask);
    var homePage;
    final user = StorageManager.getInstance().getStore("user").get();
    if (user != null) {
      _logger.info("User found so going to VerifyCodePage");
      homePage = VerifyCodePage();
    } else {
      _logger.info("User not found so going to LoginPage");
      homePage = LoginPage();
    }
    return homePage;
  }
}

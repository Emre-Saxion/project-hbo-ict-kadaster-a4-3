import 'package:json_annotation/json_annotation.dart';

import 'available_date.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  final String token;

  final String loginCode;

  final String firstName;

  final String lastName;

  final String phoneNumber;

  final String email;

  final bool admin;

  List<AvailableDate> availableDates;

  User(this.token, this.loginCode, this.firstName, this.lastName, this.phoneNumber, this.email, this.admin, this.availableDates);

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  @override
  String toString() {
    return 'User{token: $token, loginCode: $loginCode, firstName: $firstName, lastName: $lastName, phoneNumber: $phoneNumber, email: $email, admin: $admin, availableDates: $availableDates}';
  }
}

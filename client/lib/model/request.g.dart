// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Request _$RequestFromJson(Map<String, dynamic> json) {
  return Request(
    json['id'] as int,
    json['note'] as String,
    (json['stakeholderPresent'] as List)?.map((e) => e as bool)?.toList(),
    (json['authorizers'] as List)
        ?.map((e) => e == null
        ? null
        : Authorizer.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['measurements'] as String,
    (json['addedFiles'] as List)
        ?.map((e) => e == null
        ? null
        : AppointmentFile.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['unanimous'] as bool,
    json['compatible'] as bool,
  );
}

Map<String, dynamic> _$RequestToJson(Request instance) => <String, dynamic>{
  'id': instance.id,
  'note': instance.note,
  'stakeholderPresent': instance.stakeholderPresent,
  'authorizers': instance.authorizers,
  'measurements': instance.measurements,
  'addedFiles': instance.addedFiles,
  'unanimous': instance.unanimous,
  'compatible': instance.compatible,
};

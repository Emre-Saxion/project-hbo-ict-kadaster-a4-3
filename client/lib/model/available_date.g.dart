// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'available_date.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AvailableDate _$AvailableDateFromJson(Map<String, dynamic> json) {
  return AvailableDate(
    json['date'] == null ? null : DateTime.parse(json['date'] as String),
    json['available'] as bool,
    json['extra'] as String,
  );
}

Map<String, dynamic> _$AvailableDateToJson(AvailableDate instance) =>
    <String, dynamic>{
      'date': instance.date?.toIso8601String(),
      'available': instance.available,
      'extra': instance.extra,
    };

import 'package:flutter/material.dart';

class OffsetInfo {
  final int id;

  Offset offset;

  final Color color;

  final double range;

  final bool line;

  OffsetInfo(this.id, this.offset, this.color, this.range, this.line);

  factory OffsetInfo.fromJson(Map<String, dynamic> json) => OffsetInfo(
        json['id'] as int,
        Offset(json['dx'] as double, json['dy'] as double),
        Color.fromRGBO(
            json['red'] as int, json['green'] as int, json['blue'] as int, 1.0),
        json['range'] as double,
        json["line"] as bool,
      );

  Map<String, dynamic> toJson() => _offsetInfoToJson(this);

  Map<String, dynamic> _offsetInfoToJson(OffsetInfo instance) =>
      <String, dynamic>{
        'id': instance.id,
        'dx': instance.offset.dx,
        'dy': instance.offset.dy,
        'red': instance.color.red,
        'green': instance.color.green,
        'blue': instance.color.blue,
        'range': instance.range,
        'line': instance.line
      };


  @override
  String toString() {
    return 'OffsetInfo{id: $id, offset: $offset, color: $color, range: $range, line: $line}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is OffsetInfo &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          offset == other.offset &&
          color == other.color &&
          range == other.range &&
          line == other.line;

  @override
  int get hashCode =>
      id.hashCode ^
      offset.hashCode ^
      color.hashCode ^
      range.hashCode ^
      line.hashCode;
}

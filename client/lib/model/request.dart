import 'package:client/model/appointment/appointment_file.dart';
import 'package:client/model/appointment/authorizer.dart';
import 'package:json_annotation/json_annotation.dart';

part 'request.g.dart';

@JsonSerializable()
class Request {

  final int id;

  final String note;

  final List<bool> stakeholderPresent;

  final List<Authorizer> authorizers;

  final String measurements;

  final List<AppointmentFile> addedFiles;

  final bool unanimous;

  final bool compatible;

  Request(this.id, this.note, this.stakeholderPresent, this.authorizers, this.measurements,
      this.addedFiles, this.unanimous, this.compatible);

  factory Request.fromJson(Map<String, dynamic> json) => _$RequestFromJson(json);

  Map<String, dynamic> toJson() => _$RequestToJson(this);
}

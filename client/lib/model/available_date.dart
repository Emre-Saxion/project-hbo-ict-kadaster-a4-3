
import 'package:json_annotation/json_annotation.dart';

part 'available_date.g.dart';

@JsonSerializable()
class AvailableDate {

  final DateTime date;

  final bool available;

  final String extra;

  AvailableDate(this.date, this.available, this.extra);

  factory AvailableDate.fromJson(Map<String, dynamic> json) => _$AvailableDateFromJson(json);

  Map<String, dynamic> toJson() => _$AvailableDateToJson(this);

  @override
  String toString() {
    return 'AvailableDate{date: $date, available: $available, extra: $extra}';
  }
}
// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    json['token'] as String,
    json['loginCode'] as String,
    json['firstName'] as String,
    json['lastName'] as String,
    json['phoneNumber'] as String,
    json['email'] as String,
    json['admin'] as bool,
    (json['availableDates'] as List)
        ?.map((e) => e == null
        ? null
        : AvailableDate.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
  'token': instance.token,
  'loginCode': instance.loginCode,
  'firstName': instance.firstName,
  'lastName': instance.lastName,
  'phoneNumber': instance.phoneNumber,
  'email': instance.email,
  'admin': instance.admin,
  'availableDates': instance.availableDates,
};

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stakeholder.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Stakeholder _$StakeholderFromJson(Map<String, dynamic> json) {
  return Stakeholder(
    json['id'] as int,
    json['name'] as String,
    json['email'] as String,
    json['phoneNumber'] as String,
    json['birthDate'] == null
        ? null
        : DateTime.parse(json['birthDate'] as String),
    json['address'] == null
        ? null
        : Address.fromJson(json['address'] as Map<String, dynamic>),
    json['authorizer'] == null
        ? null
        : Authorizer.fromJson(json['authorizer'] as Map<String, dynamic>),
    json['present'] as bool,
    json['title'] as String,
    json['munacipility'] as bool,
  );
}

Map<String, dynamic> _$StakeholderToJson(Stakeholder instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'email': instance.email,
      'phoneNumber': instance.phoneNumber,
      'birthDate': instance.birthDate?.toIso8601String(),
      'address': instance.address,
      'authorizer': instance.authorizer,
      'present': instance.present,
      'title': instance.title,
      'munacipility': instance.munacipility,
    };

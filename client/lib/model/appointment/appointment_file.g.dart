// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appointment_file.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppointmentFile _$AppointmentFileFromJson(Map<String, dynamic> json) {
  return AppointmentFile(
    json['fileName'] as String,
    json['fileExtension'] as String,
  );
}

Map<String, dynamic> _$AppointmentFileToJson(AppointmentFile instance) =>
    <String, dynamic>{
      'fileName': instance.fileName,
      'fileExtension': instance.fileExtension,
    };

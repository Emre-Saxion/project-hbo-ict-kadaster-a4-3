
import 'package:json_annotation/json_annotation.dart';

part 'deed.g.dart';

@JsonSerializable()
class Deed {

  final int partNumber;

  final String deedNumber;

  final String deedText;

  Deed(this.partNumber, this.deedNumber, this.deedText);

  factory Deed.fromJson(Map<String, dynamic> json) => _$DeedFromJson(json);

  Map<String, dynamic> toJson() => _$DeedToJson(this);

  @override
  String toString() {
    return 'Deed{partNumber: $partNumber, deedNumber: $deedNumber, deedText: $deedText}';
  }
}
import 'package:client/model/appointment/appointment_file.dart';
import 'package:client/model/appointment/stakeholder.dart';
import 'package:json_annotation/json_annotation.dart';

import 'address.dart';
import 'deed.dart';

part 'appointment.g.dart';

@JsonSerializable()
class Appointment {
  final int id;

  final String parcel;

  String mapFile;

  List<AppointmentFile> files;

  List<AppointmentFile> addedFiles;

  final DateTime appointmentDate;

  final DateTime appointmentEndDate;

  DateTime updatedDate;

  String note;

  final Address address;

  String measurements;

  final Deed deed;

  final List<Stakeholder> stakeholders;

  final int type;

  final String description;

  bool unanimous;

  bool compatible;

  Appointment(
      this.id,
      this.parcel,
      this.mapFile,
      this.files,
      this.addedFiles,
      this.appointmentDate,
      this.appointmentEndDate,
      this.updatedDate,
      this.note,
      this.address,
      this.measurements,
      this.deed,
      this.stakeholders,
      this.type,
      this.description,
      this.unanimous,
      this.compatible);

  factory Appointment.fromJson(Map<String, dynamic> json) =>
      _$AppointmentFromJson(json);

  Map<String, dynamic> toJson() => _$AppointmentToJson(this);

  @override
  String toString() {
    return 'Appointment{id: $id, parcel: $parcel, mapFile: $mapFile, files: $files, addedFiles: $addedFiles, appointmentDate: $appointmentDate, appointmentEndDate: $appointmentEndDate, updatedDate: $updatedDate, note: $note, address: $address, measurements: $measurements, deed: $deed, stakeholders: $stakeholders, type: $type, description: $description, unanimous: $unanimous, compatible: $compatible}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Appointment &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          parcel == other.parcel &&
          mapFile == other.mapFile &&
          files == other.files &&
          addedFiles == other.addedFiles &&
          appointmentDate == other.appointmentDate &&
          appointmentEndDate == other.appointmentEndDate &&
          updatedDate == other.updatedDate &&
          note == other.note &&
          address == other.address &&
          measurements == other.measurements &&
          deed == other.deed &&
          stakeholders == other.stakeholders &&
          type == other.type &&
          description == other.description &&
          unanimous == other.unanimous &&
          compatible == other.compatible;

  @override
  int get hashCode =>
      id.hashCode ^
      parcel.hashCode ^
      mapFile.hashCode ^
      files.hashCode ^
      addedFiles.hashCode ^
      appointmentDate.hashCode ^
      appointmentEndDate.hashCode ^
      updatedDate.hashCode ^
      note.hashCode ^
      address.hashCode ^
      measurements.hashCode ^
      deed.hashCode ^
      stakeholders.hashCode ^
      type.hashCode ^
      description.hashCode ^
      unanimous.hashCode ^
      compatible.hashCode;
}

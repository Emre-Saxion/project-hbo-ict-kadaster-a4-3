// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'address.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Address _$AddressFromJson(Map<String, dynamic> json) {
  return Address(
    json['country'] as String,
    json['municipality'] as String,
    json['city'] as String,
    json['zipCode'] as String,
    json['street'] as String,
    json['number'] as String,
  );
}

Map<String, dynamic> _$AddressToJson(Address instance) => <String, dynamic>{
  'country': instance.country,
  'municipality': instance.municipality,
  'city': instance.city,
  'zipCode': instance.zipCode,
  'street': instance.street,
  'number': instance.number,
};

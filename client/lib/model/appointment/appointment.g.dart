// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'appointment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Appointment _$AppointmentFromJson(Map<String, dynamic> json) {
  return Appointment(
    json['id'] as int,
    json['parcel'] as String,
    json['mapFile'] as String,
    (json['files'] as List)
        ?.map((e) => e == null
        ? null
        : AppointmentFile.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    (json['addedFiles'] as List)
        ?.map((e) => e == null
        ? null
        : AppointmentFile.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['appointmentDate'] == null
        ? null
        : DateTime.parse(json['appointmentDate'] as String),
    json['appointmentEndDate'] == null
        ? null
        : DateTime.parse(json['appointmentEndDate'] as String),
    json['updatedDate'] == null
        ? null
        : DateTime.parse(json['updatedDate'] as String),
    json['note'] as String,
    json['address'] == null
        ? null
        : Address.fromJson(json['address'] as Map<String, dynamic>),
    json['measurements'] as String,
    json['deed'] == null
        ? null
        : Deed.fromJson(json['deed'] as Map<String, dynamic>),
    (json['stakeholders'] as List)
        ?.map((e) =>
    e == null ? null : Stakeholder.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    json['type'] as int,
    json['description'] as String,
    json['unanimous'] as bool,
    json['compatible'] as bool,
  );
}

Map<String, dynamic> _$AppointmentToJson(Appointment instance) =>
    <String, dynamic>{
      'id': instance.id,
      'parcel': instance.parcel,
      'mapFile': instance.mapFile,
      'files': instance.files,
      'addedFiles': instance.addedFiles,
      'appointmentDate': instance.appointmentDate?.toIso8601String(),
      'appointmentEndDate': instance.appointmentEndDate?.toIso8601String(),
      'updatedDate': instance.updatedDate?.toIso8601String(),
      'note': instance.note,
      'address': instance.address,
      'measurements': instance.measurements,
      'deed': instance.deed,
      'stakeholders': instance.stakeholders,
      'type': instance.type,
      'description': instance.description,
      'unanimous': instance.unanimous,
      'compatible': instance.compatible,
    };

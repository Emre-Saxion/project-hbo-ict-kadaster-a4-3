import 'package:client/model/appointment/address.dart';
import 'package:client/model/appointment/authorizer.dart';
import 'package:json_annotation/json_annotation.dart';

part 'stakeholder.g.dart';

@JsonSerializable()
class Stakeholder {
  final int id;

  final String name;

  final String email;

  final String phoneNumber;

  final DateTime birthDate;

  final Address address;

  Authorizer authorizer;

  bool present;

  final String title;

  final bool munacipility;

  Stakeholder(
      this.id,
      this.name,
      this.email,
      this.phoneNumber,
      this.birthDate,
      this.address,
      this.authorizer,
      this.present,
      this.title,
      this.munacipility);

  factory Stakeholder.fromJson(Map<String, dynamic> json) =>
      _$StakeholderFromJson(json);

  Map<String, dynamic> toJson() => _$StakeholderToJson(this);

  @override
  String toString() {
    return 'Stakeholder{id: $id, name: $name, email: $email, phoneNumber: $phoneNumber, birthDate: $birthDate, address: $address, present: $present}';
  }
}

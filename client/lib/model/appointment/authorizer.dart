import 'package:json_annotation/json_annotation.dart';

part 'authorizer.g.dart';

@JsonSerializable()
class Authorizer {
  String name;

  String acquaintance;

  DateTime birthDate;

  Authorizer(this.name, this.acquaintance, this.birthDate);

  factory Authorizer.fromJson(Map<String, dynamic> json) =>
      _$AuthorizerFromJson(json);

  Map<String, dynamic> toJson() => _$AuthorizerToJson(this);
}

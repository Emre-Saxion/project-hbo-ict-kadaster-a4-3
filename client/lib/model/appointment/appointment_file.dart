

import 'package:json_annotation/json_annotation.dart';

part 'appointment_file.g.dart';

@JsonSerializable()
class AppointmentFile {

  final String fileName;

  final String fileExtension;

  AppointmentFile(this.fileName, this.fileExtension);

  factory AppointmentFile.fromJson(Map<String, dynamic> json) => _$AppointmentFileFromJson(json);

  Map<String, dynamic> toJson() => _$AppointmentFileToJson(this);

  @override
  String toString() {
    return 'AppointmentFile{fileName: $fileName, fileExtension: $fileExtension}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppointmentFile &&
          runtimeType == other.runtimeType &&
          fileName == other.fileName &&
          fileExtension == other.fileExtension;

  @override
  int get hashCode => fileName.hashCode ^ fileExtension.hashCode;
}
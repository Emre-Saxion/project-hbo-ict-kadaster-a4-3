// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'deed.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Deed _$DeedFromJson(Map<String, dynamic> json) {
  return Deed(
    json['partNumber'] as int,
    json['deedNumber'] as String,
    json['deedText'] as String,
  );
}

Map<String, dynamic> _$DeedToJson(Deed instance) => <String, dynamic>{
  'partNumber': instance.partNumber,
  'deedNumber': instance.deedNumber,
  'deedText': instance.deedText,
};


import 'package:json_annotation/json_annotation.dart';

part 'address.g.dart';

@JsonSerializable()
class Address {

  final String country;

  final String municipality;

  final String city;

  final String zipCode;

  final String street;

  final String number;

  Address(this.country, this.municipality, this.city, this.zipCode, this.street,
      this.number);

  factory Address.fromJson(Map<String, dynamic> json) => _$AddressFromJson(json);

  Map<String, dynamic> toJson() => _$AddressToJson(this);

  @override
  String toString() {
    return 'Address{country: $country, municipality: $municipality, city: $city, zipCode: $zipCode, street: $street, number: $number}';
  }
}
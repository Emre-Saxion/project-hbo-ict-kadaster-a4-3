// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'authorizer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Authorizer _$AuthorizerFromJson(Map<String, dynamic> json) {
  return Authorizer(
    json['name'] == null ? "" : json["name"] as String,
    json['acquaintance'] == null ? "" : json["acquaintance"] as String,
    json['birthDate'] == null
        ? null
        : DateTime.parse(json['birthDate'] as String),
  );
}

Map<String, dynamic> _$AuthorizerToJson(Authorizer instance) =>
    <String, dynamic>{
      'name': instance.name,
      'acquaintance': instance.acquaintance,
      'birthDate': instance.birthDate?.toIso8601String(),
    };

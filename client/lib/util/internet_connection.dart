import 'package:connectivity/connectivity.dart';

///Checking for an internet connection
class InternetConnection {
  ///Returns a future which contains a boolean which will be true if there is an internet connection otherwise false
  static Future<bool> connected() async {
    final connectivityResult = await (Connectivity().checkConnectivity());
    return connectivityResult != ConnectivityResult.none;
  }
}

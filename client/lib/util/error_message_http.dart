import 'dart:convert';

import 'package:dio/dio.dart';

///Utility class to return errors correctly
class ErrorMessageHttp {
  static dynamic message(e) {
    if (e is DioError) {
      if (e.response != null) {
        if (e.response.data is String) {
          return "${e.message} ${e.response.data}";
        }
        return "${e.message} ${utf8.decode(e.response.data as List<int>)}";
      }
      return e.message;
    }
    return e;
  }
}

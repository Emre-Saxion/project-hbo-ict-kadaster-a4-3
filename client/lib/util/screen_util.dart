import 'package:flutter/material.dart';

///Utility class for calculating screen size and orientation
class ScreenUtil {
  final BuildContext _context;

  ScreenUtil(this._context);

  Size _getScreenSize() {
    return MediaQuery.of(_context).size;
  }

  ///The width of the screen
  double getScreenWidth() {
    return _getScreenSize().width;
  }

  ///The height of the screen
  double getScreenHeight() {
    return _getScreenSize().height;
  }

  ///The width of the screen but a percentage [percentage]
  double getScreenWidthPercentage(double percentage) {
    return getScreenWidth() * (percentage / 100);
  }

  ///The height of the screen but a percentage [percentage]
  double getScreenHeightPercentage(double percentage) {
    return getScreenHeight() * (percentage / 100);
  }

  Orientation _getOrientation() {
    return MediaQuery.of(_context).orientation;
  }

  ///Return true if the phone is in landscape otherwise false
  bool isLandScape() {
    return _getOrientation() == Orientation.landscape;
  }

  ///Return true if the phone is in portrait otherwise false
  bool isPortrait() {
    return _getOrientation() == Orientation.portrait;
  }
}

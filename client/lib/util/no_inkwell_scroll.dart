import 'package:flutter/material.dart';

///Utility class to remove inkwell from scroll
class NoInkWellScroll extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

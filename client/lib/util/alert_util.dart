import 'package:client/constants/kadaster_colors.dart';
import 'package:flutter/material.dart';

///The function that happens when pressing on confirm
typedef void OnConfirm(BuildContext context);

///The function that happens when pressing on close
typedef void OnClose(BuildContext context);

///Utility class to send alerts
class AlertUtil {
  ///The generic send alert class used privately in this class
  static Future<void> _sendAlert(BuildContext context, String title,
      String description, List<Widget> actions,
      {Color barrierColor = Colors.black54}) async {
    await showGeneralDialog(
        context: context,
        transitionBuilder: (context, animation, secondAnimation, widget) {
          return Transform.scale(
            scale: animation.value,
            child: Opacity(
              opacity: animation.value,
              child: AlertDialog(
                key: Key("alert_dialog"),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
                title: Text(title),
                content: Text(description),
                contentPadding:
                    const EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 12.0),
                buttonPadding: const EdgeInsets.only(right: 8),
                actions: actions,
              ),
            ),
          );
        },
        transitionDuration: const Duration(milliseconds: 200),
        barrierDismissible: true,
        barrierColor: barrierColor,
        barrierLabel: "",
        pageBuilder: (context, animation, secondAnimation) {
          return null;
        });
  }

  ///Sending an info alert with just a close button
  static Future<void> sendInfoAlert(
      BuildContext context, String title, String description, OnClose onClose,
      {Color barrierColor = Colors.black54}) async {
    await _sendAlert(
        context,
        title,
        description,
        [
          FlatButton(
            child: Text(
              "Sluiten",
              style: TextStyle(color: KadasterColors.errorColor),
            ),
            onPressed: () {
              Navigator.of(context).pop();
              onClose.call(context);
            },
          )
        ],
        barrierColor: barrierColor);
  }

  ///Sending a confirm alert with an ok button and a close button
  static Future<void> sendConfirmationAlert(BuildContext context, String title,
      String description, OnConfirm onConfirm,
      {Color barrierColor = Colors.black54, String close = "Sluiten"}) async {
    await _sendAlert(
        context,
        title,
        description,
        [
          FlatButton(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text(
                  close,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: KadasterColors.errorColor),
                ),
              ),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          FlatButton(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text(
                  "Oke",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: KadasterColors.okColor),
                ),
              ),
            ),
            onPressed: () {
              onConfirm(context);
            },
          ),
        ],
        barrierColor: barrierColor);
  }
}

import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:logging/logging.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constants/constants.dart';

///The abstract class used for storing objects by using json
abstract class Store<T> {
  final Logger _logger = Logger("Store");

  ///The shared preferences used for storing everything
  final SharedPreferences _sharedPreferences;

  final String _key;

  Store(this._sharedPreferences, this._key);

  ///Inserting the [T] into [_key] of [_sharedPreferences]
  void insert(T object);

  ///Getting the [T] from [_key] of [_sharedPreferences]
  T get();

  ///Deleting the [_key] from [_sharedPreferences]
  void delete();

  ///Inserting a list to [_key] in [_sharedPreferences]
  ///This function is marked protected and should only be used by direct children
  @protected
  void insertList(List<dynamic> object) {
    _logger.info("Inserting list, key=$_key list=$object");
    for (int i = 0; i < object.length; i++) {
      _sharedPreferences.setString(_key + "-$i", jsonEncode(object[i]));
    }
  }

  ///Inserting an object to [_key] in [_sharedPreferences]
  ///This function is marked protected and should only be used by direct children
  @protected
  void insertObject(T object) {
    _logger.info("Inserting object, key=$_key list=$object");
    _sharedPreferences.setString(_key, jsonEncode(object));
  }

  ///Deleting an object from [_key] in [_sharedPreferences]
  ///This function is marked protected and should only be used by direct children
  @protected
  void deleteObject() {
    _logger.info("Deleting object $_key");
    _sharedPreferences.remove(_key);
  }

  ///Deleting a list from [_key] in [_sharedPreferences]
  ///This function is marked protected and should only be used by direct children
  @protected
  void deleteList() {
    _logger.info("Deleting list $_key");
    for (int i = 0; i < Constants.MAX_INT; i++) {
      final value = _sharedPreferences.get(_key + "-$i");
      if (value == null || value.length <= 0) {
        break;
      }
      _sharedPreferences.remove(_key + "-$i");
    }
  }

  ///Grabbing a list of strings from [_key] in [_sharedPreferences]
  ///This function is marked protected and should only be used by direct children
  @protected
  List<dynamic> getList() {
    final objects = [];
    for (int i = 0; i < Constants.MAX_INT; i++) {
      final value = _sharedPreferences.get(_key + "-$i");
      if (value == null || value.length <= 0) {
        break;
      }
      objects.add(value);
    }
    return objects;
  }

  ///Grabbing a string from [_key] in [_sharedPreferences]
  ///This function is marked protected and should only be used by direct children
  @protected
  String getObject() {
    return _sharedPreferences.get(_key);
  }

  @protected
  String get key => _key;

  @protected
  SharedPreferences get sharedPreferences => _sharedPreferences;
}

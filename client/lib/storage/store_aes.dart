import 'package:encrypt/encrypt.dart';

///The class which makes it possible to encrypt and decrypt Strings
class StoreAES {
  ///The secret key
  final _aesKey;
  final _iv = IV.fromLength(16);

  StoreAES(this._aesKey);

  ///Encrypting [plainText] using AES Cipher with the key [_aesKey]
  String encrypt(String plainText) {
    final encrypter = Encrypter(AES(_aesKey));
    final encrypted = encrypter.encrypt(plainText, iv: _iv);
    return encrypted.base64;
  }

  ///Decrypting [encrypted] using AES Cipher with the key [_aesKey]
  String decrypted(String encrypted) {
    final encrypter = Encrypter(AES(_aesKey));
    final decrypted = encrypter.decrypt64(encrypted, iv: _iv);
    return decrypted;
  }
}

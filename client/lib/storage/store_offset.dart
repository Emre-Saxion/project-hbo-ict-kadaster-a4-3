import 'dart:convert';

import 'package:client/model/offset_info.dart';
import 'package:logging/logging.dart';
import 'package:shared_preferences/shared_preferences.dart';

///The class used for storing offsets
///This class exists because the current [StorageManager] should not be used for storing tens of thousands of objects
class StoreOffsetManager {
  final Logger _logger = Logger("StoreOffsetManager");

  SharedPreferences _preferences;

  ///Grabbing the offsets of an appointment by appointment id [appointmentId]
  List<OffsetInfo> getOffsets(int appointmentId) {
    final String value = _preferences.getString("offsets-$appointmentId");
    if (value == null) {
      return [];
    }

    return value
        .split("///")
        .where((element) => element.isNotEmpty)
        .map((e) => OffsetInfo.fromJson(jsonDecode(e)))
        .toList();
  }

  ///Inserting the offsets of an appointment by appointment id [appointmentId]
  void storeOffsets(int appointmentId, List<OffsetInfo> offsetInfo) {
    _logger.info("Storing offsets for appointment $appointmentId");
    final StringBuffer stringBuffer = StringBuffer();
    for (final OffsetInfo info in offsetInfo) {
      stringBuffer.write(jsonEncode(info));
      stringBuffer.write("///");
    }
    _preferences.setString("offsets-$appointmentId", stringBuffer.toString());
  }

  ///Deleting the offsets of an appointment by appointment id [appointmentId]
  void deleteOffsets(int appointmentId) {
    _logger.info("Removing offsets for $appointmentId");
    _preferences.remove("offsets-$appointmentId");
  }

  SharedPreferences get preferences => _preferences;

  set preferences(SharedPreferences value) {
    _preferences = value;
  }

  static StoreOffsetManager _instance;

  static StoreOffsetManager getInstance() {
    if (_instance == null) {
      _instance = StoreOffsetManager();
    }
    return _instance;
  }
}

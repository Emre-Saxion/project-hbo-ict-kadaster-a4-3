
import 'dart:convert';

import 'package:client/model/request.dart';
import 'package:client/storage/store.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RequestStore extends Store<List<Request>> {
  RequestStore(SharedPreferences sharedPreferences, String key) : super(sharedPreferences, key);

  @override
  void delete() {
    deleteList();

    print(getList());
  }

  @override
  List<Request> get() {
    return getList().map((e) => Request.fromJson(json.decode(e))).toList();
  }

  @override
  void insert(List<Request> object) {
    deleteList();
    insertList(object);
  }

}
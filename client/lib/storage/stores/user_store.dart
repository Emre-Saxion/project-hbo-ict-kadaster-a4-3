import 'dart:convert';

import 'package:client/model/user.dart';
import 'package:client/storage/store_aes.dart';
import 'package:encrypt/encrypt.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../store.dart';

///The store used for storing the user, which is using AES encryption
class UserStore extends Store<User> {
  UserStore(SharedPreferences _sharedPreferences, String _key)
      : super(_sharedPreferences, _key);

  final StoreAES storeAES =
      StoreAES(Key.fromUtf8("1pHoUCffNvaUmNB4l7zo30FyA26TVAET"));

  @override
  User get() {
    final user = getObject();
    if (user == null) {
      return null;
    }
    final decrypted = storeAES.decrypted(user);
    return User.fromJson(json.decode(decrypted));
  }

  @override
  void insert(User object) {
    final encrypted = storeAES.encrypt(jsonEncode(object));
    this.sharedPreferences.setString(this.key, encrypted);
  }

  @override
  void delete() {
    deleteObject();
  }
}

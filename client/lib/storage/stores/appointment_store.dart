import 'dart:convert';

import 'package:client/model/appointment/appointment.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../store.dart';

///The store used for storing a list of appointments
class AppointmentStore extends Store<List<Appointment>> {
  AppointmentStore(SharedPreferences sharedPreferences, String key)
      : super(sharedPreferences, key);

  @override
  void delete() {
    deleteList();
  }

  @override
  List<Appointment> get() {
    return getList().map((e) => Appointment.fromJson(json.decode(e))).toList();
  }

  @override
  void insert(List<Appointment> object) {
    deleteList();
    insertList(object);
  }
}

import 'package:client/storage/store_offset.dart';
import 'package:client/storage/stores/appointment_store.dart';
import 'package:client/storage/stores/request_store.dart';
import 'package:logging/logging.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'store.dart';
import 'stores/user_store.dart';

///The storage manager used for managing the available stores
class StorageManager {
  final Logger _logger = Logger("StorageManager");

  ///The SharedPreferences where everything is stored
  SharedPreferences _preferences;

  ///The stores
  final Map<String, Store> _stores = {};

  ///Initializing the stores
  void _initStores() {
    _stores["user"] = UserStore(_preferences, "user");
    _stores["appointment"] = AppointmentStore(_preferences, "appointment");
    _stores["request"] = RequestStore(_preferences, "request");
  }

  ///Grabbing a store based on the key
  Store getStore(String key) {
    return _stores[key];
  }

  SharedPreferences get preferences => _preferences;

  ///Initializing the stores of this manager and [StoreOffsetManager]
  set preferences(SharedPreferences value) {
    _logger.info("Initializing storage manager");
    _preferences = value;
    StoreOffsetManager.getInstance().preferences = value;
    _initStores();
  }

  static StorageManager _instance;

  static StorageManager getInstance() {
    if (_instance == null) {
      _instance = StorageManager();
    }
    return _instance;
  }
}

import 'package:flutter/material.dart';

class LoginBloc extends ChangeNotifier {

  ///If the user is logging in
  bool _loggingIn = false;

  ///If there has been an error while logging in
  bool _error = false;

  ///If the user has correctly logged in
  bool _loggedIn = false;

  ///The error message to display
  String _errorMessage = "";

  bool get loggingIn => _loggingIn;

  bool get error => _error;

  bool get loggedIn => _loggedIn;

  String get errorMessage => _errorMessage;

  set loggingIn(bool value) {
    _error = false;
    _loggingIn = value;
    notifyListeners();
  }

  set error(bool value) {
    _error = value;
    _loggingIn = false;
    notifyListeners();
  }

  void sendErrorWithMessage(String errorMessage) {
    _errorMessage = errorMessage;
    _error = true;
    _loggingIn = false;
    notifyListeners();
  }

  set loggedIn(bool value) {
    _errorMessage = "";
    _loggingIn = false;
    _loggedIn = value;
    notifyListeners();
  }
}

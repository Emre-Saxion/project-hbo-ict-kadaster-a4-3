import 'package:flutter/material.dart';

class AppointmentsBloc extends ChangeNotifier {

  ///If the appointments are being loading
  bool _loading = false;

  bool get loading => _loading;

  set loading(bool value) {
    _loading = value;
    notifyListeners();
  }
}

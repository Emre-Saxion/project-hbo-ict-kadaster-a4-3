import 'dart:async';

import 'package:client/model/offset_info.dart';
import 'package:flutter/material.dart';

class MapDrawingBloc extends ChangeNotifier {
  ///If the user is currently drawing
  bool _drawing = false;

  ///If the user is currently zooming, true by default
  bool _zooming = true;

  ///If the user is currently erasing
  bool _erasing = false;

  bool _lines = false;

  ///If the user has clicked on send map then this will be set to true
  ///Once disabled the user can't do any other action if using the setter
  bool _disabled = false;

  ///The currently selected color
  Color _color = Colors.blue;

  ///The matrix used for zooming and moving the map
  Matrix4 _matrix4 = Matrix4.identity();

  ///The currently selected range, which is basically the size of the circles that will be drawn
  double _range = 1;

  ///The list containing all the offsets with their information
  List<OffsetInfo> _offsetInfo = [];

  bool get drawing => _drawing;

  Color get color => _color;

  bool get zooming => _zooming;

  bool get erasing => _erasing;

  bool get lines => _lines;

  Matrix4 get matrix4 => _matrix4;

  double get range => _range;

  bool get disabled => _disabled;

  List<OffsetInfo> get offsetInfo => _offsetInfo;

  set drawing(bool value) {
    _drawing = value;
    _zooming = false;
    _erasing = false;
    _lines = false;
    notifyListeners();
  }

  set zooming(bool value) {
    _zooming = value;
    _drawing = false;
    _erasing = false;
    _lines = false;
    notifyListeners();
  }

  set erasing(bool value) {
    _erasing = value;
    _zooming = false;
    _drawing = false;
    _lines = false;
    notifyListeners();
  }

  set color(Color value) {
    _color = value;
    notifyListeners();
  }


  set lines(bool value) {
    _zooming = false;
    _drawing = false;
    _erasing = false;
    _lines = value;
    notifyListeners();
  }

  set matrix4(Matrix4 value) {
    _matrix4 = value;
    notifyListeners();
  }

  set range(double value) {
    _range = value;
    notifyListeners();
  }

  set offsetInfo(List<OffsetInfo> value) {
    _offsetInfo = value;
    notifyListeners();
  }

  void addOffset(OffsetInfo offsetInfo) {
    _offsetInfo.add(offsetInfo);
  }

  set disabled(bool value) {
    _drawing = false;
    _zooming = false;
    _erasing = false;
    _disabled = value;
    notifyListeners();
  }

  @override
  void dispose() {
    super.dispose();
  }
}

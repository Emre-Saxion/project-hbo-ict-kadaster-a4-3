
import 'package:flutter/material.dart';

class MapDownloadingBloc extends ChangeNotifier {

  ///If the user is currently downloading the file
  bool _downloading = false;

  ///If the file has been downloaded
  bool _downloaded = false;

  bool get downloading => _downloading;

  bool get downloaded => _downloaded;

  set downloading(bool value) {
    _downloading = value;
    notifyListeners();
  }

  set downloaded(bool value) {
    _downloading = false;
    _downloaded = value;
    notifyListeners();
  }
}
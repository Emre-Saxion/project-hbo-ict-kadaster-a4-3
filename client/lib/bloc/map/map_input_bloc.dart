
import 'package:client/model/appointment/appointment_file.dart';
import 'package:client/model/appointment/authorizer.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class MapInputBloc extends ChangeNotifier {

  ///The image picker used for opening the camera
  final _picker = ImagePicker();

  ///The list containing boolean values for the stakeholders that are present (false or true)
  List<bool> _present = [];

  List<Authorizer> _authorizers = [];

  ///The note text currently typed
  String _noteText = "";

  ///The measurement text currently typed
  String _measurementText = "";

  ///The TextEditingController used for the actual typing of the note
  TextEditingController _noteEditingController = TextEditingController();

  ///The TextEditingController used for the actual typing of the measurement
  TextEditingController _measurementEditingController = TextEditingController();

  ///If the user is sending the map to the server
  bool _sendingMap = false;

  bool _unanimous = false;

  bool _compatible = false;

  List<AppointmentFile> _addedFiles = [];

  List<bool> get present => _present;

  String get noteText => _noteText;

  TextEditingController get noteEditingController => _noteEditingController;

  String get measurementText => _measurementText;

  TextEditingController get measurementEditingController =>
      _measurementEditingController;

  bool get sendingMap => _sendingMap;

  get picker => _picker;

  List<AppointmentFile> get addedFiles => _addedFiles;

  bool get unanimous => _unanimous;

  bool get compatible => _compatible;

  List<Authorizer> get authorizers => _authorizers;

  set present(List<bool> value) {
    _present = value;
    notifyListeners();
  }

  void setPresent(int index, bool value) {
    _present[index] = value;
    notifyListeners();
  }

  void addPresent(bool value) {
    _present.add(value);
    notifyListeners();
  }

  void addAuthorizer(Authorizer authorizer) {
    _authorizers.add(authorizer);
    notifyListeners();
  }

  void setAuthorizer(int index, Authorizer value) {
    _authorizers[index] = value;
    notifyListeners();
  }


  set noteText(String value) {
    _noteText = value;
    notifyListeners();
  }

  void addAppointmentFile(AppointmentFile appointmentFile) {
    if (_addedFiles.contains(appointmentFile)) {
      return;
    }
    _addedFiles.add(appointmentFile);
    notifyListeners();
  }

  void deleteAppointmentFile(AppointmentFile appointmentFile) {
    _addedFiles.removeWhere((element) => element == appointmentFile);
    notifyListeners();
  }

  @override
  void dispose() {
    _noteEditingController.dispose();
    _measurementEditingController.dispose();
    super.dispose();
  }

  set sendingMap(bool value) {
    _sendingMap = value;
    notifyListeners();
  }

  set measurementText(String value) {
    _measurementText = value;
    notifyListeners();
  }

  set compatible(bool value) {
    _compatible = value;
    notifyListeners();
  }

  set unanimous(bool value) {
    _unanimous = value;
    notifyListeners();
  }
}
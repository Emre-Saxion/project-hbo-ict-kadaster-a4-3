import 'package:client/model/available_date.dart';
import 'package:flutter/material.dart';

class AvailablePageBloc extends ChangeNotifier {

  ///The available dates of the user
  final List<AvailableDate> _availableDates = [];

  ///If the user has clicked on the available button, true by default
  bool _availableClicked = true;

  ///If the user has clicked on the unavailable button
  bool _unavailableClicked = false;

  ///If the user is sending the new updated dates to the server
  bool _sending = false;

  List<AvailableDate> get availableDates => _availableDates;

  bool get unavailableClicked => _unavailableClicked;

  bool get availableClicked => _availableClicked;

  bool get sending => _sending;

  ///Adding a new date or changing an existing one if it already exists in [_availableDates]
  void addDate(AvailableDate availableDate) {
    for (AvailableDate date in _availableDates) {
      if (sameDate(date.date, availableDate.date)) {
        int index = _availableDates.indexWhere(
            (element) => sameDate(element.date, availableDate.date));
        if (index != -1) {
          _availableDates[index] = availableDate;
          notifyListeners();
        }
        return;
      }
    }
    _availableDates.add(availableDate);
    notifyListeners();
  }

  void removeDate(DateTime dateTime) {
    _availableDates.removeWhere((element) => sameDate(element.date, dateTime));
    notifyListeners();
  }

  ///Checking if [first] matches [second]
  bool sameDate(DateTime first, DateTime second) {
    return first.year == second.year &&
        first.month == second.month &&
        first.day == second.day;
  }

  set unavailableClicked(bool value) {
    _availableClicked = false;
    _unavailableClicked = value;
    notifyListeners();
  }

  set availableClicked(bool value) {
    _unavailableClicked = false;
    _availableClicked = value;
    notifyListeners();
  }

  set sending(bool value) {
    _sending = value;
    notifyListeners();
  }
}

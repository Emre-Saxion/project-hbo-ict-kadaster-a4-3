import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class VerifyBloc extends ChangeNotifier {

  ///If the user has been verified
  bool _verified = false;

  ///If there has been an error while verifying
  bool _error = false;

  Widget _currentShownAnimatedWidget;

  bool get verified => _verified;

  bool get error => _error;

  Widget get currentShownAnimatedWidget => _currentShownAnimatedWidget;

  set verified(bool value) {
    _error = false;
    _verified = value;
    notifyListeners();
  }

  set currentShownAnimatedWidget(Widget value) {
    _currentShownAnimatedWidget = value;
    notifyListeners();
  }

  set error(bool value) {
    _error = value;
    notifyListeners();
  }
}

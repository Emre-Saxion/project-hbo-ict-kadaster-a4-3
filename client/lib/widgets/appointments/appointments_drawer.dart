import 'package:client/constants/kadaster_colors.dart';
import 'package:client/pages/available_page.dart';
import 'package:client/pages/history_page.dart';
import 'package:client/pages/login_page.dart';
import 'package:client/storage/storage_manager.dart';
import 'package:client/util/alert_util.dart';
import 'package:client/widgets/login/login_kadaster_logo.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';

class AppointmentsDrawer extends StatelessWidget {

  final Logger _logger = Logger("AppointmentsDrawer");

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          Container(
            child: Padding(
              padding: EdgeInsets.only(top: 24, bottom: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  LoginKadasterLogo(),
                  SizedBox(
                    height: 12.0,
                  ),
                  Text(
                    _title(),
                    style: TextStyle(color: Colors.white, fontSize: 24),
                  ),
                ],
              ),
            ),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  KadasterColors.mainColor,
                  KadasterColors.secondaryColor.withOpacity(0.9),
                  KadasterColors.mainColor,
                ],
                stops: [0.1, 0.4, 0.9],
              ),
            ),
          ),
          Theme(
            data: ThemeData(
              splashColor: KadasterColors.secondaryColor.withOpacity(0.5),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 8, right: 6, left: 6),
              child: ListTile(
                leading: Icon(
                  Icons.history,
                  color: KadasterColors.secondaryColor,
                ),
                title: Text("Geschiedenis"),
                onTap: () {
                  _logger.info("Opening history page");
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext context) {
                    return HistoryPage();
                  }));
                },
              ),
            ),
          ),
          Theme(
            data: ThemeData(
              splashColor: KadasterColors.secondaryColor.withOpacity(0.5),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 8, right: 6, left: 6),
              child: ListTile(
                leading: Icon(
                  Icons.calendar_today,
                  color: KadasterColors.secondaryColor,
                ),
                title: Text("Beschikbaarheid"),
                onTap: () {
                  _logger.info("Opening available page");
                  Navigator.push(context,
                      MaterialPageRoute(builder: (BuildContext context) {
                    return AvailablePage();
                  }));
                },
              ),
            ),
          ),
          Theme(
            data: ThemeData(
              splashColor: KadasterColors.secondaryColor.withOpacity(0.5),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 8, right: 6, left: 6),
              child: ListTile(
                key: Key("log_off_list_tile"),
                leading: Icon(
                  Icons.person,
                  color: KadasterColors.errorColor[600],
                ),
                title: Text(
                  "Uitloggen",
                  style: TextStyle(color: KadasterColors.errorColor[600]),
                ),
                onTap: () {
                  _logOffAlert(context);
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  ///Showing the correct full name
  String _title() {
    final user = StorageManager.getInstance().getStore("user").get();
    return "${user.firstName} ${user.lastName}";
  }

  ///Showing the log off dialog
  void _logOffAlert(BuildContext context) {
    _logger.info("Showing log off dialog");
    AlertUtil.sendConfirmationAlert(context, "Uitloggen",
        "Weet je zeker dat je wil uitloggen?", (context) => _logOff(context),
        barrierColor: Colors.black54);
  }

  ///The actual logging off by deleting the user in [StorageManager]
  void _logOff(BuildContext context) {
    _logger.info("Logging off");
    StorageManager.getInstance().getStore("user").delete();
    StorageManager.getInstance().getStore("appointment").delete();
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
  }
}

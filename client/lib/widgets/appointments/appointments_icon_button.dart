import 'package:flutter/material.dart';

typedef void OnClick(BuildContext context);

class AppointmentsIconButton extends StatelessWidget {
  final Icon _icon;

  final double _size;

  final String _text;

  final OnClick _onClick;

  AppointmentsIconButton(this._icon, this._size, this._text, this._onClick);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        IconButton(
          iconSize: _size,
          icon: _icon,
          onPressed: () {
            _onClick.call(context);
          },
        ),
        if (_text.length > 0)
          GestureDetector(
            onTap: () {
              _onClick.call(context);
            },
            child: SizedBox(
              height: 8,
            ),
          ),
        if (_text.length > 0)
          GestureDetector(
            onTap: () {
              _onClick.call(context);
            },
            child: Center(
              child: Text(
                _text,
                textAlign: TextAlign.center,
              ),
            ),
          ),
      ],
    );
  }
}

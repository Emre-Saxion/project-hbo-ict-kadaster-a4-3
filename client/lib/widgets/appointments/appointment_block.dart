import 'package:client/bloc/map/map_drawing_bloc.dart';
import 'package:client/bloc/map/map_input_bloc.dart';
import 'package:client/constants/constants.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:client/model/appointment/address.dart';
import 'package:client/model/appointment/appointment.dart';
import 'package:client/pages/map_page.dart';
import 'package:client/util/screen_util.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:maps_launcher/maps_launcher.dart';

import 'appointments_icon_button.dart';

class AppointmentBlock extends StatelessWidget {
  final Logger _logger = Logger("AppointmentBlock");

  final DateTime _dateTime;

  final List<Appointment> _appointments;

  final bool _history;

  AppointmentBlock(this._dateTime, this._appointments, this._history);

  @override
  Widget build(BuildContext context) {
    final ScreenUtil screenUtil = ScreenUtil(context);

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: KadasterColors.borderColor, width: 2.0),
          borderRadius: BorderRadius.circular(12),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      _dateTextMonth(),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              thickness: 2.0,
            ),
            for (int i = 0; i < _appointments.length; i++)
              _appointmentWidget(screenUtil, i, context),
          ],
        ),
      ),
    );
  }

  Widget _appointmentWidget(ScreenUtil screenUtil, int index, BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => MapPage(
                        _appointments[index],
                        _mapDrawingBloc(_appointments[index]),
                        _mapInputBloc(_appointments[index]),
                        _history)));
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      _dateTextTime(_appointments[index]),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                      ),
                    ),
                    Center(
                      child: Container(
                        width: 40,
                        child: Divider(
                          thickness: 2.0,
                        ),
                      ),
                    ),
                    Text(
                      _dateTextSecondTime(_appointments[index]),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: screenUtil.getScreenHeightPercentage(7),
                child: VerticalDivider(
                  thickness: 2.0,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: screenUtil.getScreenWidthPercentage(40),
                        child: Text(
                          _addressText(_appointments[index]),
                        ),
                      ),
                      SizedBox(
                        height: 8.0,
                      ),
                      Text(
                        _correctType(_appointments[index]),
                      ),
                    ],
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 12),
                      child: AppointmentsIconButton(
                        Icon(
                          Constants.DIRECTION,
                          color: KadasterColors.secondaryColor,
                        ),
                        32,
                        "Route",
                        (context) {
                          final Address address =
                              _appointments[index].stakeholders[0].address;
                          _logger
                              .info("Opening google maps for address=$address");
                          MapsLauncher.launchQuery(
                              '${address.street} ${address.number} ${address.city}');
                        },
                      ),
                    ),
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 12),
                      child: AppointmentsIconButton(
                        Icon(
                          Icons.keyboard_arrow_right,
                          color: KadasterColors.secondaryColor,
                        ),
                        32,
                        "Details",
                        (context) {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => MapPage(
                                      _appointments[index],
                                      _mapDrawingBloc(_appointments[index]),
                                      _mapInputBloc(_appointments[index]),
                                      _history)));
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        if (index < _appointments.length - 1)
          Divider(
            thickness: 2.0,
          ),
        if (index == _appointments.length - 1)
          SizedBox(
            height: 8,
          ),
      ],
    );
  }

  ///Setting the correct MapInputBloc based on if [_history] is true or not
  MapInputBloc _mapInputBloc(Appointment appointment) {
    if (_history) {
      _logger.info("History clicked, initializing filled MapInputBloc");
      final MapInputBloc mapInputBloc = MapInputBloc();
      if (appointment.stakeholders != null) {
        for (int i = 0; i < appointment.stakeholders.length; i++) {
          mapInputBloc.addPresent(appointment.stakeholders[i].present);
          mapInputBloc.addAuthorizer(appointment.stakeholders[i].authorizer);
        }
      }
      if (appointment.addedFiles != null) {
        for (int i = 0; i < appointment.addedFiles.length; i++) {
          mapInputBloc.addAppointmentFile(appointment.addedFiles[i]);
        }
      }
      mapInputBloc.noteEditingController.text =
          appointment.note == null ? "" : appointment.note;
      mapInputBloc.noteText = appointment.note == null ? "" : appointment.note;

      mapInputBloc.measurementEditingController.text =
          appointment.measurements == null ? "" : appointment.measurements;
      mapInputBloc.measurementText =
          appointment.measurements == null ? "" : appointment.measurements;

      mapInputBloc.compatible = appointment.compatible;
      mapInputBloc.unanimous = appointment.unanimous;
      return mapInputBloc;
    }
    _logger.info("No history, initializing empty MapInputBloc");
    final MapInputBloc mapInputBloc = MapInputBloc();
    for (int i = 0; i < appointment.stakeholders.length; i++) {
      mapInputBloc.addPresent(false);
      mapInputBloc.addAuthorizer(null);
    }
    return mapInputBloc;
  }

  ///Setting the correct MapDrawingBloc based on if [_history] is true or not
  MapDrawingBloc _mapDrawingBloc(Appointment appointment) {
    _logger.info("initializing empty MapDrawingBloc");
    return MapDrawingBloc();
  }

  ///Showing the correct date
  String _dateTextMonth() {
    final days = {
      1: "Maandag",
      2: "Dinsdag",
      3: "Woensdag",
      4: "Donderdag",
      5: "Vrijdag",
      6: "Zaterdag",
      7: "Zondag",
    };
    final months = {
      1: "Januari",
      2: "Februari",
      3: "Maart",
      4: "April",
      5: "Mei",
      6: "Juni",
      7: "Juli",
      8: "Augustus",
      9: "September",
      10: "Oktober",
      11: "November",
      12: "December",
    };
    return "${days[_dateTime.weekday]} ${_dateTime.day} ${months[_dateTime.month]}";
  }

  String _dateTextTime(Appointment appointment) {
    final formatter = new DateFormat('HH:mm');
    return "${formatter.format(appointment.appointmentDate)}";
  }

  String _dateTextSecondTime(Appointment appointment) {
    final formatter = new DateFormat('HH:mm');
    return "${formatter.format(appointment.appointmentEndDate)}";
  }

  String _addressText(Appointment appointment) {
    final Address address = appointment.address;
    return "${address.street} ${address.number}, ${address.zipCode}, ${address.city}";
  }

  ///Showing correct type based on [_appointment.type]
  String _correctType(Appointment appointment) {
    if (appointment.type == 0) {
      return "";
    }
    final List<String> types = [
      "Stuk grond (ver)kopen",
      "De erfgrens bepalen",
      "Conflict over grens",
      "Boom of heg naast erfgrens",
      "Grens zelf opmeten"
    ];
    return types[appointment.type - 1];
  }
}

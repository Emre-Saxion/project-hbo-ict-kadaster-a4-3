import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:client/bloc/appointments_bloc.dart';
import 'package:client/constants/constants.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:client/file/file_manager.dart';
import 'package:client/model/appointment/appointment.dart';
import 'package:client/model/appointment/appointment_file.dart';
import 'package:client/model/request.dart';
import 'package:client/model/user.dart';
import 'package:client/storage/storage_manager.dart';
import 'package:client/storage/store_offset.dart';
import 'package:client/util/error_message_http.dart';
import 'package:client/util/internet_connection.dart';
import 'package:client/util/no_inkwell_scroll.dart';
import 'package:client/util/screen_util.dart';
import 'package:client/widgets/appointments/appointment_block.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

class AppointmentsBody extends StatefulWidget {
  ///If the page has been opened as a history page
  final bool _history;

  AppointmentsBody(this._history);

  @override
  _AppointmentsBodyState createState() => _AppointmentsBodyState();
}

class _AppointmentsBodyState extends State<AppointmentsBody>
    with AfterLayoutMixin<AppointmentsBody> {
  ///The streamController containing a list of appointments or an error
  final StreamController _streamController = StreamController();

  final Logger _logger = Logger("AppointmentsBody");

  @override
  void dispose() {
    _streamController.close();
    super.dispose();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    final AppointmentsBloc appointmentsBloc =
        Provider.of<AppointmentsBloc>(context, listen: false);
    _appointmentsWidgets(appointmentsBloc);
  }

  @override
  Widget build(BuildContext context) {
    final ScreenUtil screenUtil = ScreenUtil(context);
    final AppointmentsBloc appointmentsBloc =
        Provider.of<AppointmentsBloc>(context);

    final StreamBuilder streamBuilder = StreamBuilder(
      stream: _streamController.stream,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData && !appointmentsBloc.loading) {
          return ListView(
            physics: AlwaysScrollableScrollPhysics(),
            children: snapshot.data,
          );
        }
        if (snapshot.hasError) {
          return ScrollConfiguration(
            behavior: NoInkWellScroll(),
            child: ListView(
              physics: AlwaysScrollableScrollPhysics(),
              children: [
                SizedBox(
                  height: screenUtil.getScreenHeightPercentage(40),
                ),
                Center(
                  child: Text("Geen afspraken gevonden",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 26)),
                ),
              ],
            ),
          );
        }
        return Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(KadasterColors.mainColor),
          ),
        );
      },
    );

    final Widget parent = widget._history
        ? streamBuilder
        : RefreshIndicator(
            color: KadasterColors.mainColor,
            onRefresh: () async {
              if (widget._history) {
                return;
              }
              _logger.info("Refreshing appointments");
              _appointmentsWidgets(appointmentsBloc);
            },
            child: streamBuilder,
          );
    return parent;
  }

  ///Adding the appointments to [_streamController]
  Future<void> _appointmentsWidgets(AppointmentsBloc appointmentsBloc) async {
    final User user = StorageManager.getInstance().getStore("user").get();
    appointmentsBloc.loading = true;
    bool connection = await InternetConnection.connected();

    final appointments =
        connection ? await _appointmentsNetwork(user) : _appointmentsLocal();

    if (appointments == null || appointments.length <= 0) {
      _logger.warning("No appointments found");
      _streamController.addError(Exception("No appointments"));
      return;
    }

    appointmentsBloc.loading = false;
    final dates = <DateTime, List<Appointment>>{};
    for (final appointment in appointments) {
      final date = DateTime(appointment.appointmentDate.year,
          appointment.appointmentDate.month, appointment.appointmentDate.day);
      if (dates[date] == null) {
        dates[date] = [appointment];
        continue;
      }
      dates[date].add(appointment);
    }

    final List<Widget> appointmentBlocks = [];
    dates.forEach((key, value) {
      appointmentBlocks.add(AppointmentBlock(key, value, widget._history));
    });

    _streamController.add(appointmentBlocks);
  }

  ///Loading the appointments from the server, if updatdDate is null then only display if [widget._history] is false
  ///If updatedDate is not null then only display if [widget._history} is true
  Future<List<Appointment>> _appointmentsNetwork(User user) async {
    try {
      final List<Request> requests =
          StorageManager.getInstance().getStore("request").get();

      _logger.info("Found ${requests.length} old requests");
      if (requests.length > 0) {
        for (Request request in requests) {
          await _sendUpdateAppointment(request, user);

          if (request.addedFiles.length > 0) {
            await _uploadAddedFiles(request, request.addedFiles, user);
          }

          if (!await FileManager.getInstance()
              .appointmentMapFileExistsById(request.id)) {
            continue;
          }

          await _sendMap(request.id, user);
        }
      }

      StorageManager.getInstance().getStore("request").delete();

      final response = await Dio(
        BaseOptions(connectTimeout: Constants.CONNECTION_TIME_OUT_MS),
      ).get(
        Constants.URL + "/appointment/all",
        options: Options(
          headers: {"authorization": user.token},
        ),
      );

      final appointments = response.data
          .map<Appointment>((json) => Appointment.fromJson(json))
          .toList();

      _logger.info("Found ${appointments.length} appointments");

      for (final appointment in appointments) {
        await _downloadFile(user, appointment);
      }

      try {
        final List<Appointment> oldAppointments =
            StorageManager.getInstance().getStore("appointment").get();

        if (oldAppointments.length > 0) {
          _deleteOldFiles(oldAppointments, appointments);
        }
      } catch (e, stacktrace) {
        _logger.severe(
            "Error loading old appointments message=${ErrorMessageHttp.message(e)}",
            e,
            stacktrace);
      }


      _logger.info("Storing appointments to local");
      StorageManager.getInstance().getStore("appointment").insert(appointments);
      return _correctAppointments(appointments);
    } catch (e, stacktrace) {
      _logger.severe(
          "Error grabbing appointments or downloading map message=${ErrorMessageHttp.message(e)}",
          e,
          stacktrace);
      if (e is DioError) {
        if (e.type == DioErrorType.CONNECT_TIMEOUT) {
          _logger.info("Grabbing appointments from local");
          return _appointmentsLocal();
        } else {
          _logger.info("Displaying no appointments found");
          return [];
        }
      }
      return [];
    }
  }

  Future<void> _sendUpdateAppointment(Request request, User user) async {
    await Dio(
        BaseOptions(connectTimeout: Constants.CONNECTION_TIME_OUT_MS))
        .post(
      Constants.URL + "/appointment/update/${request.id}",
      data: {
        "note": request.note,
        "stakeholderPresent": request.stakeholderPresent,
        "authorizers": request.authorizers,
        "measurements": request.measurements,
        "addedFiles": request.addedFiles.map((e) => e.toJson()).toList(),
        "unanimous": request.unanimous,
        "compatible": request.compatible,
      },
      options: Options(
        headers: {
          "authorization": user.token,
        },
      ),
    );
  }

  Future<void> _uploadAddedFiles(Request request, List<AppointmentFile> files, User user) async {
    final List<String> paths = [];
    for (AppointmentFile appointmentFile in files) {
      final String path = await FileManager.getInstance()
          .appointmentFileLocationById(request.id, appointmentFile);
      paths.add(path);
    }

    final formData = FormData();
    formData.files.addAll(paths
        .map((e) => MapEntry("files", MultipartFile.fromFileSync(e)))
        .toList());

    await Dio(BaseOptions(
        connectTimeout: Constants.FILE_UPLOAD_CONNECTION_TIME_OUT_MS))
        .post(
      Constants.URL + "/upload/multi",
      data: formData,
      options: Options(
        headers: {"authorization": user.token, "id": request.id},
      ),
    );
  }

  Future<void> _sendMap(int id, User user) async {
    final path =
        await FileManager.getInstance().appointmentMapFileLocationById(id);
    final file = await MultipartFile.fromFile(path);

    final formData = FormData();
    formData.files.add(MapEntry("file", file));

    await Dio(BaseOptions(
            connectTimeout: Constants.FILE_UPLOAD_CONNECTION_TIME_OUT_MS))
        .post(
      Constants.URL + "/upload/map",
      data: formData,
      options: Options(
        headers: {"authorization": user.token, "id": id},
      ),
    );
  }

  ///Deleting old files if the id doesn't exist anymore.
  ///We delete the folder which has the name of the id
  Future<void> _deleteOldFiles(List<Appointment> oldAppointments,
      List<Appointment> newAppointments) async {
    _logger.info("Checking if appointments need to be deleted");
    final Set<int> oldIds = oldAppointments.map((e) => e.id).toSet();
    final Set<int> newIds = newAppointments.map((e) => e.id).toSet();

    oldIds.removeAll(newIds);

    oldIds.forEach((element) {
      _logger.info("Deleting appointment $element");
      FileManager.getInstance().deleteAppointmentFiles(element);
      StoreOffsetManager.getInstance().deleteOffsets(element);
    });
  }

  ///Loading appointments from local
  List<Appointment> _appointmentsLocal() {
    return _correctAppointments(
        StorageManager.getInstance().getStore("appointment").get());
  }

  ///Loading the correct appointments, if updatdDate is null then only display if [widget._history] is false
  ///If updatedDate is not null then only display if [widget._history} is true
  List<Appointment> _correctAppointments(List<Appointment> appointments) {
    if (widget._history) {
      _logger.info("Loading history appointments");
      return appointments
          .where((element) => element.updatedDate != null)
          .toList();
    }
    _logger.info("Loading appointments");
    return appointments
        .where((element) => element.updatedDate == null)
        .toList();
  }

  ///Downloading the map file
  Future<void> _downloadFile(User user, Appointment appointment) async {
    _logger.info("Downloading pdf file for appointment $appointment");
    final response = await Dio(
      BaseOptions(
          connectTimeout: Constants.FILE_DOWNLOAD_CONNECTION_TIME_OUT_MS),
    ).get(
      Constants.URL + "/download/file/${appointment.mapFile}",
      options: Options(headers: {
        "authorization": user.token,
        "id": appointment.id,
      }, responseType: ResponseType.bytes, followRedirects: false),
    );

    await FileManager.getInstance().saveAppointmentMap(
        response.data, "${appointment.id}", appointment.mapFile, appointment);
  }
}

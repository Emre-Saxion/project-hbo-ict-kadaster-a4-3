import 'dart:io';

import 'package:client/constants/kadaster_colors.dart';
import 'package:client/file/file_manager.dart';
import 'package:client/model/appointment/appointment.dart';
import 'package:client/model/appointment/appointment_file.dart';
import 'package:client/util/screen_util.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:native_pdf_renderer/native_pdf_renderer.dart';

class FilePreview {
  final Logger _logger = Logger("FilePreview");

  Future<Widget> previewWidgetAppointmentFile(ScreenUtil screenUtil,
      Appointment appointment, AppointmentFile appointmentFile) async {
    switch (appointmentFile.fileExtension) {
      case "pdf":
        final Image memoryImage =
            await openPDFAppointmentFile(appointment, appointmentFile);
        return Container(
          height: screenUtil
              .getScreenHeightPercentage(screenUtil.isPortrait() ? 20 : 28),
          width: screenUtil
              .getScreenWidthPercentage(screenUtil.isPortrait() ? 40 : 28),
          child: FittedBox(
            fit: BoxFit.fill,
            child: memoryImage,
          ),
        );
      case "jpg":
      case "jpeg":
      case "png":
        final path = await FileManager.getInstance()
            .appointmentFileLocation(appointment, appointmentFile);
        return Container(
          height: screenUtil
              .getScreenHeightPercentage(screenUtil.isPortrait() ? 20 : 28),
          width: screenUtil
              .getScreenWidthPercentage(screenUtil.isPortrait() ? 40 : 28),
          child: FittedBox(
            fit: BoxFit.fill,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(15.0),
              child: Image.file(
                File(path),
              ),
            ),
          ),
        );
      default:
        return Padding(
          padding: EdgeInsets.only(
            left: screenUtil.getScreenWidthPercentage(10),
            right: screenUtil.getScreenWidthPercentage(10),
          ),
          child: Icon(
            Icons.folder_open,
            size: 48,
            color: KadasterColors.secondaryColor,
          ),
        );
    }
  }

  Future<Image> openPDFAppointment(Appointment appointment, int scale) async {
    _logger.info("Loading pdf ${appointment.mapFile}");
    final PdfDocument document =
        await PdfDocument.openFile(appointment.mapFile);
    final page = await document.getPage(1);
    final pageImage = await page.render(
      width: page.width * scale,
      height: page.height * scale,
      format: PdfPageFormat.PNG,
    );
    await page.close();
    await document.close();
    return Image.memory(
      pageImage.bytes,
      scale: 2.0,
    );
  }

  Future<Image> openPDFAppointmentFile(
      Appointment appointment, AppointmentFile appointmentFile) async {
    final path = await FileManager.getInstance()
        .appointmentFileLocation(appointment, appointmentFile);
    _logger.info("Loading pdf $path");
    final PdfDocument document = await PdfDocument.openFile(path);
    final page = await document.getPage(1);
    final pageImage = await page.render(
      width: page.width * 2,
      height: page.height * 2,
      format: PdfPageFormat.PNG,
    );
    await page.close();
    await document.close();
    return Image.memory(pageImage.bytes);
  }
}

import 'package:client/bloc/available_page_bloc.dart';
import 'package:client/constants/constants.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:client/model/available_date.dart';
import 'package:client/model/user.dart';
import 'package:client/storage/storage_manager.dart';
import 'package:client/util/alert_util.dart';
import 'package:client/util/error_message_http.dart';
import 'package:client/util/internet_connection.dart';
import 'package:client/util/no_inkwell_scroll.dart';
import 'package:client/util/screen_util.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

class AvailablePageBody extends StatelessWidget {
  final Logger _logger = Logger("AvailablePageBody");

  @override
  Widget build(BuildContext context) {
    final AvailablePageBloc availablePageBloc =
        Provider.of<AvailablePageBloc>(context);

    final ScreenUtil screenUtil = ScreenUtil(context);
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return ScrollConfiguration(
          behavior: NoInkWellScroll(),
          child: SingleChildScrollView(
            primary: true,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      FlatButton.icon(
                        key: Key("available_button"),
                        onPressed: () {
                          if (availablePageBloc.availableClicked) {
                            return;
                          }
                          availablePageBloc.availableClicked = true;
                        },
                        icon: Icon(
                          Icons.event_available,
                          color: availablePageBloc.availableClicked
                              ? KadasterColors.mainColor
                              : Colors.black,
                        ),
                        textColor: availablePageBloc.availableClicked
                            ? KadasterColors.mainColor
                            : Colors.black,
                        label: Text(
                          "Beschikbaar",
                        ),
                      ),
                      FlatButton.icon(
                        key: Key("unavailable_button"),
                        onPressed: () {
                          if (availablePageBloc.unavailableClicked) {
                            return;
                          }
                          availablePageBloc.unavailableClicked = true;
                        },
                        icon: Icon(
                          Icons.not_interested,
                          color: availablePageBloc.unavailableClicked
                              ? KadasterColors.mainColor
                              : Colors.black,
                        ),
                        textColor: availablePageBloc.unavailableClicked
                            ? KadasterColors.mainColor
                            : Colors.black,
                        label: Text(
                          "Bezet",
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: CalendarCarousel(
                    isScrollable: screenUtil.isPortrait() ? false : true,
                    pageScrollPhysics: screenUtil.isPortrait()
                        ? NeverScrollableScrollPhysics()
                        : ScrollPhysics(),
                    height: screenUtil.isPortrait()
                        ? constraints.maxHeight / 1.52
                        : constraints.maxHeight,
                    daysHaveCircularBorder: false,
                    onDayPressed:
                        (DateTime date, List<EventInterface> events) async {
                      if (availablePageBloc.sending) {
                        return;
                      }
                      if (date.weekday == 6 || date.weekday == 7) {
                        Scaffold.of(context).showSnackBar(
                          SnackBar(
                            content: Text(
                              "Het is niet mogelijk om in het weekend in te plannen.",
                              style: TextStyle(
                                color: Colors.redAccent,
                              ),
                            ),
                          ),
                        );
                        _logger.info("Weekend clicked, can't change");
                        return;
                      }
                      if (date.difference(DateTime.now()).inDays < 0) {
                        Scaffold.of(context).showSnackBar(
                          SnackBar(
                            content: Text(
                              "Deze dag ligt in het verleden.",
                              style: TextStyle(
                                color: Colors.redAccent,
                              ),
                            ),
                          ),
                        );
                        _logger.info("Day before today clicked, can't change");
                        return;
                      }
                      for (AvailableDate availableDate
                          in availablePageBloc.availableDates) {
                        if (availablePageBloc.sameDate(
                                availableDate.date, date) &&
                            availableDate.extra.length > 0) {
                          Scaffold.of(context).showSnackBar(
                            SnackBar(
                              content: Text(
                                "Dit is een vrije dag.",
                                style: TextStyle(
                                  color: Colors.redAccent,
                                ),
                              ),
                            ),
                          );
                          return;
                        }
                      }
                      _onPressed(date, availablePageBloc, context);
                    },
                    onDayLongPressed: (dateTime) {},
                    todayButtonColor: Theme.of(context).scaffoldBackgroundColor,
                    todayBorderColor: Theme.of(context).scaffoldBackgroundColor,
                    customWeekDayBuilder: (int weekday, String weekdayName) {
                      final days = {
                        0: "Zo",
                        1: "Ma",
                        2: "Di",
                        3: "Woe",
                        4: "Don",
                        5: "Vrij",
                        6: "Zat",
                      };
                      if (weekday == 0 || weekday == 6) {
                        return Expanded(
                            child: Container(
                          margin: const EdgeInsets.only(bottom: 4.0),
                          padding: EdgeInsets.zero,
                          child: Center(
                            child: Text(
                              "${days[weekday]}",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        ));
                      }
                      return Expanded(
                          child: Container(
                        margin: const EdgeInsets.only(bottom: 4.0),
                        padding: EdgeInsets.zero,
                        child: Center(
                          child: Text(
                            "${days[weekday]}",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.red,
                            ),
                          ),
                        ),
                      ));
                    },
                    customDayBuilder: (bool isSelectable,
                        int index,
                        bool isSelectedDay,
                        bool isToday,
                        bool isPrevMonthDay,
                        TextStyle textStyle,
                        bool isNextMonthDay,
                        bool isThisMonthDay,
                        DateTime day) {
                      for (AvailableDate availableDate
                          in availablePageBloc.availableDates) {
                        if (availablePageBloc.sameDate(
                            availableDate.date, day)) {
                          if (isToday) {
                            return Container(
                              decoration: BoxDecoration(
                                color: availableDate.extra.length > 0
                                    ? KadasterColors.holidayColor
                                        .withOpacity(0.9)
                                    : availableDate.available
                                        ? KadasterColors.AVAILABLE_COLOR
                                            .withOpacity(0.9)
                                        : KadasterColors.unavailableColor
                                            .withOpacity(0.9),
                                border: Border.all(
                                    color: KadasterColors.borderColor,
                                    width: 3.0),
                              ),
                              width: double.infinity,
                              height: double.infinity,
                              child: Center(
                                child: Text(
                                  "${day.day}",
                                  style: TextStyle(
                                    color: isPrevMonthDay || isNextMonthDay
                                        ? KadasterColors.BLACK_COLOR
                                        : KadasterColors.WHITE_COLOR,
                                  ),
                                ),
                              ),
                            );
                          } else {
                            return Container(
                              color: availableDate.extra.length > 0
                                  ? KadasterColors.holidayColor
                                  : availableDate.available
                                      ? KadasterColors.AVAILABLE_COLOR
                                          .withOpacity(0.9)
                                      : KadasterColors.unavailableColor
                                          .withOpacity(0.9),
                              width: double.infinity,
                              height: double.infinity,
                              child: Center(
                                child: Text(
                                  "${day.day}",
                                  style: TextStyle(
                                    color: isPrevMonthDay || isNextMonthDay
                                        ? KadasterColors.BLACK_COLOR
                                        : KadasterColors.WHITE_COLOR,
                                  ),
                                ),
                              ),
                            );
                          }
                        }
                      }

                      if (isToday) {
                        return Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                                color: KadasterColors.mainColor, width: 3.0),
                          ),
                          child: Center(
                            child: Text("${day.day}"),
                          ),
                        );
                      }
                      if ((isNextMonthDay || isPrevMonthDay) &&
                          (day.weekday != 6 && day.weekday != 7)) {
                        return Container(
                          child: Center(
                            child: Text(
                              "${day.day}",
                              style: textStyle,
                            ),
                          ),
                        );
                      }

                      if (day.weekday == 6 || day.weekday == 7) {
                        return Container(
                          child: Center(
                            child: Text(
                              "${day.day}",
                              style: TextStyle(
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        );
                      }

                      return null;
                    },
                  ),
                ),
                SizedBox(
                  height: 48.0,
                ),
                FlatButton(
                  key: Key("save_button"),
                  color: KadasterColors.mainColor,
                  onPressed: () async {
                    if (availablePageBloc.sending) {
                      return;
                    }
                    _onSave(context, availablePageBloc);
                  },
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Text(
                    "Opslaan",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                if (availablePageBloc.sending)
                  Column(
                    children: [
                      SizedBox(
                        height: 12.0,
                      ),
                      CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(
                            KadasterColors.mainColor),
                      ),
                    ],
                  ),
                SizedBox(
                  height: 36.0,
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  ///What happens when we press on a valid date in the calendar
  void _onPressed(DateTime dateTime, AvailablePageBloc availablePageBloc,
      BuildContext context) async {
    if (availablePageBloc.availableClicked) {
      _logger.info("Adding available date at $dateTime");
      availablePageBloc.addDate(AvailableDate(dateTime, true, ""));
    } else {
      _logger.info("Adding unavailable date at $dateTime");
      availablePageBloc.addDate(AvailableDate(dateTime, false, ""));
    }
  }

  ///Sending the new dates to the server if the user is connected to the internet
  void _onSave(
      BuildContext context, AvailablePageBloc availablePageBloc) async {
    bool connected = await InternetConnection.connected();
    if (connected) {
      try {
        _logger.info("Sending new dates");
        availablePageBloc.sending = true;
        final User user = StorageManager.getInstance().getStore("user").get();

        await Dio(
          BaseOptions(connectTimeout: Constants.CONNECTION_TIME_OUT_MS),
        ).post(
          Constants.URL + "/employee/dates",
          data: {
            "availableDates":
                availablePageBloc.availableDates.map((e) => e.toJson()).toList()
          },
          options: Options(
            headers: {
              "authorization": user.token,
            },
          ),
        );

        user.availableDates = availablePageBloc.availableDates;
        StorageManager.getInstance().getStore("user").insert(user);

        AlertUtil.sendInfoAlert(context, "Opgeslagen",
            "De nieuwe datums zijn opgeslagen", (context) {});

        availablePageBloc.sending = false;
      } catch (e, stacktrace) {
        _logger.severe(
            "Error sending new dates message=${ErrorMessageHttp.message(e)}",
            e,
            stacktrace);
        AlertUtil.sendInfoAlert(context, "Geen verbinding",
            "Geen verbinding kunnen maken met de server", (context) {});
        availablePageBloc.sending = false;
      }
    } else {
      _logger.info("No internet connection found");
      AlertUtil.sendInfoAlert(context, "Geen internet",
          "Geen internet verbinding gevonden", (context) {});
    }
  }
}

import 'dart:async';

import 'package:client/bloc/verify_bloc.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:client/model/user.dart';
import 'package:client/pages/appointments_page.dart';
import 'package:client/storage/storage_manager.dart';
import 'package:client/util/alert_util.dart';
import 'package:client/util/screen_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logging/logging.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';

///The class used to validate the code given
class VerifyCodeInputValidator {
  ///Validating the code, the [code] needs to match the login code of the [user]
  ///Will return true if it matches the code otherwise false
  static bool validate(String code, User user) {
    return user.loginCode == code;
  }
}

class VerifyCodeInput extends StatefulWidget {
  @override
  _VerifyCodeInputState createState() => _VerifyCodeInputState();
}

class _VerifyCodeInputState extends State<VerifyCodeInput> {
  ///An errorController where you can make the pincodefield shake if you add to it
  final StreamController<ErrorAnimationType> errorController =
      StreamController<ErrorAnimationType>();

  ///The text editing controller of the pincodefield, this can be used to manipulate the text
  final TextEditingController _textEditingController = TextEditingController();

  ///The focus node of the pincodefield used for requesting focus back to open the keyboard
  final FocusNode _focusNode = FocusNode();

  final Logger _logger = Logger("VerifyCodeInput");

  @override
  void dispose() {
    errorController.close();
    _textEditingController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final VerifyBloc verifyBloc = Provider.of<VerifyBloc>(context);

    final ScreenUtil screenUtil = ScreenUtil(context);
    if (_focusNode.parent != null) {
      _focusNode.requestFocus();
    }
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: SizedBox(
            width: screenUtil.getScreenWidth() > 500 ? 500 : double.infinity,
            child: PinCodeTextField(
              key: Key("verification_code_field"),
              focusNode: _focusNode,
              autoFocus: true,
              inputFormatters: [
                WhitelistingTextInputFormatter.digitsOnly,
              ],
              enabled: !verifyBloc.verified && !verifyBloc.error,
              backgroundColor: KadasterColors.SCAFFOLD_BACKGROUND_COLOR,
              textInputType: TextInputType.number,
              autoDisposeControllers: false,
              length: 6,
              enableActiveFill: true,
              animationType: AnimationType.fade,
              animationDuration: const Duration(milliseconds: 300),
              onChanged: (String value) {},
              errorAnimationController: errorController,
              controller: _textEditingController,
              onCompleted: (value) {
                if (verifyBloc.verified) {
                  return;
                }
                final User user =
                    StorageManager.getInstance().getStore("user").get();
                if (!VerifyCodeInputValidator.validate(value, user)) {
                  _logger.info(
                      "Wrong code, given $value expected ${user.loginCode}");
                  verifyBloc.error = true;
                  errorController.add(ErrorAnimationType.shake);
                  _sendDelayedAlert(verifyBloc);
                  return;
                }
                _logger.info("Correct code entered $value");
                verifyBloc.verified = true;
                _setAnimatedWidget(verifyBloc, context);
              },
              pinTheme: PinTheme(
                shape: PinCodeFieldShape.circle,
                activeColor: KadasterColors.mainColor,
                activeFillColor: KadasterColors.mainColor,
                inactiveColor: KadasterColors.errorColor.withOpacity(0.7),
                inactiveFillColor: KadasterColors.errorColor.withOpacity(0.7),
                selectedColor: KadasterColors.secondaryColor.withOpacity(0.7),
                selectedFillColor:
                    KadasterColors.secondaryColor.withOpacity(0.7),
                disabledColor: verifyBloc.verified
                    ? Colors.grey
                    : KadasterColors.errorColor,
                borderRadius: BorderRadius.circular(5),
                fieldHeight: 50,
                fieldWidth: 40,
              ),
            ),
          ),
        ),
        SizedBox(
          height: 24,
        ),
        verifyBloc.verified
            ? AnimatedSwitcher(
                duration: const Duration(milliseconds: 500),
                child: verifyBloc.currentShownAnimatedWidget,
              )
            : SizedBox.shrink(),
      ],
    );
  }

  ///Starting the verify code animation after the user has been verified
  void _setAnimatedWidget(VerifyBloc verifyBloc, BuildContext context) {
    verifyBloc.currentShownAnimatedWidget = Container(
      key: UniqueKey(),
      child: Column(
        children: [
          SizedBox(
            height: 6,
          ),
          CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(KadasterColors.mainColor),
          ),
          SizedBox(
            height: 12,
          ),
        ],
      ),
    );

    _updateAnimatedWidgetDelayed(verifyBloc, context);
  }

  ///The second widget that gets updated after 500 milliseconds, for the animation
  void _updateAnimatedWidgetDelayed(
      VerifyBloc verifyBloc, BuildContext context) {
    Future.delayed(const Duration(milliseconds: 500)).then((value) {
      verifyBloc.currentShownAnimatedWidget = Container(
        key: Key("verify_code_ok_icon"),
        child: Column(
          children: [
            Icon(
              Icons.check_circle,
              color: KadasterColors.okColor,
              size: 42,
            ),
            SizedBox(
              height: 12,
            ),
          ],
        ),
      );
      _sendToAppointmentsPage(verifyBloc);
    });
  }

  ///Sending the user to the next page with a delay
  void _sendToAppointmentsPage(VerifyBloc verifyBloc) {
    Future.delayed(const Duration(milliseconds: 500)).then((value) async {
      _goToAppointmentsPage();
    });
  }

  ///Sending a delayed error alert
  void _sendDelayedAlert(VerifyBloc verifyBloc) {
    Future.delayed(const Duration(milliseconds: 1000)).then((value) async {
      await AlertUtil.sendInfoAlert(
              context,
              "Verkeerde code",
              "De code komt niet overeen",
              (context) => _focusNode.requestFocus())
          .then((value) => _focusNode.requestFocus());
      verifyBloc.error = false;
      _textEditingController.text = "";
    });
  }

  ///The actual sending to the appointments page
  void _goToAppointmentsPage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => AppointmentsPage()));
  }
}

import 'package:client/constants/kadaster_colors.dart';
import 'package:flutter/material.dart';

class LoginKadasterLogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(200),
        border: Border.all(color: KadasterColors.borderColor, width: 2.0),
        boxShadow: [
          BoxShadow(
            color: KadasterColors.borderColor,
            blurRadius: 2,
            spreadRadius: 2,
            offset: Offset.fromDirection(0, 0),
          ),
        ],
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(400),
        child: Container(
          color: KadasterColors.SCAFFOLD_BACKGROUND_COLOR,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Image.asset(
              "images/kadaster.png",
              height: 100,
              width: 100,
            ),
          ),
        ),
      ),
    );
  }
}

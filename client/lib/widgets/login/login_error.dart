import '../../constants/kadaster_colors.dart';
import 'package:client/util/screen_util.dart';
import 'package:flutter/material.dart';

class LoginError extends StatelessWidget {

  final String _error;

  LoginError(this._error);

  @override
  Widget build(BuildContext context) {
    final ScreenUtil screenUtil = ScreenUtil(context);

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: KadasterColors.errorColor[600].withOpacity(0.5),
      ),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.all(6),
            child: Icon(
              Icons.error,
              color: KadasterColors.errorColor,
              size: 36,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(6),
            child: Container(
              width: screenUtil.getScreenWidthPercentage(60),
              child: Text(
                _error,
                softWrap: true,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

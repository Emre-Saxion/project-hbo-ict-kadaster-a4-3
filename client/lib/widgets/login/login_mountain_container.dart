import 'package:client/util/screen_util.dart';
import 'package:flutter/material.dart';

import '../../constants/kadaster_colors.dart';

class LoginMountainContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ScreenUtil screenUtil = ScreenUtil(context);
    return ClipPath(
      clipper: MountainClipper(),
      child: Container(
        height: screenUtil.getScreenHeightPercentage(35),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              KadasterColors.mainColor,
              KadasterColors.secondaryColor,
              KadasterColors.mainColor,
            ],
            stops: [0.0, 0.8, 1],
          ),
        ),
      ),
    );
  }
}

///The clipper used to clip the container to the correct shape
class MountainClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, size.height * 0.38);
    path.quadraticBezierTo(
        size.width / 10, size.height, size.width / 2, size.height);
    path.quadraticBezierTo(size.width - (size.width / 10), size.height,
        size.width, size.height * 0.38);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

import 'package:client/bloc/login_bloc.dart';
import 'package:client/constants/constants.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:client/model/user.dart';
import 'package:client/pages/appointments_page.dart';
import 'package:client/storage/storage_manager.dart';
import 'package:client/util/error_message_http.dart';
import 'package:client/util/internet_connection.dart';
import 'package:client/util/screen_util.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

import '../../constants/constants.dart';
import '../../constants/kadaster_colors.dart';
import 'login_error.dart';

///The class used to validate the login code
class LoginCodeValidation {
  static final Logger _logger = Logger("LoginCodeValidation");

  ///The function which returns an error String if [value] is invalid otherwise return null
  ///
  ///A login code is valid if the login code is numbers only and 6 characters long
  static String validate(String value) {
    if (value.length == 0) {
      _logger.info("Empty username");
      return "Gebruikersnaam mag niet leeg zijn";
    }
    if (int.tryParse(value) == null) {
      _logger.info("No number username");
      return "Gebruikersnaam mag alleen getallen bevatten";
    }
    if (value.length != 6) {
      _logger.info("Username not equal to 6 characters");
      return "Gebruikersnaam moet minimaal 6 cijfers lang zijn";
    }
    return null;
  }
}

///The class used to validate the password
class PasswordValidation {
  static final Logger _logger = Logger("PasswordValidation");

  ///The function which returns an error String if [value] is invalid otherwise return null
  ///
  ///A password is valid if the password is a minimum of 8 characters long
  static String validate(String value) {
    if (value.length == 0) {
      _logger.info("Empty password");
      return "Wachtwoord mag niet leeg zijn";
    }
    if (value.length < 8) {
      _logger.info("Password not equal to 8 characters");
      return "Wachtwoord moet minimaal 8 letters lang zijn";
    }
    return null;
  }
}

class LoginForm extends StatelessWidget {
  ///The key holding the username and password
  static final _formKey = GlobalKey<FormState>();

  final Logger _logger = Logger("LoginForm");

  ///The username entered
  ///This will be saved after save gets called on [_formKey]
  var _username;

  ///The password entered
  ///This will be saved after save gets called on [_formKey]
  var _password;

  @override
  Widget build(BuildContext context) {
    final ScreenUtil screenUtil = ScreenUtil(context);
    final LoginBloc loginBloc = Provider.of<LoginBloc>(context);

    return Container(
      width: screenUtil.getScreenWidthPercentage(80),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            TextFormField(
                key: Key("login_code_field"),
                inputFormatters: [
                  WhitelistingTextInputFormatter.digitsOnly,
                ],
                keyboardType: TextInputType.number,
                decoration: InputDecoration(hintText: "Gebruikersnaam"),
                validator: (value) => LoginCodeValidation.validate(value),
                onSaved: (value) {
                  _username = value;
                }),
            SizedBox(
              height: 24,
            ),
            TextFormField(
              key: Key("password_field"),
              obscureText: true,
              decoration: InputDecoration(hintText: "Wachtwoord"),
              validator: (value) => PasswordValidation.validate(value),
              onSaved: (value) {
                _password = value;
              },
            ),
            SizedBox(
              height: 48,
            ),
            Container(
              width: screenUtil.getScreenWidthPercentage(60),
              child: RaisedButton(
                key: Key("login_button"),
                onPressed: () async {
                  if (loginBloc.loggingIn || loginBloc.loggedIn) {
                    return;
                  }
                  _sendLoginRequest(loginBloc, context);
                },
                child: Text(
                  "Aanmelden",
                  style: TextStyle(
                      color: KadasterColors.WHITE_COLOR, fontSize: 18),
                ),
                color: KadasterColors.mainColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: KadasterColors.WHITE_COLOR)),
              ),
            ),
            AnimatedSwitcher(
              duration: const Duration(milliseconds: 500),
              child: _correctWidget(loginBloc),
            ),
            SizedBox(
              height: 24,
            ),
          ],
        ),
      ),
    );
  }

  ///Showing the correct widget based on the state in [loginBloc]
  Widget _correctWidget(LoginBloc loginBloc) {
    if (loginBloc.loggingIn) {
      _logger.info("Logging in");
      return Container(
        key: UniqueKey(),
        child: Padding(
          padding: const EdgeInsets.only(top: 18),
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(KadasterColors.mainColor),
          ),
        ),
      );
    }

    if (loginBloc.error) {
      _logger.info("Found error");
      return Container(
        child: Padding(
          padding: const EdgeInsets.only(top: 6),
          child: LoginError(loginBloc.errorMessage),
        ),
      );
    }

    if (loginBloc.loggedIn) {
      _logger.info("Logged in");
      return Container(
        key: UniqueKey(),
        child: Padding(
          padding: const EdgeInsets.only(top: 12),
          child: Icon(
            Icons.check_circle,
            color: KadasterColors.okColor,
            size: 42,
          ),
        ),
      );
    }

    return SizedBox.shrink();
  }

  ///Sending the login request to the server if [_formKey] is valid
  void _sendLoginRequest(LoginBloc loginBloc, BuildContext context) async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      _logger.info("Sending $_username and $_password to server");
      loginBloc.loggingIn = true;

      bool connected = await InternetConnection.connected();
      if (!connected) {
        _logger.info("No internet connection");
        _delayedError(loginBloc, "Geen internet verbinding");
        return;
      }

      try {
        final response = await Dio(
          BaseOptions(connectTimeout: Constants.CONNECTION_TIME_OUT_MS),
        ).post(
          Constants.URL + "/employee/login",
          data: {
            "loginCode": _username,
            "password": _password,
          },
        );

        final user = User.fromJson(response.data);
        StorageManager.getInstance().getStore("user").insert(user);

        _delayedLogin(loginBloc, context);
      } catch (e, stacktrace) {
        _logger.severe(
            "Error logging in message=${ErrorMessageHttp.message(e)}",
            e,
            stacktrace);
        _onConnectionTimedOut(loginBloc, e);
      }
    }
  }

  ///On error when sending the login request
  void _onConnectionTimedOut(LoginBloc loginBloc, DioError e) {
    if (e.type == DioErrorType.CONNECT_TIMEOUT) {
      _logger.info("Unable to connect");
      _delayedError(loginBloc, "Geen verbinding kunnen maken");
    } else {
      _delayedError(loginBloc, "Verkeerde gegevens");
    }
  }

  ///Changing the widget after 500 milliseconds for the animation
  void _delayedLogin(LoginBloc loginBloc, BuildContext context) {
    Future.delayed(const Duration(milliseconds: 500)).then((value) {
      loginBloc.loggedIn = true;
      _delayedPageSwitch(loginBloc, context);
    });
  }

  ///Sending the user to the next page after 500 milliseconds
  void _delayedPageSwitch(LoginBloc loginBloc, BuildContext context) {
    Future.delayed(const Duration(milliseconds: 500)).then((value) {
      _logger.info("Going to AppointmentsPage");
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => AppointmentsPage()));
    });
  }

  ///Showing the widget with an error after 600 milliseconds for the animation
  void _delayedError(LoginBloc loginBloc, String message) {
    Future.delayed(const Duration(milliseconds: 600))
        .then((value) => loginBloc.sendErrorWithMessage(message));
  }
}

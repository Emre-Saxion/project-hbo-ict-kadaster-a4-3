import 'dart:math';

import 'package:client/bloc/map/map_drawing_bloc.dart';
import 'package:client/model/appointment/appointment.dart';
import 'package:client/model/offset_info.dart';
import 'package:flutter/material.dart';
import 'package:matrix_gesture_detector/matrix_gesture_detector.dart';
import 'package:provider/provider.dart';

import 'map_painter.dart';

class ZoomableWidget extends StatefulWidget {
  final Widget child;

  final Appointment appointment;

  const ZoomableWidget({Key key, this.child, this.appointment})
      : super(key: key);

  @override
  _ZoomableWidgetState createState() => _ZoomableWidgetState();
}

class _ZoomableWidgetState extends State<ZoomableWidget> {
  final GlobalKey _canvasKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    final MapDrawingBloc mapDrawingBloc = Provider.of<MapDrawingBloc>(context);

    final customPaint = CustomPaint(
      isComplex: true,
      key: _canvasKey,
      foregroundPainter:
          MapPainter(mapDrawingBloc.matrix4, mapDrawingBloc.offsetInfo),
      child: Transform(
        transform: mapDrawingBloc.matrix4,
        child: widget.child,
      ),
    );

    final gesture = mapDrawingBloc.drawing || mapDrawingBloc.erasing
        ? Listener(
            onPointerDown: (event) {
              _onPointerDown(event, mapDrawingBloc);
            },
            onPointerMove: (event) {
              _onPointerUpdate(event, mapDrawingBloc);
            },
            child: customPaint,
          )
        : mapDrawingBloc.lines
            ? GestureDetector(
                onTapDown: (event) {
                  _onPointerDown(event, mapDrawingBloc);
                },
                child: customPaint,
              )
            : customPaint;

    return MatrixGestureDetector(
      focalPointAlignment: Alignment.center,
      shouldScale: mapDrawingBloc.zooming,
      shouldTranslate: mapDrawingBloc.zooming,
      shouldRotate: false,
      onMatrixUpdate: (Matrix4 m, Matrix4 tm, Matrix4 sm, Matrix4 rm) {
        mapDrawingBloc.matrix4 = m;
      },
      child: gesture,
    );
  }

  ///On pointer down action
  void _onPointerDown(event, MapDrawingBloc mapDrawingBloc) {
    if (mapDrawingBloc.drawing || mapDrawingBloc.lines) {
      mapDrawingBloc.addOffset(_correctOffset(event, mapDrawingBloc));
    } else {
      _removeByOffset(_correctOffset(event, mapDrawingBloc), mapDrawingBloc);
    }
    _canvasKey.currentContext.findRenderObject().markNeedsPaint();
  }

  ///On pointer update action
  void _onPointerUpdate(event, MapDrawingBloc mapDrawingBloc) {
    if (mapDrawingBloc.drawing) {
      mapDrawingBloc.addOffset(_correctOffset(event, mapDrawingBloc));
    } else {
      _removeByOffset(_correctOffset(event, mapDrawingBloc), mapDrawingBloc);
    }
    _canvasKey.currentContext.findRenderObject().markNeedsPaint();
  }

  ///Removing the offsets, this is used when erasing
  void _removeByOffset(OffsetInfo offsetInfo, MapDrawingBloc mapDrawingBloc) {
    mapDrawingBloc.offsetInfo.removeWhere((element) {
      final deltaY = offsetInfo.offset.dy - element.offset.dy;
      final deltaX = offsetInfo.offset.dx - element.offset.dx;

      final distance = sqrt(deltaX * deltaX + deltaY * deltaY);

      return distance < mapDrawingBloc.range;
    });
  }

  ///Setting the offset to the correct value based on the matrix
  OffsetInfo _correctOffset(event, MapDrawingBloc mapDrawingBloc) {
    final Matrix4 matrix4 = mapDrawingBloc.matrix4;
    final double scaleX = matrix4[matrix4.index(0, 0)];
    final double scaleY = matrix4[matrix4.index(1, 1)];
    final Offset translate =
        MatrixGestureDetector.decomposeToValues(matrix4).translation;

    final Offset givenOffset = event.localPosition;

    final Offset correctOffset = Offset(
        (givenOffset.dx - translate.dx) / scaleX,
        (givenOffset.dy - translate.dy) / scaleY);


    return OffsetInfo(widget.appointment.id, correctOffset,
        mapDrawingBloc.color, mapDrawingBloc.range, mapDrawingBloc.lines);
  }
}

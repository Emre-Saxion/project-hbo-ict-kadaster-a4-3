import 'dart:ui';

import 'package:client/model/offset_info.dart';
import 'package:flutter/material.dart';

class MapPainter extends CustomPainter {
  ///The matrix given
  final Matrix4 _matrix;

  ///The offsets drawn
  final List<OffsetInfo> _offsets;

  MapPainter(this._matrix, this._offsets);

  ///The painter object
  final Paint painter = Paint()..style = PaintingStyle.fill;

  @override
  void paint(Canvas canvas, Size size) {

    //First we need to save the canvas
    canvas.save();
    //Then we transform the canvas according to the matrix given
    canvas.transform(_matrix.storage);
    //Then we draw the offsets

    for (int i = 0; i < _offsets.length; i++) {
      final OffsetInfo offsetInfo = _offsets[i];
      if (offsetInfo.line && i + 1 < _offsets.length && _offsets[i + 1].line) {
        final offsetInfoTwo = _offsets[i + 1];

        for (double t = 0; t < 1; t += 0.01) {
          final interpolation =
          Offset.lerp(offsetInfo.offset, offsetInfoTwo.offset, t);
          addOffsets(OffsetInfo(offsetInfo.id, interpolation,
              offsetInfo.color, offsetInfo.range, false));
          canvas.drawCircle(interpolation, offsetInfo.range,
              painter..color = offsetInfo.color);
        }
      } else {
        canvas.drawCircle(offsetInfo.offset, offsetInfo.range,
            painter..color = offsetInfo.color);
      }
    }

    //Then we restore the canvas which pops the save
    canvas.restore();
  }

  @override
  bool shouldRepaint(MapPainter oldDelegate) => oldDelegate._offsets.length != _offsets.length;

  void addOffsets(final interpolation) async {
    if (_offsets.contains(interpolation)) {
      return;
    }
    _offsets.add(interpolation);
  }
}

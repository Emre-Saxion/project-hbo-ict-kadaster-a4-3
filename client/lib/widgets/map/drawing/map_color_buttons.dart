import 'package:client/bloc/map/map_drawing_bloc.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';


class MapColorButtons extends StatelessWidget {
  final Logger _logger = Logger("MapColorButtons");

  @override
  Widget build(BuildContext context) {
    final MapDrawingBloc mapDrawingBloc = Provider.of<MapDrawingBloc>(context);

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        _circleColor(Colors.blue, mapDrawingBloc),
        _circleColor(Colors.green, mapDrawingBloc),
        _circleColor(Colors.red, mapDrawingBloc),
        _circleColor(Colors.yellow, mapDrawingBloc),
        _circleColor(Colors.orange, mapDrawingBloc),
        _circleColor(Colors.black, mapDrawingBloc),
      ],
    );
  }

  ///The circle color which will set the color of the paint which is based on [color]
  Widget _circleColor(Color color, MapDrawingBloc mapDrawingBloc) {
    return GestureDetector(
      onTap: () {
        _logger.info("Clicked color=$color");
        mapDrawingBloc.color = color;
      },
      child: Container(
        height: 40,
        width: 40,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: color,
          border: Border.all(
              color: mapDrawingBloc.color == color ? Colors.grey : color, width: 3),
        ),
      ),
    );
  }
}

import 'package:client/bloc/map/map_drawing_bloc.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MapRangeSlider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final MapDrawingBloc mapDrawingBloc = Provider.of<MapDrawingBloc>(context);

    return Padding(
      padding: const EdgeInsets.only(left: 12.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            "Dikte",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16,
            ),
          ),
          Expanded(
            child: Slider(
              activeColor: KadasterColors.secondaryColor,
              min: 1,
              max: 10,
              value: mapDrawingBloc.range,
              onChanged: (double value) {
                mapDrawingBloc.range = value;
              },
            ),
          ),
          SizedBox(
            height: 8,
          ),
        ],
      ),
    );
  }
}

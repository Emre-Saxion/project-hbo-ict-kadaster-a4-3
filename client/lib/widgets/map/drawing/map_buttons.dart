import 'package:client/bloc/map/map_drawing_bloc.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

class MapButtons extends StatelessWidget {
  final Logger _logger = Logger("MapButtons");

  @override
  Widget build(BuildContext context) {
    final MapDrawingBloc mapDrawingBloc = Provider.of<MapDrawingBloc>(context);

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        RawMaterialButton(
          onPressed: () {
            if (mapDrawingBloc.lines) {
              return;
            }
            _logger.info("Clicked lines");
            mapDrawingBloc.lines = true;
          },
          elevation: 4.0,
          fillColor: mapDrawingBloc.lines
              ? KadasterColors.secondaryColor
              : KadasterColors.WHITE_COLOR,
          child: Icon(
            Icons.edit,
            size: 24.0,
            color: mapDrawingBloc.lines
                ? KadasterColors.WHITE_COLOR
                : KadasterColors.secondaryColor,
          ),
          padding: EdgeInsets.all(15.0),
          shape: CircleBorder(),
        ),
        RawMaterialButton(
          onPressed: () {
            if (mapDrawingBloc.drawing) {
              return;
            }
            _logger.info("Clicked drawing");
            mapDrawingBloc.drawing = true;
          },
          elevation: 4.0,
          fillColor: mapDrawingBloc.drawing
              ? KadasterColors.secondaryColor
              : KadasterColors.WHITE_COLOR,
          child: Icon(
            Icons.brush,
            size: 24.0,
            color: mapDrawingBloc.drawing
                ? KadasterColors.WHITE_COLOR
                : KadasterColors.secondaryColor,
          ),
          padding: EdgeInsets.all(15.0),
          shape: CircleBorder(),
        ),
        RawMaterialButton(
          onPressed: () {
            if (mapDrawingBloc.erasing) {
              return;
            }
            _logger.info("Clicked erasing");
            mapDrawingBloc.erasing = true;
          },
          elevation: 4.0,
          fillColor: mapDrawingBloc.erasing
              ? KadasterColors.secondaryColor
              : KadasterColors.WHITE_COLOR,
          child: Icon(
            Icons.delete,
            size: 24.0,
            color: mapDrawingBloc.erasing
                ? KadasterColors.WHITE_COLOR
                : KadasterColors.secondaryColor,
          ),
          padding: EdgeInsets.all(15.0),
          shape: CircleBorder(),
        ),
        RawMaterialButton(
          onPressed: () {
            if (mapDrawingBloc.zooming) {
              return;
            }
            _logger.info("Clicked zooming");
            mapDrawingBloc.zooming = true;
          },
          elevation: 4.0,
          fillColor: mapDrawingBloc.zooming
              ? KadasterColors.secondaryColor
              : KadasterColors.WHITE_COLOR,
          child: Icon(
            Icons.zoom_in,
            size: 24.0,
            color: mapDrawingBloc.zooming
                ? KadasterColors.WHITE_COLOR
                : KadasterColors.secondaryColor,
          ),
          padding: EdgeInsets.all(15.0),
          shape: CircleBorder(),
        ),
      ],
    );
  }
}

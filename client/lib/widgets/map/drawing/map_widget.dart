import 'package:client/constants/kadaster_colors.dart';
import 'package:client/model/appointment/appointment.dart';
import 'package:client/widgets/preview/file_preview.dart';
import 'package:flutter/material.dart';

import 'zoomable_widget.dart';

class MapWidget extends StatefulWidget {
  final Appointment _appointment;

  MapWidget(this._appointment);

  @override
  _MapWidgetState createState() => _MapWidgetState();
}

class _MapWidgetState extends State<MapWidget>
    with AutomaticKeepAliveClientMixin {

  Future<Image> _pdfImage;


  @override
  void initState() {
    _pdfImage = FilePreview().openPDFAppointment(widget._appointment, 3);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _pdfImage,
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData) {
          return ZoomableWidget(
            appointment: widget._appointment,
            child: Container(
              width: double.infinity,
              height: double.infinity,
              child: snapshot.data,
            ),
          );
        }
        return Center(
          child: SizedBox(
            width: 36,
            height: 36,
            child: CircularProgressIndicator(
              valueColor:
                  AlwaysStoppedAnimation<Color>(KadasterColors.mainColor),
            ),
          ),
        );
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}

import 'package:client/bloc/map/map_input_bloc.dart';
import 'package:client/constants/constants.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:client/model/appointment/address.dart';
import 'package:client/model/appointment/appointment.dart';
import 'package:client/model/appointment/stakeholder.dart';
import 'package:client/util/screen_util.dart';
import 'package:client/widgets/appointments/appointments_icon_button.dart';
import 'package:client/widgets/map/input/map_stakeholder_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class MapStakeholders extends StatelessWidget {

  final Appointment _appointment;

  final GlobalKey<ScaffoldState> _scaffoldKey;

  MapStakeholders(this._appointment, this._scaffoldKey);

  @override
  Widget build(BuildContext context) {
    final MapInputBloc mapInputBloc = Provider.of<MapInputBloc>(context);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 12),
          child: Text(
            "Betrokkenen",
            textAlign: TextAlign.start,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26),
          ),
        ),
        SizedBox(
          height: 8,
        ),
        _appointment.stakeholders.length > 0
            ? ListView(
                primary: false,
                children: _stakeHolders(mapInputBloc, context),
                shrinkWrap: true,
              )
            : Center(
                child: Text(
                  "Geen betrokkenen gevonden.",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 18),
                ),
              ),
        Divider(
          thickness: 2.0,
        ),
      ],
    );
  }

  List<Widget> _stakeHolders(MapInputBloc mapInputBloc, BuildContext context) {
    final List<Widget> stakeholders = [];

    for (int i = 0; i < _appointment.stakeholders.length; i++) {
      stakeholders.add(
        MapStakeholdersWidget(_appointment.stakeholders[i], i, _scaffoldKey)
      );
      if (i != _appointment.stakeholders.length - 1) {
        stakeholders.add(
          Divider(),
        );
      }
    }
    return stakeholders;
  }
}

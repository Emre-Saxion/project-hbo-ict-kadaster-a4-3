import 'package:client/bloc/map/map_input_bloc.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:client/model/appointment/appointment.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MapReview extends StatelessWidget {
  final Appointment _appointment;

  MapReview(this._appointment);

  @override
  Widget build(BuildContext context) {
    final MapInputBloc mapInputBloc = Provider.of<MapInputBloc>(context);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 12),
          child: Text(
            "Toetsing",
            textAlign: TextAlign.start,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26),
          ),
        ),
        SizedBox(
          height: 8,
        ),
        Row(
          children: [
            SizedBox(
              width: 16,
            ),
            Expanded(
              child: Text(
                "Zijn de aanwijzingen van de verschenen belanghebbenden eensluidend?",
              ),
            ),
            Row(
              children: [
                Checkbox(
                  activeColor: KadasterColors.secondaryColor,
                  onChanged: (bool value) {
                    if (mapInputBloc.unanimous) {
                      return;
                    }
                    mapInputBloc.unanimous = true;
                  },
                  value: mapInputBloc.unanimous,
                ),
                Text(
                  "Ja"
                ),
              ],
            ),
            SizedBox(
              width: 16,
            ),
            Row(
              children: [
                Checkbox(
                  activeColor: KadasterColors.secondaryColor,
                  onChanged: (bool value) {
                    if (!mapInputBloc.unanimous) {
                      return;
                    }
                    mapInputBloc.unanimous = false;
                  },
                  value: !mapInputBloc.unanimous,
                ),
                Text(
                    "Nee"
                ),
              ],
            ),
            SizedBox(
              width: 16,
            ),
          ],
        ),
        SizedBox(
          height: 8,
        ),
        Row(
          children: [
            SizedBox(
              width: 16,
            ),
            Expanded(
              child: Text(
                "Zijn alle aanwijzingen werenigbaar met de akte(n)",
              ),
            ),
            Row(
              children: [
                Checkbox(
                  activeColor: KadasterColors.secondaryColor,
                  onChanged: (bool value) {
                    if (mapInputBloc.compatible) {
                      return;
                    }
                    mapInputBloc.compatible = true;
                  },
                  value: mapInputBloc.compatible,
                ),
                Text(
                    "Ja"
                ),
              ],
            ),
            SizedBox(
              width: 16,
            ),
            Row(
              children: [
                Checkbox(
                  activeColor: KadasterColors.secondaryColor,
                  onChanged: (bool value) {
                    if (!mapInputBloc.compatible) {
                      return;
                    }
                    mapInputBloc.compatible = false;
                  },
                  value: !mapInputBloc.compatible,
                ),
                Text(
                    "Nee"
                ),
              ],
            ),
            SizedBox(
              width: 16,
            ),
          ],
        ),
        Divider(
          thickness: 2.0,
        ),
      ],
    );
  }
}

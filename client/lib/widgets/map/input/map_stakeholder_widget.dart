import 'dart:ui';

import 'package:client/bloc/map/map_input_bloc.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:client/model/appointment/address.dart';
import 'package:client/model/appointment/authorizer.dart';
import 'package:client/model/appointment/stakeholder.dart';
import 'package:client/util/screen_util.dart';
import 'package:client/widgets/appointments/appointments_icon_button.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class MapStakeholdersWidget extends StatefulWidget {
  final Stakeholder _stakeholder;

  final int _index;

  final GlobalKey<ScaffoldState> _scaffoldKey;

  MapStakeholdersWidget(this._stakeholder, this._index, this._scaffoldKey);

  @override
  _MapStakeholdersWidgetState createState() => _MapStakeholdersWidgetState();
}

class _MapStakeholdersWidgetState extends State<MapStakeholdersWidget> {
  final Logger _logger = Logger("MapStakeHoldersWidget");

  final _formKey = GlobalKey<FormState>();

  bool _clickedEmpower = false;

  @override
  Widget build(BuildContext context) {
    final MapInputBloc mapInputBloc = Provider.of<MapInputBloc>(context);
    final ThemeData theme = Theme.of(context);

    return ListTile(
      title: Row(
        children: [
          Text(
            widget._stakeholder.name,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            " (${widget._stakeholder.title})",
            style: TextStyle(
              fontSize: 12,
              color: theme.textTheme.caption.color,
            ),
          ),
          Text(" - "),
          Expanded(
            child: Text(
              mapInputBloc.present[widget._index] &&
                      mapInputBloc.authorizers[widget._index] != null
                  ? "Gemachtigd"
                  : mapInputBloc.present[widget._index] ? "Aanwezig" : "Afwezig",
              style: TextStyle(
                color: mapInputBloc.present[widget._index]
                    ? KadasterColors.okColor
                    : KadasterColors.errorColor,
              ),
            ),
          ),
        ],
      ),
      onTap: () {
        _showDialog(mapInputBloc, widget._index);
      },
      trailing: IconButton(
        icon: Icon(Icons.keyboard_arrow_right),
        iconSize: 32,
        color: KadasterColors.secondaryColor,
        onPressed: () {
          _showDialog(mapInputBloc, widget._index);
        },
      ),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(_addressText(widget._stakeholder)),
          SizedBox(
            height: 12.0,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 65.0),
            child: Row(
              children: [
                AppointmentsIconButton(
                  Icon(
                    Icons.phone,
                    color: KadasterColors.secondaryColor,
                  ),
                  32,
                  "Bellen",
                  (context) {
                    _logger.info(
                        "Opening call for ${widget._stakeholder.phoneNumber}");
                    launch("tel://${widget._stakeholder.phoneNumber}");
                  },
                ),
                SizedBox(
                  width: 42,
                ),
                AppointmentsIconButton(
                  Icon(
                    Icons.email,
                    color: KadasterColors.secondaryColor,
                  ),
                  32,
                  "Email",
                  (context) {
                    _logger
                        .info("Opening maps for ${widget._stakeholder.email}");
                    launch("mailto:${widget._stakeholder.email}");
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  String _addressText(Stakeholder stakeholder) {
    final Address address = stakeholder.address;
    return "${address.street} ${address.number}, ${address.zipCode}, ${address.city}";
  }

  void _showDialog(MapInputBloc mapInputBloc, int i) {
    final ScreenUtil screenUtil = ScreenUtil(context);

    final format = DateFormat("dd-MM-yyyy");

    _clickedEmpower = false;

    var future = Future.delayed(const Duration(milliseconds: 400));

    var name = "";
    var relation = "";
    var birthDate = mapInputBloc.authorizers[i] == null
        ? null
        : mapInputBloc.authorizers[i].birthDate;
    var error = false;
    var errorText = "";

    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext stateContext,
              void Function(void Function()) setModalState) {
            return AnimatedContainer(
              height: screenUtil.getScreenHeightPercentage(_clickedEmpower
                  ? screenUtil.isLandScape()
                      ? 100
                      : 70 + MediaQuery.of(context).viewInsets.bottom
                  : screenUtil.isLandScape() ? 30 : 20),
              duration: const Duration(milliseconds: 500),
              curve: Curves.linear,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 12,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        if (!_clickedEmpower)
                          AppointmentsIconButton(
                            Icon(
                              Icons.check,
                              color: KadasterColors.secondaryColor,
                            ),
                            32,
                            "Aanwezig",
                            (context) {
                              if (widget._stakeholder.munacipility) {
                                Navigator.pop(context);
                                widget._scaffoldKey.currentState.showSnackBar(
                                  SnackBar(
                                    content: Text(
                                      "De betrokkene heeft geen datum",
                                      style: TextStyle(
                                        color: Colors.redAccent,
                                      ),
                                    ),
                                  ),
                                );
                                return;
                              }
                              if (mapInputBloc.present[i] && mapInputBloc.authorizers[i] == null) {
                                Navigator.pop(context);
                                widget._scaffoldKey.currentState.showSnackBar(
                                  SnackBar(
                                    content: Text(
                                      "De betrokkene is al aanwezig",
                                      style: TextStyle(
                                        color: Colors.redAccent,
                                      ),
                                    ),
                                  ),
                                );
                                return;
                              }
                              _logger.info(
                                  "Opening calender for ${widget._stakeholder.name}");
                              showDatePicker(
                                      context: context,
                                      helpText: "Verifieer geboortedatum",
                                      initialDate: DateTime.now(),
                                      firstDate: DateTime(1900),
                                      lastDate: DateTime.now())
                                  .then((date) {
                                _logger.info(date);
                                if (date == null) {
                                  return;
                                }
                                Navigator.pop(context);

                                final stakeholderDate =
                                    widget._stakeholder.birthDate;

                                if (date.day == stakeholderDate.day &&
                                    date.month == stakeholderDate.month &&
                                    date.year == stakeholderDate.year) {
                                  mapInputBloc.setPresent(i, true);
                                  mapInputBloc.setAuthorizer(i, null);
                                  widget._scaffoldKey.currentState.showSnackBar(
                                    SnackBar(
                                      content: Text(
                                        "De betrokkene is aanwezig gemeld",
                                        style: TextStyle(
                                          color: KadasterColors.okColor,
                                        ),
                                      ),
                                    ),
                                  );
                                } else {
                                  widget._scaffoldKey.currentState.showSnackBar(
                                    SnackBar(
                                      content: Text(
                                        "Verkeerde datum",
                                        style: TextStyle(
                                          color: Colors.redAccent,
                                        ),
                                      ),
                                    ),
                                  );
                                }
                              });
                            },
                          ),
                        if (!_clickedEmpower)
                          AppointmentsIconButton(
                            Icon(
                              Icons.close,
                              color: KadasterColors.secondaryColor,
                            ),
                            32,
                            "Afwezig",
                            (context) {
                              _logger.info("Setting present to false at $i");
                              if (!mapInputBloc.present[i]) {
                                Navigator.pop(context);
                                widget._scaffoldKey.currentState.showSnackBar(
                                  SnackBar(
                                    content: Text(
                                      "De betrokkene is al afwezig",
                                      style: TextStyle(
                                        color: Colors.redAccent,
                                      ),
                                    ),
                                  ),
                                );
                                return;
                              }
                              Navigator.pop(context);
                              mapInputBloc.setPresent(i, false);
                              mapInputBloc.setAuthorizer(i, null);
                              widget._scaffoldKey.currentState.showSnackBar(
                                SnackBar(
                                  content: Text(
                                    "De betrokkene is afwezig gemeld",
                                    style: TextStyle(
                                      color: KadasterColors.okColor,
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        if (!_clickedEmpower)
                          AppointmentsIconButton(
                            Icon(
                              Icons.edit,
                              color: KadasterColors.secondaryColor,
                            ),
                            32,
                            "Machtigen",
                            (context) {
                              _logger.info(
                                  "Opening empowerment form for ${widget._stakeholder.name}");
                              setModalState(() {
                                future = Future.delayed(
                                    const Duration(milliseconds: 400));
                                _clickedEmpower = !_clickedEmpower;
                              });
                            },
                          ),
                        if (_clickedEmpower)
                          FutureBuilder(
                            future: future,
                            builder: (BuildContext context,
                                AsyncSnapshot<dynamic> snapshot) {
                              if (snapshot.connectionState ==
                                  ConnectionState.done) {
                                return SizedBox(
                                  width: screenUtil.getScreenWidth() - 20,
                                  child: Form(
                                    key: _formKey,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.max,
                                      children: [
                                        Align(
                                          alignment: Alignment.centerRight,
                                          child: Row(
                                            mainAxisSize: MainAxisSize.max,
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 6),
                                                child: AppointmentsIconButton(
                                                  Icon(
                                                    Icons.arrow_downward,
                                                    color: KadasterColors
                                                        .secondaryColor,
                                                  ),
                                                  32,
                                                  "Terug",
                                                  (context) {
                                                    setModalState(() {
                                                      _clickedEmpower = false;
                                                    });
                                                  },
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height: 8.0,
                                        ),
                                        SizedBox(
                                          width: screenUtil
                                              .getScreenWidthPercentage(80),
                                          child: TextFormField(
                                            initialValue:
                                                mapInputBloc.authorizers[i] ==
                                                        null
                                                    ? ""
                                                    : mapInputBloc
                                                        .authorizers[i].name,
                                            validator: (value) {
                                              if (value.isEmpty) {
                                                return "Er moet nog een naam worden ingevuld.";
                                              }
                                              return null;
                                            },
                                            decoration: const InputDecoration(
                                                icon: Icon(Icons.person),
                                                hintText: 'Naam'),
                                            onSaved: (String value) {
                                              name = value;
                                            },
                                          ),
                                        ),
                                        SizedBox(
                                          height: 8.0,
                                        ),
                                        SizedBox(
                                          width: screenUtil
                                              .getScreenWidthPercentage(80),
                                          child: TextFormField(
                                            initialValue:
                                                mapInputBloc.authorizers[i] ==
                                                        null
                                                    ? ""
                                                    : mapInputBloc
                                                        .authorizers[i]
                                                        .acquaintance,
                                            decoration: const InputDecoration(
                                                icon:
                                                    Icon(Icons.business_center),
                                                hintText: 'Relatie'),
                                            validator: (value) {
                                              if (value.isEmpty) {
                                                return "Er moet nog een relatie worden ingevuld.";
                                              }
                                              return null;
                                            },
                                            onSaved: (String value) {
                                              relation = value;
                                            },
                                          ),
                                        ),
                                        SizedBox(
                                          height: 8.0,
                                        ),
                                        SizedBox(
                                          width: screenUtil
                                              .getScreenWidthPercentage(80),
                                          child: DateTimeField(
                                            initialValue: birthDate,
                                            format: format,
                                            decoration: InputDecoration(
                                              icon: Icon(Icons.cake),
                                              hintText: "Geboortedatum",
                                              errorText: error
                                                  ? errorText
                                                  : null,
                                            ),
                                            onShowPicker:
                                                (context, currentValue) {
                                              return showDatePicker(
                                                  context: context,
                                                  initialDate: currentValue ??
                                                      DateTime.now(),
                                                  firstDate: DateTime(1900),
                                                  lastDate: DateTime.now());
                                            },
                                            onChanged: (DateTime value) {
                                              birthDate = value;
                                            },
                                          ),
                                        ),
                                        SizedBox(
                                          height: 24.0,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            MaterialButton(
                                              color:
                                                  KadasterColors.secondaryColor,
                                              onPressed: () {
                                                if (_formKey.currentState
                                                    .validate()) {
                                                  if (birthDate == null) {
                                                    setModalState(() {
                                                      errorText = "Er moet nog een geboortedatum worden ingevuld.";
                                                      error = true;
                                                    });
                                                    return;
                                                  }
                                                  if (_calculateAge(birthDate) < 18) {
                                                    setModalState(() {
                                                      errorText = "De gemachtigde moet ouder zijn dan 18 jaar";
                                                      error = true;
                                                    });
                                                    return;
                                                  }
                                                  _formKey.currentState.save();
                                                  setModalState(() {
                                                    error = false;
                                                  });

                                                  Navigator.pop(context);
                                                  mapInputBloc.setAuthorizer(
                                                      i,
                                                      Authorizer(name, relation,
                                                          birthDate));
                                                  if (!mapInputBloc
                                                      .present[i]) {
                                                    widget._scaffoldKey
                                                        .currentState
                                                        .showSnackBar(
                                                      SnackBar(
                                                        content: Text(
                                                          "De betrokkene is aanwezig gemeld en gemachtigd",
                                                          style: TextStyle(
                                                            color:
                                                                KadasterColors
                                                                    .okColor,
                                                          ),
                                                        ),
                                                      ),
                                                    );
                                                  } else {
                                                    widget._scaffoldKey
                                                        .currentState
                                                        .showSnackBar(
                                                      SnackBar(
                                                        content: Text(
                                                          "De gemachtigde is geupdate",
                                                          style: TextStyle(
                                                            color:
                                                                KadasterColors
                                                                    .okColor,
                                                          ),
                                                        ),
                                                      ),
                                                    );
                                                  }
                                                  mapInputBloc.setPresent(
                                                      i, true);
                                                }
                                              },
                                              child: Text("Opslaan"),
                                              textColor:
                                                  KadasterColors.WHITE_COLOR,
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              }
                              return SizedBox.shrink();
                            },
                          ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }

  int _calculateAge(DateTime birthDate) {
    DateTime currentDate = DateTime.now();
    int age = currentDate.year - birthDate.year;
    int month1 = currentDate.month;
    int month2 = birthDate.month;
    if (month2 > month1) {
      age--;
    } else if (month1 == month2) {
      int day1 = currentDate.day;
      int day2 = birthDate.day;
      if (day2 > day1) {
        age--;
      }
    }
    return age;
  }
}

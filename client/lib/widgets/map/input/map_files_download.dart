import 'package:client/bloc/map/map_downloading_bloc.dart';
import 'package:client/constants/constants.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:client/file/file_manager.dart';
import 'package:client/model/appointment/appointment.dart';
import 'package:client/model/appointment/appointment_file.dart';
import 'package:client/model/user.dart';
import 'package:client/storage/storage_manager.dart';
import 'package:client/util/alert_util.dart';
import 'package:client/util/error_message_http.dart';
import 'package:client/util/internet_connection.dart';
import 'package:client/util/screen_util.dart';
import 'package:client/widgets/preview/file_preview.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:open_file/open_file.dart';
import 'package:provider/provider.dart';

class MapFilesDownload extends StatelessWidget {
  final Appointment _appointment;

  final AppointmentFile _appointmentFile;

  MapFilesDownload(this._appointment, this._appointmentFile);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: FutureBuilder(
        future: _fileExists(),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data) {
              return MapFilesDownloadExists(_appointment, _appointmentFile);
            }
            return MapFilesDownloadNotExists(_appointment, _appointmentFile);
          }
          return CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(KadasterColors.mainColor),
          );
        },
      ),
    );
  }

  Future<bool> _fileExists() {
    return FileManager.getInstance()
        .appointmentFileExists(_appointment, _appointmentFile);
  }
}

class MapFilesDownloadExists extends StatelessWidget {
  final Appointment _appointment;

  final AppointmentFile _appointmentFile;

  final Logger _logger = Logger("MapFilesDownloadExists");

  MapFilesDownloadExists(this._appointment, this._appointmentFile);

  @override
  Widget build(BuildContext context) {
    final ScreenUtil screenUtil = ScreenUtil(context);
    return Card(
      elevation: 3.0,
      child: InkWell(
        onTap: () async {
          final String path = await FileManager.getInstance()
              .appointmentFileLocation(_appointment, _appointmentFile);
          _logger.info("Opening file at $path");
          await OpenFile.open(path);
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                FutureBuilder(
                  future: FilePreview().previewWidgetAppointmentFile(
                      screenUtil, _appointment, _appointmentFile),
                  builder:
                      (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                    if (snapshot.hasData) {
                      return snapshot.data;
                    }
                    return Padding(
                      padding: EdgeInsets.only(
                        left: screenUtil.getScreenWidthPercentage(10),
                        right: screenUtil.getScreenWidthPercentage(10),
                      ),
                      child: Center(
                        child: SizedBox(
                          height: 36,
                          width: 36,
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(
                                KadasterColors.mainColor),
                          ),
                        ),
                      ),
                    );
                  },
                ),
                SizedBox(
                  height: 8,
                ),
                Container(
                  width: screenUtil.getScreenWidthPercentage(
                      screenUtil.isPortrait() ? 45 : 30),
                  child: Text(
                    "${_appointmentFile.fileName}.${_appointmentFile.fileExtension}",
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class MapFilesDownloadNotExists extends StatelessWidget {
  final Appointment _appointment;

  final AppointmentFile _appointmentFile;

  final Logger _logger = Logger("MapFilesDownloadNotExists");

  MapFilesDownloadNotExists(this._appointment, this._appointmentFile);

  @override
  Widget build(BuildContext context) {
    final MapDownloadingBloc mapDownloadingBloc =
        Provider.of<MapDownloadingBloc>(context);

    final ScreenUtil screenUtil = ScreenUtil(context);

    return Card(
      elevation: 3.0,
      child: InkWell(
        onTap: () async {
          if (mapDownloadingBloc.downloading) {
            return;
          }
          if (mapDownloadingBloc.downloaded) {
            final String path = await FileManager.getInstance()
                .appointmentFileLocation(_appointment, _appointmentFile);
            _logger.info("Opening file at $path");
            await OpenFile.open(path);
            return;
          }
          _logger.info("Downloading file $_appointmentFile");
          mapDownloadingBloc.downloading = true;

          InternetConnection.connected().then((value) {
            if (value) {
              _download(mapDownloadingBloc, context);
            } else {
              _logger.info("No internet connection to download files");
              AlertUtil.sendInfoAlert(context, "Internet",
                  "Geen internet verbinding", (context) {});
              mapDownloadingBloc.downloading = false;
            }
          });
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                mapDownloadingBloc.downloading
                    ? Padding(
                        padding: EdgeInsets.only(
                          left: screenUtil.getScreenWidthPercentage(10),
                          right: screenUtil.getScreenWidthPercentage(10),
                        ),
                        child: Center(
                          child: SizedBox(
                            height: 36,
                            width: 36,
                            child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  KadasterColors.mainColor),
                            ),
                          ),
                        ),
                      )
                    : mapDownloadingBloc.downloaded
                        ? FutureBuilder(
                            future: FilePreview().previewWidgetAppointmentFile(
                                screenUtil, _appointment, _appointmentFile),
                            builder: (BuildContext context,
                                AsyncSnapshot<dynamic> snapshot) {
                              if (snapshot.hasData) {
                                return snapshot.data;
                              }
                              return Padding(
                                padding: EdgeInsets.only(
                                  left: screenUtil.getScreenWidthPercentage(10),
                                  right:
                                      screenUtil.getScreenWidthPercentage(10),
                                ),
                                child: Center(
                                  child: SizedBox(
                                    height: 36,
                                    width: 36,
                                    child: CircularProgressIndicator(
                                      valueColor:
                                      AlwaysStoppedAnimation<Color>(KadasterColors.mainColor),
                                    ),
                                  ),
                                ),
                              );
                            },
                          )
                        : Padding(
                            padding: EdgeInsets.only(
                              left: screenUtil.getScreenWidthPercentage(10),
                              right: screenUtil.getScreenWidthPercentage(10),
                            ),
                            child: Icon(
                              Icons.file_download,
                              size: 48,
                              color: KadasterColors.secondaryColor,
                            ),
                          ),
                SizedBox(
                  height: mapDownloadingBloc.downloading ? 18 : 8,
                ),
                Container(
                  width: screenUtil.getScreenWidthPercentage(
                      screenUtil.isPortrait() ? 45 : 30),
                  child: Text(
                    "${_appointmentFile.fileName}.${_appointmentFile.fileExtension}",
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 18),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _download(
      MapDownloadingBloc mapDownloadingBloc, BuildContext context) async {
    final User user = StorageManager.getInstance().getStore("user").get();

    try {
      final response = await Dio(
              BaseOptions(connectTimeout: Constants.CONNECTION_TIME_OUT_MS))
          .get(
              Constants.URL +
                  "/download/file/${_appointmentFile.fileName}.${_appointmentFile.fileExtension}",
              options: Options(
                  headers: {"authorization": user.token, "id": _appointment.id},
                  responseType: ResponseType.bytes,
                  followRedirects: false));

      await FileManager.getInstance().saveFile(response.data, _appointment.id,
          "${_appointmentFile.fileName}.${_appointmentFile.fileExtension}");

      mapDownloadingBloc.downloaded = true;
    } catch (e, stacktrace) {
      _logger.severe(
          "Error downloading files message=${ErrorMessageHttp.message(e)}",
          e,
          stacktrace);
      AlertUtil.sendInfoAlert(context, "Mislukt",
          "Het downloaden van het bestand is mislukt", (context) {});
      mapDownloadingBloc.downloading = false;
    }
  }
}

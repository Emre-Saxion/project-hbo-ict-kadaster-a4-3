import 'package:client/constants/kadaster_colors.dart';
import 'package:client/model/appointment/address.dart';
import 'package:client/model/appointment/appointment.dart';
import 'package:client/model/appointment/deed.dart';
import 'package:client/widgets/appointments/appointments_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:url_launcher/url_launcher.dart';

class MapInfo extends StatelessWidget {

  final Logger _logger = Logger("MapInfo");

  final Appointment _appointment;

  MapInfo(this._appointment);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 12),
          child: Text(
            "Info",
            textAlign: TextAlign.start,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26),
          ),
        ),
        SizedBox(
          height: 8,
        ),
        ListTile(
          leading: Icon(
            Icons.navigation,
            color: KadasterColors.secondaryColor,
            size: 34,
          ),
          title: Text(_addressText()),
        ),
        ListTile(
          leading: Icon(
            Icons.folder,
            color: KadasterColors.secondaryColor,
            size: 34,
          ),
          title: Text(_deedText()),
        ),
        if (_appointment.type > 0)
          ListTile(
            leading: Icon(
              Icons.info,
              color: KadasterColors.secondaryColor,
              size: 34,
            ),
            title: Text(_correctType()),
          ),
        ListTile(
          leading: Icon(
            Icons.description,
            color: KadasterColors.secondaryColor,
            size: 34,
          ),
          title: Text(_appointment.description),
        ),
        SizedBox(
          height: 8.0,
        ),
        Divider(
          thickness: 2.0,
        ),
      ],
    );
  }

  String _addressText() {
    final Address address = _appointment.address;
    return "${address.street} ${address.number}, ${address.zipCode}, ${address.city}";
  }

  String _deedText() {
    final Deed deed = _appointment.deed;
    return "Deelnummer ${deed.deedNumber} - akte ${deed.partNumber}";
  }

  String _correctType() {
    if (_appointment.type == 0) {
      return "";
    }
    final List<String> types = [
      "Stuk grond (ver)kopen",
      "De erfgrens bepalen",
      "Conflict over grens",
      "Boom of heg naast erfgrens",
      "Grens zelf opmeten"
    ];
    return types[_appointment.type - 1];
  }
}

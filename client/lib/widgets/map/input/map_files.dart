import 'package:client/bloc/map/map_downloading_bloc.dart';
import 'package:client/model/appointment/appointment.dart';
import 'package:client/util/screen_util.dart';
import 'package:client/widgets/map/input/map_files_download.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

class MapFiles extends StatefulWidget {
  final Appointment _appointment;

  MapFiles(this._appointment);

  @override
  _MapFilesState createState() => _MapFilesState();
}

class _MapFilesState extends State<MapFiles> with AutomaticKeepAliveClientMixin {

  final Logger _logger = Logger("MapFilesState");

  @override
  Widget build(BuildContext context) {
    final ScreenUtil screenUtil = ScreenUtil(context);
    _logger.info("Showing MapFilesDownload for ${widget._appointment.files.length} files");
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 12,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 12),
          child: Text(
            "Akte en bijlagen",
            textAlign: TextAlign.start,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26),
          ),
        ),
        widget._appointment.files.length > 0
            ? Container(
          height: screenUtil.getScreenHeightPercentage(
              screenUtil.isPortrait() ? 32 : 50),
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: widget._appointment.files
                .map(
                  (e) => ChangeNotifierProvider(
                create: (BuildContext context) {
                  return MapDownloadingBloc();
                },
                child: MapFilesDownload(widget._appointment, e),
              ),
            )
                .toList(),
          ),
        )
            : Center(
          child: Text(
            "Geen bestanden gevonden.",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 18),
          ),
        ),
        Divider(
          thickness: 2.0,
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}



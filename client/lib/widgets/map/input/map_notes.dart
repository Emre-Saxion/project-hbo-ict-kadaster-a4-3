import 'package:client/bloc/map/map_input_bloc.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:client/util/screen_util.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

class MapNotes extends StatelessWidget {
  final Logger _logger = Logger("MapNotes");

  @override
  Widget build(BuildContext context) {
    final MapInputBloc mapInputBloc = Provider.of<MapInputBloc>(context);

    final ScreenUtil screenUtil = ScreenUtil(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 12),
          child: Text(
            "Notitie",
            textAlign: TextAlign.start,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26),
          ),
        ),
        GestureDetector(
          onTap: () {
            _openInputAlert(context, mapInputBloc);
          },
          child: Container(
            width: screenUtil.getScreenWidth(),
            height: screenUtil
                .getScreenHeightPercentage(screenUtil.isPortrait() ? 30 : 50),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                elevation: 3.0,
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: mapInputBloc.noteText.length == 0
                      ? Text(
                          "Notitie",
                          style: TextStyle(color: Colors.grey),
                        )
                      : Text(
                          mapInputBloc.noteText,
                        ),
                ),
              ),
            ),
          ),
        ),
        Divider(
          thickness: 2.0,
        )
      ],
    );
  }

  void _openInputAlert(BuildContext context, MapInputBloc mapInputBloc) {
    showGeneralDialog(
        context: context,
        transitionBuilder: (context, animation, secondAnimation, widget) {
          return Transform.scale(
            scale: animation.value,
            child: Opacity(
              opacity: animation.value,
              child: AlertDialog(
                key: Key("alert_dialog"),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
                title: null,
                content: Row(
                  children: [
                    Expanded(
                      child: Card(
                        elevation: 3.0,
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: TextField(
                            autofocus: true,
                            controller: mapInputBloc.noteEditingController,
                            maxLines: 10,
                            decoration:
                                InputDecoration.collapsed(hintText: "Notitie"),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                contentPadding: EdgeInsets.zero,
                buttonPadding: const EdgeInsets.only(right: 8),
                actions: <Widget>[
                  FlatButton(
                    child: Text(
                      "Opslaan",
                      style: TextStyle(color: KadasterColors.okColor),
                    ),
                    onPressed: () {
                      _logger.info("Setting note text to ${mapInputBloc.noteEditingController.text}");
                      Navigator.of(context).pop();
                      mapInputBloc.noteText =
                          mapInputBloc.noteEditingController.text;

                    },
                  ),
                ],
              ),
            ),
          );
        },
        transitionDuration: const Duration(milliseconds: 200),
        barrierDismissible: true,
        barrierColor: Colors.black54,
        barrierLabel: "",
        pageBuilder: (context, animation, secondAnimation) {
          return null;
        });
  }
}

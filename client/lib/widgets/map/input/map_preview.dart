import 'dart:io';
import 'dart:math';

import 'package:after_layout/after_layout.dart';
import 'package:client/bloc/map/map_drawing_bloc.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:client/file/file_manager.dart';
import 'package:client/model/appointment/appointment.dart';
import 'package:client/pages/map_drawing_page.dart';
import 'package:client/storage/store_offset.dart';
import 'package:client/util/screen_util.dart';
import 'package:client/widgets/preview/file_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart' as painting;
import 'package:provider/provider.dart';

class MapPreview extends StatefulWidget {
  final Appointment _appointment;

  MapPreview(this._appointment);

  @override
  _MapPreviewState createState() => _MapPreviewState();
}

class _MapPreviewState extends State<MapPreview>
    with AfterLayoutMixin<MapPreview> {
  Widget _preview;

  @override
  void afterFirstLayout(BuildContext context) {
    _correctMap();
  }

  @override
  Widget build(BuildContext context) {
    final ScreenUtil screenUtil = ScreenUtil(context);
    final MapDrawingBloc mapDrawingBloc =
        Provider.of<MapDrawingBloc>(context, listen: false);
    return GestureDetector(
      onTap: () async {
        mapDrawingBloc.offsetInfo =
            StoreOffsetManager.getInstance()
                .getOffsets(widget._appointment.id);
        await Navigator.push(context,
            MaterialPageRoute(builder: (BuildContext context) {
              return ChangeNotifierProvider.value(
                  value: mapDrawingBloc,
                  child: MapDrawingPage(widget._appointment));
            }));
        _correctMap();
      },
      behavior: HitTestBehavior.translucent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 12),
            child: Text(
              "Kaart",
              textAlign: TextAlign.start,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26),
            ),
          ),
          _preview == null
              ? Center(
                  child: SizedBox(
                    width: 36,
                    height: 36,
                    child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(KadasterColors.mainColor),
                    ),
                  ),
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: screenUtil.getScreenHeightPercentage(
                          screenUtil.isLandScape() ? 80 : 40),
                      width: screenUtil.getScreenWidthPercentage(50),
                      child: FittedBox(
                        fit: BoxFit.fill,
                        child: _preview,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 24.0),
                      child: IconButton(
                        iconSize: 34,
                        icon: Icon(
                          Icons.arrow_forward,
                          color: KadasterColors.secondaryColor,
                        ),
                        onPressed: () async {
                          mapDrawingBloc.offsetInfo =
                              StoreOffsetManager.getInstance()
                                  .getOffsets(widget._appointment.id);
                          await Navigator.push(context,
                              MaterialPageRoute(builder: (BuildContext context) {
                            return ChangeNotifierProvider.value(
                                value: mapDrawingBloc,
                                child: MapDrawingPage(widget._appointment));
                          }));
                          _correctMap();
                        },
                      ),
                    ),
                  ],
                ),
          Divider(
            thickness: 2.0,
          ),
        ],
      ),
    );
  }

  Future<void> _correctMap() async {
    final bool exists = await FileManager.getInstance()
        .appointmentMapFileExists(widget._appointment);

    if (exists) {
      imageCache.clear();
      painting.imageCache.clear();
      PaintingBinding.instance.imageCache.clear();
      painting.imageCache.clearLiveImages();
      PaintingBinding.instance.imageCache.clear();

      final path = await FileManager.getInstance()
          .appointmentMapFileLocation(widget._appointment);

      final file = File(path);

      setState(() {
        _preview = Image.file(
          file,
          key: ValueKey(Random().nextInt(2100000000)),
        );
      });
      return;
    }
    final map = await FilePreview().openPDFAppointment(widget._appointment, 2);
    setState(() {
      _preview = map;
    });
  }
//
//  Widget _mapLayout(
//      Widget child, ScreenUtil screenUtil, MapDrawingBloc mapDrawingBloc) {
//    return Row(
//      mainAxisAlignment: MainAxisAlignment.spaceBetween,
//      children: [
//        Container(
//          height: screenUtil
//              .getScreenHeightPercentage(screenUtil.isLandScape() ? 80 : 40),
//          width: screenUtil.getScreenWidthPercentage(50),
//          child: FittedBox(
//            fit: BoxFit.fill,
//            child: child,
//          ),
//        ),
//        Padding(
//          padding: const EdgeInsets.only(right: 24.0),
//          child: IconButton(
//            icon: Icon(
//              Icons.arrow_forward,
//              color: KadasterColors.secondaryColor,
//            ),
//            onPressed: () async {
//              mapDrawingBloc.offsetInfo = StoreOffsetManager.getInstance()
//                  .getOffsets(widget._appointment.id);
//              await Navigator.push(context,
//                  MaterialPageRoute(builder: (BuildContext context) {
//                return ChangeNotifierProvider.value(
//                    value: mapDrawingBloc,
//                    child: MapDrawingPage(widget._appointment));
//              }));
//              _correctMap(screenUtil);
//            },
//          ),
//        ),
//      ],
//    );
//  }
}

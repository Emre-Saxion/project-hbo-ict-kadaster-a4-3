
import 'package:client/bloc/map/map_input_bloc.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:client/util/screen_util.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

class MapMeasurements extends StatelessWidget {

  final Logger _logger = Logger("MapMeasurements");

  @override
  Widget build(BuildContext context) {
    final ScreenUtil screenUtil = ScreenUtil(context);
    final MapInputBloc mapInputBloc = Provider.of<MapInputBloc>(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 12),
          child: Text(
            "Grensomschrijvingen",
            textAlign: TextAlign.start,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26),
          ),
        ),
        GestureDetector(
          onTap: () {
            _openInputAlert(context, mapInputBloc);
          },
          child: Container(
            width: screenUtil.getScreenWidth(),
            height: screenUtil
                .getScreenHeightPercentage(screenUtil.isPortrait() ? 30 : 50),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                elevation: 3.0,
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: mapInputBloc.measurementText.length == 0
                      ? Text(
                    "Grensomschrijvingen",
                    style: TextStyle(color: Colors.grey),
                  )
                      : Text(
                    mapInputBloc.measurementText,
                  ),
                ),
              ),
            ),
          ),
        ),
        Divider(
          thickness: 2.0,
        ),
      ],
    );
  }


  void _openInputAlert(BuildContext context, MapInputBloc mapInputBloc) {
    showGeneralDialog(
        context: context,
        transitionBuilder: (context, animation, secondAnimation, widget) {
          return Transform.scale(
            scale: animation.value,
            child: Opacity(
              opacity: animation.value,
              child: AlertDialog(
                key: Key("alert_dialog"),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
                title: null,
                content: Row(
                  children: [
                    Expanded(
                      child: Card(
                        elevation: 3.0,
                        child: Padding(
                          padding: const EdgeInsets.all(12.0),
                          child: TextField(
                            autofocus: true,
                            controller: mapInputBloc.measurementEditingController,
                            maxLines: 10,
                            decoration:
                            InputDecoration.collapsed(hintText: "Grensomschrijvingen"),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                contentPadding: EdgeInsets.zero,
                buttonPadding: const EdgeInsets.only(right: 8),
                actions: <Widget>[
                  FlatButton(
                    child: Text(
                      "Opslaan",
                      style: TextStyle(color: KadasterColors.okColor),
                    ),
                    onPressed: () {
                      _logger.info("Setting note text to ${mapInputBloc.measurementEditingController.text}");
                      Navigator.of(context).pop();
                      mapInputBloc.measurementText =
                          mapInputBloc.measurementEditingController.text;
                    },
                  ),
                ],
              ),
            ),
          );
        },
        transitionDuration: const Duration(milliseconds: 200),
        barrierDismissible: true,
        barrierColor: Colors.black54,
        barrierLabel: "",
        pageBuilder: (context, animation, secondAnimation) {
          return null;
        });
  }
}
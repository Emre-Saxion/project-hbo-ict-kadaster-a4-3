import 'dart:io';

import 'package:client/bloc/map/map_input_bloc.dart';
import 'package:client/constants/kadaster_colors.dart';
import 'package:client/file/file_manager.dart';
import 'package:client/model/appointment/appointment.dart';
import 'package:client/model/appointment/appointment_file.dart';
import 'package:client/util/screen_util.dart';
import 'package:client/widgets/preview/file_preview.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:logging/logging.dart';
import 'package:open_file/open_file.dart';
import 'package:path/path.dart';
import 'package:provider/provider.dart';

class MapAddFiles extends StatelessWidget {
  final Appointment _appointment;

  final Logger _logger = Logger("MapAddFiles");

  MapAddFiles(this._appointment);

  @override
  Widget build(BuildContext context) {
    final ScreenUtil screenUtil = ScreenUtil(context);
    final MapInputBloc mapInputBloc = Provider.of<MapInputBloc>(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 12),
          child: Text(
            "Bestanden toevoegen",
            textAlign: TextAlign.start,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26),
          ),
        ),
        SizedBox(
          height: 12.0,
        ),
        Container(
          height: screenUtil
              .getScreenHeightPercentage(screenUtil.isPortrait() ? 20 : 35),
          child: ListView(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            children: [
              SizedBox(
                width: 12,
              ),
              for (AppointmentFile appointmentFile in mapInputBloc.addedFiles)
                Padding(
                  padding: const EdgeInsets.only(right: 12.0),
                  child:
                      _showAddedFile(screenUtil, appointmentFile, mapInputBloc),
                ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.only(right: 12.0),
                  child: FloatingActionButton(
                    onPressed: () async {
                      final String result = await showDialog<String>(
                          context: context,
                          builder: (BuildContext context) {
                            return SimpleDialog(
                              title: Text("Kies een optie"),
                              children: [
                                SimpleDialogOption(
                                  onPressed: () {
                                    Navigator.pop(context, "Foto");
                                  },
                                  child: Text("Foto"),
                                ),
                                SizedBox(
                                  height: 12.0,
                                ),
                                SimpleDialogOption(
                                  onPressed: () {
                                    Navigator.pop(context, "Bestand");
                                  },
                                  child: Text("Bestand"),
                                )
                              ],
                            );
                          });

                      if (result == "Foto") {
                        _openCamera(context, mapInputBloc);
                      } else if (result == "Bestand") {
                        _openFilePicker(context, mapInputBloc);
                      }
                    },
                    child: Icon(
                      Icons.add,
                      color: KadasterColors.WHITE_COLOR,
                      size: 32,
                    ),
                    backgroundColor: KadasterColors.secondaryColor,
                  ),
                ),
              ),
            ],
          ),
        ),
        Divider(
          thickness: 2.0,
        ),
      ],
    );
  }

  Widget _showAddedFile(ScreenUtil screenUtil, AppointmentFile appointmentFile,
      MapInputBloc mapInputBloc) {
    return Card(
      elevation: 3.0,
      child: InkWell(
        onTap: () async {
          final String path = await FileManager.getInstance()
              .appointmentFileLocation(_appointment, appointmentFile);
          await OpenFile.open(path);
        },
        child: FutureBuilder(
          future: FilePreview().previewWidgetAppointmentFile(
              screenUtil, _appointment, appointmentFile),
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              return Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: snapshot.data,
                  ),
                  Positioned(
                    top: 6.0,
                    right: 6.0,
                    child: GestureDetector(
                      onTap: () {
                        mapInputBloc.deleteAppointmentFile(appointmentFile);
                      },
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(12.0),
                        child: Cutout(
                          color: KadasterColors.errorColor,
                          child: Icon(
                            Icons.cancel,
                            color: KadasterColors.WHITE_COLOR,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              );
            }
            return Padding(
              padding: EdgeInsets.only(
                left: screenUtil.getScreenWidthPercentage(10),
                right: screenUtil.getScreenWidthPercentage(10),
              ),
              child: Center(
                child: SizedBox(
                  height: 36,
                  width: 36,
                  child: CircularProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation<Color>(KadasterColors.mainColor),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Future<void> _openCamera(
      BuildContext context, MapInputBloc mapInputBloc) async {
    _logger.info("Opening camera");
    final PickedFile image =
        await mapInputBloc.picker.getImage(source: ImageSource.camera);

    if (image != null) {
      _saveFile(File(image.path), mapInputBloc);
    } else {
      _logger.info("No photo taken");
    }
  }

  Future<void> _openFilePicker(
      BuildContext context, MapInputBloc mapInputBloc) async {
    _logger.info("Opening file picker");
    final File file = await FilePicker.getFile();

    if (file != null) {
      _saveFile(file, mapInputBloc);
    } else {
      _logger.info("No file picked");
    }
  }

  void _saveFile(File file, MapInputBloc mapInputBloc) async {
    final String fileName = basename(file.path);
    final List<String> split = fileName.split(".");
    await FileManager.getInstance().saveFile(File(file.path).readAsBytesSync(),
        _appointment.id, "${split[0]}.${split[1]}");
    mapInputBloc.addAppointmentFile(AppointmentFile(split[0], split[1]));
  }
}

class Cutout extends StatelessWidget {
  const Cutout({
    Key key,
    @required this.color,
    @required this.child,
  }) : super(key: key);

  final Color color;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      blendMode: BlendMode.color,
      shaderCallback: (bounds) =>
          LinearGradient(colors: [color], stops: [0.0]).createShader(bounds),
      child: child,
    );
  }
}

import 'dart:io';

import 'package:client/model/appointment/appointment.dart';
import 'package:client/model/appointment/appointment_file.dart';
import 'package:logging/logging.dart';
import 'package:path_provider/path_provider.dart';

///The class used to manage files on the device
class FileManager {
  final Logger _logger = Logger("FileManager");

  ///Saving a file with bytes [data] at [getTemporaryDirectory()]/[directory]/[fileName]
  ///and returning the path
  Future<String> saveFile(data, directory, fileName) async {
    final tempDir = await getTemporaryDirectory();
    await Directory(tempDir.path + "/$directory").create();
    _logger.info("Saving file $fileName at ${tempDir.path + "/$directory"}");

    final path = tempDir.path + "/$directory/$fileName";
    final file = File(path);
    final raf = file.openSync(mode: FileMode.write);
    raf.writeFromSync(data);
    raf.closeSync();
    return path;
  }

  ///Saving the pdf file containing the map by calling [saveFile]
  ///and setting the mapFile variable of [appointment] to that path
  Future<void> saveAppointmentMap(
      data, directory, fileName, Appointment appointment) async {
    final path = await saveFile(data, directory, fileName);
    appointment.mapFile = path;
  }

  ///Checking if an appointment file [appointmentFile] is already downloaded
  Future<bool> appointmentFileExists(
      Appointment appointment, AppointmentFile appointmentFile) async {
    final tempDir = await getTemporaryDirectory();
    final path = tempDir.path +
        "/${appointment.id}/${appointmentFile.fileName}.${appointmentFile.fileExtension}";

    return File(path).exists();
  }

  Future<bool> appointmentMapFileExists(Appointment appointment) async {
    final tempDir = await getTemporaryDirectory();
    final path = tempDir.path + "/${appointment.id}/map.png";
    return File(path).exists();
  }

  Future<bool> appointmentMapFileExistsById(int id) async {
    final tempDir = await getTemporaryDirectory();
    final path = tempDir.path + "/$id/map.png";
    return File(path).exists();
  }

  ///Returning the location of an appointment file [appointmentFile]
  Future<String> appointmentFileLocation(
      Appointment appointment, AppointmentFile appointmentFile) async {
    final tempDir = await getTemporaryDirectory();
    return tempDir.path +
        "/${appointment.id}/${appointmentFile.fileName}.${appointmentFile.fileExtension}";
  }

  Future<String> appointmentFileLocationById(
      int id, AppointmentFile appointmentFile) async {
    final tempDir = await getTemporaryDirectory();
    return tempDir.path +
        "/$id/${appointmentFile.fileName}.${appointmentFile.fileExtension}";
  }

  Future<String> appointmentMapFileLocation(Appointment appointment) async {
    final tempDir = await getTemporaryDirectory();
    return tempDir.path + "/${appointment.id}/map.png";
  }

  Future<String> appointmentMapFileLocationById(int id) async {
    final tempDir = await getTemporaryDirectory();
    return tempDir.path + "/$id/map.png";
  }

  ///Deleting all the files related to the appointment with an id of [appointmentId]
  Future<void> deleteAppointmentFiles(int appointmentId) async {
    final tempDir = await getTemporaryDirectory();
    final directory = Directory(tempDir.path + "/$appointmentId");
    final bool exists = await directory.exists();
    if (exists) {
      await directory.delete();
    }
    _logger.info("Deleting directory ${tempDir.path + "/$appointmentId"}");
  }

  ///Deleting a single appointment file
  Future<void> deleteAppointmentFile(
      Appointment appointment, AppointmentFile appointmentFile) async {
    final path = await appointmentFileLocation(appointment, appointmentFile);
    final file = File(path);
    final bool exists = await file.exists();
    if (exists) {
      await file.delete();
    }
    _logger.info("Deleted file $path");
  }

  static FileManager _instance;

  static FileManager getInstance() {
    if (_instance == null) {
      _instance = FileManager();
    }
    return _instance;
  }
}

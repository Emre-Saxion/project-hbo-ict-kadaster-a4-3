import 'package:flutter/material.dart';

///The class containing all the useful constants
class Constants {
  ///Max int value
  static const int MAX_INT = 2147483647;

  ///The amount of time in ms before the client stops trying to reach the server
  static const int CONNECTION_TIME_OUT_MS = 2000;

  ///The amount of time in ms before the client stops trying to reach the server but for downloading files
  static const int FILE_DOWNLOAD_CONNECTION_TIME_OUT_MS = 10000;

  ///The amount of time in ms before the client stops trying to reach the server but for uploading files
  static const int FILE_UPLOAD_CONNECTION_TIME_OUT_MS = 10000;

  ///If we're in debug
  static const bool DEBUG = true;

  ///The URL to connect to
  static const String URL = DEBUG ? "http://10.0.2.2:8080" : "https://productionapi.hinx.nl:8080/";

  static const _kFontFam = 'MyFlutterApp';

  static const _kFontPkg = null;

  static const IconData DIRECTION = IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}

import 'package:flutter/material.dart';

///The class containing all the colors used in the app
class KadasterColors {
  static const _MAIN_COLOR_CODES = {
    50: Color.fromRGBO(0, 136, 158, 0.1),
    100: Color.fromRGBO(0, 136, 158, 0.2),
    200: Color.fromRGBO(0, 136, 158, 0.3),
    300: Color.fromRGBO(0, 136, 158, 0.4),
    400: Color.fromRGBO(0, 136, 158, 0.5),
    500: Color.fromRGBO(0, 136, 158, 0.6),
    600: Color.fromRGBO(0, 136, 158, 0.7),
    700: Color.fromRGBO(0, 136, 158, 0.8),
    800: Color.fromRGBO(0, 136, 158, 0.9),
    900: Color.fromRGBO(0, 136, 158, 1),
  };

  static final mainColor = MaterialColor(0xFF00889E, _MAIN_COLOR_CODES);

  static const _SECONDARY_COLOR_CODES = {
    50: Color.fromRGBO(0, 56, 125, 0.1),
    100: Color.fromRGBO(0, 56, 125, 0.2),
    200: Color.fromRGBO(0, 56, 125, 0.3),
    300: Color.fromRGBO(0, 56, 125, 0.4),
    400: Color.fromRGBO(0, 56, 125, 0.5),
    500: Color.fromRGBO(0, 56, 125, 0.6),
    600: Color.fromRGBO(0, 56, 125, 0.7),
    700: Color.fromRGBO(0, 56, 125, 0.8),
    800: Color.fromRGBO(0, 56, 125, 0.9),
    900: Color.fromRGBO(0, 56, 125, 1),
  };

  static final secondaryColor =
      MaterialColor(0xFF00387D, _SECONDARY_COLOR_CODES);

  static const _OK_COLOR_CODES = {
    50: Color.fromRGBO(112, 164, 38, 0.1),
    100: Color.fromRGBO(112, 164, 38, 0.2),
    200: Color.fromRGBO(112, 164, 38, 0.3),
    300: Color.fromRGBO(112, 164, 38, 0.4),
    400: Color.fromRGBO(112, 164, 38, 0.5),
    500: Color.fromRGBO(112, 164, 38, 0.6),
    600: Color.fromRGBO(112, 164, 38, 0.7),
    700: Color.fromRGBO(112, 164, 38, 0.8),
    800: Color.fromRGBO(112, 164, 38, 0.9),
    900: Color.fromRGBO(112, 164, 38, 1),
  };

  static final okColor = MaterialColor(0xFF70A426, _OK_COLOR_CODES);

  static const WHITE_COLOR = Colors.white;

  static const _ERROR_COLOR_CODES = {
    50: Color.fromRGBO(213, 43, 30, 0.1),
    100: Color.fromRGBO(213, 43, 30, 0.2),
    200: Color.fromRGBO(213, 43, 30, 0.3),
    300: Color.fromRGBO(213, 43, 30, 0.4),
    400: Color.fromRGBO(213, 43, 30, 0.5),
    500: Color.fromRGBO(213, 43, 30, 0.6),
    600: Color.fromRGBO(213, 43, 30, 0.7),
    700: Color.fromRGBO(213, 43, 30, 0.8),
    800: Color.fromRGBO(213, 43, 30, 0.9),
    900: Color.fromRGBO(213, 43, 30, 1),
  };

  static final errorColor = MaterialColor(0xFFD52B1E, _ERROR_COLOR_CODES);

  static const SCAFFOLD_BACKGROUND_COLOR = Colors.white;

  static final borderColor = Colors.grey[300];

  static const AVAILABLE_COLOR = MaterialColor(0xFF70A426, _OK_COLOR_CODES);

  static final unavailableColor = Colors.blue[900];

  static const _HOLIDAY_COLOR_CODES = {
    50: Color.fromRGBO(217, 201, 0, 0.1),
    100: Color.fromRGBO(217, 201, 0, 0.2),
    200: Color.fromRGBO(217, 201, 0, 0.3),
    300: Color.fromRGBO(217, 201, 0, 0.4),
    400: Color.fromRGBO(217, 201, 0, 0.5),
    500: Color.fromRGBO(217, 201, 0, 0.6),
    600: Color.fromRGBO(217, 201, 0, 0.7),
    700: Color.fromRGBO(217, 201, 0, 0.8),
    800: Color.fromRGBO(217, 201, 0, 0.9),
    900: Color.fromRGBO(217, 201, 0, 1),
  };

  static const holidayColor = MaterialColor(0xFFD9C900, _HOLIDAY_COLOR_CODES);

  static const BLACK_COLOR = Colors.black54;
}

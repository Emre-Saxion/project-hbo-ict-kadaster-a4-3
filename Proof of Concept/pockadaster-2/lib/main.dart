import 'package:animations/animations.dart';
import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SharedPreferences prefs = await SharedPreferences.getInstance();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        accentColor: Colors.blueAccent,
        scaffoldBackgroundColor: Colors.grey[200],
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.blueAccent),
          backgroundColor: Colors.white,
          title: Text(
            "Afspraken",
            style: TextStyle(color: Colors.black),
          ),
        ),
        drawer: Theme(
          data: Theme.of(context).copyWith(
            canvasColor: Colors.grey[200],
          ),
          child: Drawer(
            child: ListView(
              padding: EdgeInsets.zero,
              children: [
                Container(
                  child: Padding(
                    padding: EdgeInsets.only(top: 20, bottom: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image(
                          image: AssetImage("images/kadaster.png"),
                        ),
                        Text(
                          "Werknemer naam",
                          style: TextStyle(color: Colors.white, fontSize: 24),
                        ),
                      ],
                    ),
                  ),
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Colors.blueAccent,
                        Color(0xFF2962FF),
                        Colors.blueAccent,
                      ],
                      stops: [0.0, 0.8, 1],
                    ),
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                ListTile(
                  leading: Icon(Icons.history),
                  title: Text("Geschiedenis"),
                  onTap: () {},
                ),
                ListTile(
                  leading: Icon(Icons.person),
                  title: Text("Log off"),
                  onTap: () {},
                ),
              ],
            ),
          ),
        ),
        body: ListView(
          children: [
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey[300]),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: ExpansionTileCard(
                  elevation: 0.0,
                  finalPadding: EdgeInsets.zero,
                  baseColor: Theme.of(context).cardColor,
                  title: Text("LDN03H7497",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                  subtitle: Padding(
                      padding: const EdgeInsets.only(top: 4),
                      child:
                          Text("Straattest 50, 7000TT, Almelo \n10:00-12:00")),
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12.0),
                      child: OpenContainer(
                        closedElevation: 0.0,
                        closedColor: Theme.of(context).cardColor,
                        openColor: Theme.of(context).cardColor,
                        transitionDuration: Duration(milliseconds: 500),
                        transitionType: ContainerTransitionType.fadeThrough,
                        openBuilder:
                            (BuildContext context, void Function() action) {
                          return Scaffold(
                            appBar: AppBar(
                              iconTheme:
                                  IconThemeData(color: Colors.blueAccent),
                              backgroundColor: Colors.white,
                              title: Text(
                                "Afspraken",
                                style: TextStyle(color: Colors.black),
                              ),
                            ),
                            body: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Container(
                                color: Colors.red,
                              ),
                            ),
                          );
                        },
                        closedBuilder:
                            (BuildContext context, void Function() action) {
                          return Column(
                            children: [
                              Container(
                                height: MediaQuery.of(context).orientation ==
                                        Orientation.portrait
                                    ? MediaQuery.of(context).size.height * 0.3
                                    : MediaQuery.of(context).size.height * 0.4,
                                color: Colors.blueAccent,
                              ),
                            ],
                          );
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 50, right: 50, top: 12, bottom: 12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              IconButton(
                                icon: Icon(
                                  Icons.phone,
                                  color: Color(0xFF3758FF),
                                  size: 36,
                                ),
                                onPressed: () {},
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                "Call",
                                style: TextStyle(color: Colors.black),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              IconButton(
                                icon: Icon(
                                  Icons.message,
                                  color: Color(0xFF3758FF),
                                  size: 36,
                                ),
                                onPressed: () {},
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                "Message",
                                style: TextStyle(color: Colors.black),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              IconButton(
                                icon: Icon(
                                  Icons.navigation,
                                  color: Color(0xFF3758FF),
                                  size: 36,
                                ),
                                onPressed: () {},
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                "Route",
                                style: TextStyle(color: Colors.black),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey[300]),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: ExpansionTileCard(
                  elevation: 0.0,
                  finalPadding: EdgeInsets.zero,
                  baseColor: Theme.of(context).cardColor,
                  title: Text("LDN03H7497",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                  subtitle: Padding(
                      padding: const EdgeInsets.only(top: 4),
                      child:
                          Text("Straattest 50, 7000TT, Almelo \n10:00-12:00")),
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12.0),
                      child: OpenContainer(
                        closedElevation: 0.0,
                        closedColor: Theme.of(context).cardColor,
                        openColor: Theme.of(context).cardColor,
                        transitionDuration: Duration(milliseconds: 500),
                        transitionType: ContainerTransitionType.fadeThrough,
                        openBuilder:
                            (BuildContext context, void Function() action) {
                          return Scaffold(
                            appBar: AppBar(
                              iconTheme:
                                  IconThemeData(color: Colors.blueAccent),
                              backgroundColor: Colors.white,
                              title: Text(
                                "Afspraken",
                                style: TextStyle(color: Colors.black),
                              ),
                            ),
                            body: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Container(
                                color: Colors.red,
                              ),
                            ),
                          );
                        },
                        closedBuilder:
                            (BuildContext context, void Function() action) {
                          return Column(
                            children: [
                              Container(
                                height: MediaQuery.of(context).orientation ==
                                        Orientation.portrait
                                    ? MediaQuery.of(context).size.height * 0.3
                                    : MediaQuery.of(context).size.height * 0.4,
                                color: Colors.blueAccent,
                              ),
                            ],
                          );
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 50, right: 50, top: 12, bottom: 12),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Column(
                              children: [
                                IconButton(
                                  onPressed: () {},
                                  icon: Icon(
                                    Icons.phone,
                                    color: Color(0xFF3758FF),
                                    size: 36,
                                  ),
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  "Call",
                                  style: TextStyle(color: Colors.black),
                                ),
                              ],
                            ),
                          ),
                          InkWell(
                            onTap: () {},
                            child: Column(
                              children: [
                                IconButton(
                                  icon: Icon(
                                    Icons.message,
                                    color: Color(0xFF3758FF),
                                    size: 36,
                                  ),
                                  onPressed: () {},
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  "Message",
                                  style: TextStyle(color: Colors.black),
                                ),
                              ],
                            ),
                          ),
                          InkWell(
                            onTap: () {},
                            child: Column(
                              children: [
                                IconButton(
                                  icon: Icon(
                                    Icons.navigation,
                                    color: Color(0xFF3758FF),
                                    size: 36,
                                  ),
                                  onPressed: () {},
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  "Route",
                                  style: TextStyle(color: Colors.black),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 12,
            ),
          ],
        ));
  }
}

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF3758FF),
        title: Text("Afspraken"),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            Container(
              child: Padding(
                padding: EdgeInsets.only(top: 20, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image(
                      image: AssetImage("images/kadaster.png"),
                    ),
                    Text(
                      "Werknemer naam",
                      style: TextStyle(color: Colors.white, fontSize: 24),
                    ),
                  ],
                ),
              ),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xFF3758FF),
                    Color(0xFF1C2C80),
                    Color(0xFF3758FF),
                  ],
                  stops: [0.0, 0.8, 1],
                ),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text("Log off"),
              onTap: () {},
            ),
          ],
        ),
      ),
      body: ListView(
        children: [
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
              side: BorderSide(
                color: Color(0xFFCFCFCF),
              ),
            ),
            child: Column(
              children: [
                Padding(
                  padding:
                      EdgeInsets.only(left: 40, top: 20, right: 20, bottom: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "LDN03H7497",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.5,
                            child: Text(
                                "Straattest 50, 7000TT, Almelo 10:00-12:00"),
                          ),
                          Icon(
                            Icons.navigate_next,
                            size: 38,
                            color: Color(0xFF787878),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 12,
                ),
                Container(
                  height: 1,
                  color: Color(0xFFCFCFCF),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 50, right: 50, top: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Icon(
                            Icons.phone,
                            color: Color(0xFF3758FF),
                            size: 36,
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            "Call",
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Icon(
                            Icons.message,
                            color: Color(0xFF3758FF),
                            size: 36,
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            "Message",
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Icon(
                            Icons.navigation,
                            color: Color(0xFF3758FF),
                            size: 36,
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            "Route",
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
              side: BorderSide(
                color: Color(0xFFCFCFCF),
              ),
            ),
            child: Column(
              children: [
                Padding(
                  padding:
                  EdgeInsets.only(left: 40, top: 20, right: 20, bottom: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "LDN03H7497",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.5,
                            child: Text(
                                "Straattest 50, 7000TT, Almelo 10:00-12:00"),
                          ),
                          Icon(
                            Icons.navigate_next,
                            size: 38,
                            color: Color(0xFF787878),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 12,
                ),
                Container(
                  height: 1,
                  color: Color(0xFFCFCFCF),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 50, right: 50, top: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Icon(
                            Icons.phone,
                            color: Color(0xFF3758FF),
                            size: 36,
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            "Call",
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Icon(
                            Icons.message,
                            color: Color(0xFF3758FF),
                            size: 36,
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            "Message",
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Icon(
                            Icons.navigation,
                            color: Color(0xFF3758FF),
                            size: 36,
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            "Route",
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
              side: BorderSide(
                color: Color(0xFFCFCFCF),
              ),
            ),
            child: Column(
              children: [
                Padding(
                  padding:
                  EdgeInsets.only(left: 40, top: 20, right: 20, bottom: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "LDN03H7497",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.5,
                            child: Text(
                                "Straattest 50, 7000TT, Almelo 10:00-12:00"),
                          ),
                          Icon(
                            Icons.navigate_next,
                            size: 38,
                            color: Color(0xFF787878),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 12,
                ),
                Container(
                  height: 1,
                  color: Color(0xFFCFCFCF),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 50, right: 50, top: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Icon(
                            Icons.phone,
                            color: Color(0xFF3758FF),
                            size: 36,
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            "Call",
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Icon(
                            Icons.message,
                            color: Color(0xFF3758FF),
                            size: 36,
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            "Message",
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Icon(
                            Icons.navigation,
                            color: Color(0xFF3758FF),
                            size: 36,
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            "Route",
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
              side: BorderSide(
                color: Color(0xFFCFCFCF),
              ),
            ),
            child: Column(
              children: [
                Padding(
                  padding:
                  EdgeInsets.only(left: 40, top: 20, right: 20, bottom: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "LDN03H7497",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 22,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.5,
                            child: Text(
                                "Straattest 50, 7000TT, Almelo 10:00-12:00"),
                          ),
                          Icon(
                            Icons.navigate_next,
                            size: 38,
                            color: Color(0xFF787878),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 12,
                ),
                Container(
                  height: 1,
                  color: Color(0xFFCFCFCF),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 50, right: 50, top: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Icon(
                            Icons.phone,
                            color: Color(0xFF3758FF),
                            size: 36,
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            "Call",
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Icon(
                            Icons.message,
                            color: Color(0xFF3758FF),
                            size: 36,
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            "Message",
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Icon(
                            Icons.navigation,
                            color: Color(0xFF3758FF),
                            size: 36,
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            "Route",
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

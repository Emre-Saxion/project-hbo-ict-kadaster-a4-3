import React, { Component } from 'react';
import { TextField, withStyles, createStyles } from '@material-ui/core';

const useStyles = createStyles((theme) => ({
  textfield: {
    marginLeft: 20,
    marginBottom: 20,
    width: '87%',
    maxHeight: '19vh',
  },
}));

/**
 * Component to render the Appointment Note
 */
class AppointmentNote extends Component {
  /**
   * Appointment note render
   * @return {JSX} main conent
   */
  render() {
    return (
      <TextField
        required
        variant="outlined"
        value={this.props.text}
        className={this.props.classes.textfield}
        multiline
        disabled
        helperText="Opdracht beschrijving"
      />
    );
  }
}

export default withStyles(useStyles)(AppointmentNote);

/* eslint-disable max-len */
import React, { Component } from 'react';
import { TextField, withStyles, createStyles } from '@material-ui/core';

const useStyles = createStyles((theme) => ({
  customerName: {
    width: '100%',
    marginBottom: 10,
  },
  streetName: {
    width: '100%',
    marginBottom: 10,
  },
  zipCode: {
    width: '30%',
    paddingRight: 10,
  },
  city: {
    width: '70%',
  },
  mainDiv: {
    padding: 20,
  },
}));


/**
 * Main component for the loading of the owners of a appointment
 */
class AppointmentOwner extends Component {
  /**
   * component render
   * @return {JSX} the content
   */
  render() {
    const {classes} = this.props;

    return (
      this.props.stakeholder && <div className={classes.mainDiv}>
        <TextField id="outlined-basic" variant="outlined" value={this.props.stakeholder.name} disabled className={classes.customerName}/>
        <TextField id="outlined-basic" variant="outlined" value={`${this.props.stakeholder.address.street} ${this.props.stakeholder.address.number}`} disabled className={classes.streetName}/>
        <TextField id="outlined-basic" variant="outlined" value={this.props.stakeholder.address.zipCode} disabled className={classes.zipCode}/>
        <TextField id="outlined-basic" variant="outlined" value={this.props.stakeholder.address.city} disabled className={classes.city}/>
      </div>
    );
  }
}

export default withStyles(useStyles)(AppointmentOwner);

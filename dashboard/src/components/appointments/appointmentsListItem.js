import React, { Component } from 'react';
import { ListItem, withStyles, createStyles, ListItemText, Grid } from '@material-ui/core';

const useStyles = createStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  dateItemText: {
    textAlign: 'right',
  },
  listItem: {
    '&:hover': {
      borderLeft: 'solid 5px #f00',
    },
  },
}));

/**
 * Component to render the list item
 */
class AppointmentsListItem extends Component {

  /**
   * Constructor
   * @param {Object} props the props
   */
  constructor(props) {
    super(props);

    this.dateRender = this.dateRender.bind(this);
  }

  /**
   * Make the date a readable format
   * @param {String} appointmentDate appointment date
   * @return {String} date string
   */
  dateRender(appointmentDate) {
    const date = new Date(appointmentDate);
    return date.getDate() + '-' + (('0' + (date.getMonth() + 1)).slice(-2)) + ' ' + date.getHours() + ':' + date.getMinutes();
  }

  /**
   * Main render
   * @return {JSX} main render
   */
  render() {

    const {classes} = this.props;

    // eslint-disable-next-line no-console
    console.log(this.props.appointment.id);


    return (
      <ListItem onClick={() => this.props.selectedAppointment(this.props.appointment.id)} className={classes.listItem} key={this.props.appointment.id}>
        <Grid container>
          <Grid item xs={8}>
            {this.props.appointment && <ListItemText primary={this.props.appointment.address.city} secondary={this.props.appointment.parcel} />}
          </Grid>
          <Grid item xs={4}>
            {this.props.appointment && <ListItemText className={classes.dateItemText} primary={this.dateRender(this.props.appointment.appointmentDate)} />}
          </Grid>
        </Grid>
      </ListItem>
    );
  }
}

export default withStyles(useStyles)(AppointmentsListItem);

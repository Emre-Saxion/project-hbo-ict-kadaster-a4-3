import React, { Component } from 'react';
import { withStyles, createStyles, Dialog, DialogTitle, DialogContentText, DialogContent, DialogActions, Button, FormControl, InputLabel, Select, Typography } from '@material-ui/core';

import Api from '../../lib/api';
import PaperComponent from './PaperComponent';

const Store = window.require('electron-store');
const store = new Store();

const useStyles = createStyles((theme) => ({
  appointmentListHeader: {
    fontSize: 13,
  },
  formControl: {
    width: '50%',
  },
  mainTitle: {
    paddingBottom: 5,
  },
  subTitle: {
    marginTop: -14,
    marginBottom: 15,
  },
  content: {
    width: 600,
  },
  selectBox: {
  },
}));

/**
 * AppintmentsPlan for planning in the appointment
 */
class AppointmentsPlan extends Component {

  /**
   * Constructor
   * @param {Object} props the props
   */
  constructor(props) {
    super(props);

    this.state = {
      selectedAppointment: undefined,
      employees: [],
    };

    this.callback = this.callback.bind(this);
    this.errorCallback = this.errorCallback.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.callbackSave = this.callbackSave.bind(this);
    this.callbackSaveError = this.callbackSaveError.bind(this);
  }

  /**
   * When the component first loads
   */
  componentDidMount() {
    Api.getEmployees(store.get('userData'), this.callback, this.errorCallback);
  }

  /**
   * Callback when call succeseds
   * @param {Object} data api data
   */
  callback(data) {
    console.log(data);
    this.setState({ employees: data });
  }

  /**
   * Callback when data api fails
   * @param {Object} data Error data
   */
  errorCallback(data) {

  }

  /**
   * Handle change of the checkbox
   * @param {Object} e the event
   */
  handleChange(e) {
    this.setState({selectedEmployee: e.target.value});
  }

  /**
   * Save the employee to the appointment
   */
  handleSave() {
    Api.saveAppointmentEmployee(this.state.selectedEmployee, this.callbackSave, this.callbackSaveError);
  }

  /**
   * The callback when the save is a success
   * @param {*} data data api
   */
  callbackSave(data) {
    console.log(data);
  }

  /**
   * the callback when the save fails
   * @param {*} data the data
   */
  callbackSaveError(data) {
    // eslint-disable-next-line no-console
    console.log(data);
  }
  /**
   * Main render
   * @return {JSX} main content
   */
  render() {
    const {classes} = this.props;
    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.handleClose}
        PaperComponent={PaperComponent}
        aria-labelledby="draggable-dialog-title"
      >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title" className={classes.mainTitle}>
          Afspraak inplannen
        </DialogTitle>
        <DialogContent className={classes.content}>
          <DialogContentText>
            <Typography className={classes.subTitle}>
              #{this.props.selectedAppointment.parcel}
            </Typography>

            <FormControl variant="outlined" className={classes.formControl}>
              <InputLabel htmlFor="outlined-age-native-simple">Age</InputLabel>
              <Select
                native
                value={this.state.selectedEmployee}
                onChange={this.handleChange}
                label="Age"
                inputProps={{
                  name: 'age',
                  id: 'outlined-age-native-simple',
                }}
                className={classes.selectBox}
              >
                {this.state.employees.map(function(employee) {
                  return (
                    <option value={employee.loginCode} key={employee.loginCode}>{employee.firstName} {employee.lastName}</option>
                  );
                })}
              </Select>
            </FormControl>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={this.props.handleClose} color="primary" >
            Cancel
          </Button>
          <Button onClick={this.handleSave} color="primary">
            Opslaan
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(useStyles)(AppointmentsPlan);

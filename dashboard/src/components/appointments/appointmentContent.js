/* eslint-disable max-len */
/* eslint-disable no-console */
import React, { Component } from 'react';
import { Paper, Grid } from '@material-ui/core';
import AppointmentOwner from './appointmentOwner';
import AppointmentActions from './appointmentActions';
import AppointmentMap from './appointmentMap';
import AppointmentNote from './appointmentNote';
import Api from '../../lib/api';

const Store = window.require('electron-store');
const store = new Store();

/**
 * Create the appointmentContent box on the appointment page
 */
class AppointmentContent extends Component {

  /**
   * Constructor
   * @param {Object} props the component props
   */
  constructor(props) {
    super(props);

    this.state = {
      selectedAppointment: 0,
      pdf: '',
    };

    this.getMapImage = this.getMapImage.bind(this);
    this.callback = this.callback.bind(this);
    this.errorCallback = this.errorCallback.bind(this);
  }

  /**
   * Update the props
   * @param {Object} newProps props
   * @param {Object} prevProps props
   * @return {{selectedAppointment: (Appointments.selectedAppointment|number)}} new state
   */
  static getDerivedStateFromProps(newProps, prevProps) {
    if (newProps.selectedAppointment.id !== prevProps.selectedAppointment.id) {
      return ({ selectedAppointment: newProps.selectedAppointment });
    }
  }

  /**
   * Get the map image using the api
   * @param {Object} appointment the appointment with the map url
   */
  getMapImage(appointment) {
    Api.getMapPicture(store.get('userData'), appointment, this.callback, this.errorCallback);
  }

  /**
   * Callback for the getMapPicture when the api call succes
   * @param {Object} data Data from the api
   */
  callback(data) {
    console.log(data);
    // this.setState({appointments: data});
  }

  /**
   * Error callback for the getMapImage when the api call fails.
   * @param {Object} data error data from the api call
   */
  errorCallback(data) {
    console.log(data);
  }

  /**
   * Main render fucntion
   * @return {JSX} main rendered content
   */
  render() {
    const selectedAppointment = this.state.selectedAppointment;

    console.log(selectedAppointment);

    return (
      <Paper>
        <Grid container>
          <Grid item xs={4}>
            {selectedAppointment && <AppointmentOwner stakeholder={selectedAppointment.stakeholders[0]} />}
            <AppointmentNote text={selectedAppointment.description} />
          </Grid>
          <Grid item xs={4}>
            {selectedAppointment && <AppointmentOwner stakeholder={selectedAppointment.stakeholders[1]} />}
            <AppointmentActions selectedAppointment={selectedAppointment}/>
          </Grid>
          <Grid item xs={4}>
            <AppointmentMap selectedAppointment={selectedAppointment} />
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

export default AppointmentContent;

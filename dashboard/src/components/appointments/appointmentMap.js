/* eslint-disable max-len */
import React, { Component } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';
import Api from '../../lib/api';
import { createStyles, withStyles, Dialog, DialogTitle, DialogContent, DialogActions, Button } from '@material-ui/core';
import PaperComponent from './PaperComponent';

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const Store = window.require('electron-store');
const store = new Store();


const useStyles = createStyles((theme) => ({
  customerName: {
    width: '100%',
    marginBottom: 10,
  },
  dialog: {
    maxWidth: 700,
  },
  mainDocument: {
    height: '19vh',
    marginTop: 18,
    marginRight: 20,
    border: '2px solid #eee',
    borderRadius: 3,
    overflow: 'hidden',
  },
}));

/**
 * Component to render the map on the appointmentContent
 */
class AppointmentMap extends Component {

  /**
   * Constructor
   * @param {Object} props the props
   */
  constructor(props) {
    super(props);

    this.state = {
      open: false,
    };

    this.handleClose = this.handleClose.bind(this);
    this.handleOpen = this.handleOpen.bind(this);

  }

  /**
   * Close the dialog
   */
  handleClose() {
    this.setState({open: false});
  }

  /**
   * Open the dialog
   */
  handleOpen() {
    this.setState({open: true});
  }
  /**
   * Main render
   * @return {JSX} main render
   */
  render() {
    const { classes } = this.props;

    return (
      <div>
        {this.props.selectedAppointment.mapFile && <Document onClick={this.handleOpen} className={classes.mainDocument}
          file={{ url: `${Api.apiUrl}/download/file/${this.props.selectedAppointment.mapFile}`, httpHeaders: { 'accept-encoding': '*', 'Authorization': `${store.get('userData').token}`, 'id': this.props.selectedAppointment.id } }}
        >
          <Page pageNumber={1} />
        </Document>}
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          PaperComponent={PaperComponent}
          aria-labelledby="draggable-dialog-title"
        >
          <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
            Planbestand
          </DialogTitle>
          <DialogContent>
            {this.props.selectedAppointment.mapFile && <Document onClick={this.handleOpen}
              file={{ url: `${Api.apiUrl}/download/file/${this.props.selectedAppointment.mapFile}`, httpHeaders: { 'accept-encoding': '*', 'Authorization': `${store.get('userData').token}`, 'id': this.props.selectedAppointment.id } }}
            >
              <Page pageNumber={1} />
            </Document>}
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Sluiten
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(useStyles)(AppointmentMap);

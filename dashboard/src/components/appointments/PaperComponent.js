import React, { Component } from 'react';
import Draggable from 'react-draggable';
import { Paper, withStyles, createStyles } from '@material-ui/core';

const useStyles = createStyles((theme) => ({
  dialog: {
    maxWidth: 700,
  },
}));
/**
 * Paper component to make dialog draggable
 */
class PaperComponent extends Component {
  /**
     * The main render
     * @return {JSX} main conent
     */
  render() {
    return (
      <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
        <Paper {...this.props} className={this.props.width ? this.props.width : this.props.classes.dialog}/>
      </Draggable>
    );
  }
}

export default withStyles(useStyles)(PaperComponent);

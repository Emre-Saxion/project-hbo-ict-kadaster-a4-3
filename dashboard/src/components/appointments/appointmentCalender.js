import React from 'react';
import { Paper, createStyles, withStyles } from '@material-ui/core';
import { Calendar } from '@fullcalendar/core';
import FullCalendar from '@fullcalendar/react';
import resourceTimelinePlugin from '@fullcalendar/resource-timeline';


const useStyles = createStyles((theme) => ({
  paper: {
    marginTop: 20,
  },
}));

/**
 * Appointment calender class;
 */
class AppointmentCalender extends React.Component {

  /**
     * constructor
     * @param {*} props  the props
     */
  constructor(props) {
    super(props);
  }

  /**
   * The render
   * @return {JSX} jsx
   */
  render() {
    const {classes} = this.props;

    return (
     <Paper className={classes.paper}>
        {/* <FullCalendar
          plugins={[ resourceTimelinePlugin ]}
        /> */}
     </Paper>
    );
  }
}

export default withStyles(useStyles)(AppointmentCalender);

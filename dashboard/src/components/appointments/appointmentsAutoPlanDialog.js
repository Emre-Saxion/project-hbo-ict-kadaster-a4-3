import React, { Component } from 'react';
import {withStyles, createStyles, Dialog, DialogTitle, DialogContentText, DialogContent, DialogActions, Button } from '@material-ui/core';

import PaperComponent from './PaperComponent';
const electron = window.require('electron');

const useStyles = createStyles((theme) => ({
  appointmentListHeader: {
    fontSize: 13,
  },
  DialogActions: {
    marginLeft: 50,
    marginRight: 50,
    display: 'flex',
    justifyContent: 'space-between',
  },
  cancelButton: {
    'paddingTop': 22,
    'paddingBottom': 22,
    'paddingLeft': 44,
    'paddingRight': 44,
    'backgroundColor': 'rgba(0,0,0,0.12)',
    '&:hover': {
      backgroundColor: 'rgba(0,0,0,0.24)',
    },
  },
  upgradeButton: {
    'paddingTop': 22,
    'paddingBottom': 22,
    'paddingLeft': 44,
    'paddingRight': 44,
    'backgroundColor': '#16aa16',
    'color': '#fff',
    'overflow': 'hidden',
    '&:hover': {
      'backgroundColor': '#16aa16',
      '&::before': {
        width: 180,
        height: 180,
      },
    },
    '&::before': {
      content: '""',
      width: 120,
      height: 120,
      borderRadius: '50%',
      position: 'absolute',
      backgroundColor: '#248524',
      transition: 'all 0.2s ease-in-out',
    },
    '& $span': {
      zIndex: 1,
    },
  },
}));

/**
 * AppointmentsAutoPlan for planning in the appointment
 */
class AppointmentsAutoPlan extends Component {

  /**
   * Constructor
   * @param {Object} props the props
   */
  constructor(props) {
    super(props);

    this.state = {
      selectedAppointment: undefined,
    };

    this.openPaypal = this.openPaypal.bind(this);

  }

  /**
   * Open paypal, just a joke
   */
  openPaypal() {
    electron.shell.openExternal('https://www.paypal.me/ThomasStoopendaal');
  }

  /**
   * Main render
   * @return {JSX} main content
   */
  render() {
    const { classes } = this.props;

    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.handleClose}
        PaperComponent={PaperComponent}
        aria-labelledby="draggable-dialog-title"
      >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          Automatisch inplannen
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            Helaas is de licentie niet toereikend. Automatisch inplannen
            is ondersteund vanaf het licentieniveau Pro
            <br></br><br></br>
            Upgrade nu je licentie om direct aan de slag te gaan.
          </DialogContentText>
        </DialogContent>
        <DialogActions className={classes.DialogActions}>
          <Button autoFocus onClick={this.props.handleClose} color="primary" className={classes.cancelButton}>
            Cancel
          </Button>
          <Button color="primary" className={classes.upgradeButton} onClick={this.openPaypal}>
            Subscribe
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(useStyles)(AppointmentsAutoPlan);

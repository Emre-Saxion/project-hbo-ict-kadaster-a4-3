import React, { Component } from 'react';
import { Paper, Typography, Divider, List, withStyles, createStyles } from '@material-ui/core';
import AppointmentsListItem from '../../components/appointments/appointmentsListItem';
import AutoPlanDialog from '../../components/appointments/appointmentsAutoPlanDialog';

const useStyles = createStyles((theme) => ({
  titleContainer: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  appointmentListHeader: {
    fontSize: 20,
    paddingTop: 5,
    paddingLeft: 16,
    fontWeight: 700,
    display: 'inline-block',
  },
  automatePlanning: {
    fontSize: 14,
    fontWeight: 500,
    display: 'inline-block',
    paddingTop: 10,
    paddingRight: 16,
    paddingLeft: 20,
    color: '#5d00ff',
    cursor: 'pointer',
  },
  mainPaper: {
    height: '100%',
  },
}));

/**
 * Component to render the list with appointments
 */
class AppointmentsList extends Component {

  /**
   * Constructor
   * @param {Object} props the props
   */
  constructor(props) {
    super(props);

    this.state = {
      selectedAppointment: undefined,
      autoPlanDialog: false,
    };

    this.closeAutoPlanDialog = this.closeAutoPlanDialog.bind(this);
  }

  /**
   * Close auto plan dialog
   */
  closeAutoPlanDialog() {
    this.setState({autoPlanDialog: false});
  }

  /**
   * Main render
   * @return {JSX} main content
   */
  render() {
    const { classes } = this.props;

    return (
      <Paper className={classes.mainPaper}>
        <div className={classes.titleContainer}>
          <Typography className={classes.appointmentListHeader}>Afspraken</Typography>
          <Typography className={classes.automatePlanning} onClick={() => this.setState({autoPlanDialog: true})}>Automatisch inplannen</Typography>
          <AutoPlanDialog open={this.state.autoPlanDialog} handleClose={() => this.closeAutoPlanDialog()}/>
        </div>
        <Divider/>
        <List>
          {this.props.appointments && this.props.appointments.map(function(appointment) {
            // eslint-disable-next-line no-console
            console.log(appointment);
            return (
              <div>
                <AppointmentsListItem appointment={appointment} selectedAppointment={this.props.selectedAppointment}/>
                <Divider/>
              </div>

            );
          }.bind(this))}


        </List>
      </Paper>
    );
  }
}

export default withStyles(useStyles)(AppointmentsList);

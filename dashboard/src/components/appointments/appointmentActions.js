import React, { Component } from 'react';
import { Paper, Typography, withStyles, createStyles } from '@material-ui/core';
import EventAvailableOutlinedIcon from '@material-ui/icons/EventAvailableOutlined';
import AppointmentPlan from './appointmentPlan';

const useStyles = createStyles((theme) => ({
  paperWrapper: {
    paddingLeft: 20,
    paddingRight: 20,
  },
  mainPaper: {
    paddingTop: 18.5,
    paddingBottom: 18.5,
    paddingRight: 14,
    paddingLeft: 14,
    height: '17vh',
  },
  mainTitle: {
    fontWeight: 600,
    fontSize: 18,
  },
  iconList: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  listItem: {
    width: '25%',
    padding: '5%',
  },
  listItemContent: {
    display: 'flex',
    justifyContent: 'center',
  },
  icon: {
    width: '100%',
    fontSize: '2.5rem',
  },
}));
/**
 * Component for actions
 */
class AppointmentActions extends Component {

  /**
   * Constructor
   * @param {Object} props the props
   */
  constructor(props) {
    super(props);

    this.state = {
      dialogOpen: false,
    };

    this.closeDialog = this.closeDialog.bind(this);
  }

  /**
   * Close the appointment dialog
   */
  closeDialog() {
    this.setState({dialogOpen: false});
  }
  /**
   * Component render
   * @return {JSX} main content
   */
  render() {
    const {classes} = this.props;
    return (
      <div className={classes.paperWrapper}>
        <Paper className={classes.mainPaper}>
          <Typography className={classes.mainTitle}>Acties</Typography>
          <div className={classes.iconList}>
            <div className={classes.listItem}>
              <div className={classes.listItemContent}>
                <EventAvailableOutlinedIcon className={classes.icon} onClick={() => this.setState({dialogOpen: true})}/>
                <AppointmentPlan selectedAppointment={this.props.selectedAppointment} open={this.state.dialogOpen} handleClose={() => this.closeDialog()}/>
              </div>
            </div>
          </div>
        </Paper>
      </div>
    );
  }
}

export default withStyles(useStyles)(AppointmentActions);

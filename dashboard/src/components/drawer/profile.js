/* eslint-disable no-console */
import React from 'react';
import { createStyles, withStyles, Typography } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import avatar from '../../assets/img/Avatar.png';
import Api from '../../lib/api';
const Store = window.require('electron-store');
const store = new Store();

const useStyles = createStyles((theme) => ({
  mainProfileDiv: {
    paddingTop: 30,
    paddingLeft: 34,
    paddingBottom: 20,
  },
  employeeName: {
    fontSize: 20,
    lineHeight: 1.3,
    letterSpacing: 0.26,
    color: '#000',
    marginTop: 4,
    fontWeight: 500,
    fontFamily: 'Roboto',
  },
  empolyeeId: {
    fontSize: 14,
    fontFamily: 'Roboto',
    lineHeight: 1.36,
    letterSpacing: 0.24,
    color: 'rgba(0,0,0,0.6)'
  },
  profileAvater: {
    width: 48,
    height: 48,
    borderRadius: '50%',
  }
}));

/**
 * Com to render the profile info in the main navigation
 */
class DrawerProfile extends React.Component {
  /**
   * Constructor
   * @param {Object} props React props
   * @version 1.0.0
   */
  constructor(props) {
    super(props);

    this.state = {
      userData: undefined,
      profileImage: '',
    };

    this.getProfilePicture = this.getProfilePicture.bind(this);
    this.callback = this.callback.bind(this);
    this.errorCallback = this.errorCallback.bind(this);
  }

  /**
   * When the component first loads
   */
  componentDidMount() {
    this.setState({userData: store.get('userData')}, this.getProfilePicture());
  }

  /**
   * Get the profile picture of the user.
   */
  getProfilePicture() {
    Api.getProfilePicture(store.get('userData'), this.callback, this.errorCallback);

  }

  /**
   * The callback for the user profile data
   * @param {Objec} data Api data
   */
  callback(data) {
    console.log(data);
    this.setState({profileImage: data});
  }

  /**
   * Callback for when the api call fails
   */
  errorCallback() {

  }


  /**
   * React render
   * @version 1.0.0
   * @return {JSX} the render
   */
  render() {
    const {classes} = this.props;
    // const open = true;


    return (
      <div className={classes.mainProfileDiv}>
        {this.state.profileImage ? <img src={this.state.profileImage} alt="avatar" className={classes.profileAvater}/> : <Skeleton variant="circle" width={48} height={48} />}
        {this.state.userData ? <Typography className={classes.employeeName}>{this.state.userData.firstName} {this.state.userData.lastName}</Typography> : ''}
        {this.state.userData ? <Typography className={classes.empolyeeId}>#{this.state.userData.loginCode}</Typography> : ''}
      </div>
    );
  }
}

export default withStyles(useStyles)(DrawerProfile);

import React from 'react';
import { createStyles, withStyles, Fab } from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';

const useStyles = createStyles((theme) => ({

}));

/**
 * Component to render the main button
 */
class DrawerSlideButton extends React.Component {

  /**
   * Constructor
   * @param {Object} props the props
   */
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  /**
   * Main render
   * @return {JSX} main content
   */
  render() {

    return (
            !this.props.open ? <Fab color="primary" aria-label="add" onClick={this.props.handlehandleDrawerOpen}><ArrowBackIosIcon/></Fab> : <Fab color="primary" aria-label="add" onClick={this.props.handlehandleDrawerClose}><ArrowForwardIosIcon/></Fab>
    );
  }
}

export default DrawerSlideButton;

import React, { Component } from 'react';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button, createStyles, withStyles } from '@material-ui/core';

const useStyles = createStyles((theme) => ({
  DialogActions: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  cancelButton: {
    'paddingTop': 22,
    'paddingBottom': 22,
    'paddingLeft': 44,
    'paddingRight': 44,
    'backgroundColor': 'rgba(0,0,0,0.12)',
    '&:hover': {
      backgroundColor: 'rgba(0,0,0,0.24)',
    },
  },
  logoutButton: {
    'paddingTop': 22,
    'paddingBottom': 22,
    'paddingLeft': 44,
    'paddingRight': 44,
    'backgroundColor': '#BE1D1D',
    'color': '#fff',
    'overflow': 'hidden',
    '&:hover': {
      'backgroundColor': '#BE1D1D',
      '&::before': {
        width: 180,
        height: 180,
      },
    },
    '&::before': {
      content: '""',
      width: 120,
      height: 120,
      borderRadius: '50%',
      position: 'absolute',
      backgroundColor: '#E50808',
      transition: 'all 0.2s ease-in-out',
    },
    '& $span': {
      zIndex: 1,
    },
  },
}));
/**
 * Component to render the logout dialog
 */
class LogoutDialog extends Component {
  /**
   * Main render
   * @return {JSX} main content
   */
  render() {
    const {classes} = this.props;
    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Uitloggen</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
                    Weet je zeker dat je wilt uitloggen?
          </DialogContentText>
        </DialogContent>
        <DialogActions className={classes.DialogActions}>
          <Button onClick={this.props.handleClose} color="primary" className={classes.cancelButton}>
                    Annuleren
          </Button>
          <Button onClick={this.props.logout} color="primary" autoFocus className={classes.logoutButton}>
                    Uitloggen
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default withStyles(useStyles)(LogoutDialog);

import React from 'react';
import useStyles from './style';
import { withStyles } from '@material-ui/core';

/**
 * Default page after login
 */
class Frame extends React.Component {
  /**
   * Constructor
   * @param {Object} props React props
   * @version 1.0.0
   */
  constructor(props) {
    super(props);

    this.state = {
      open: true,
    };
  }

  /**
   * React render
   * @version 1.0.0
   * @return {JSX} the render
   */
  render() {
    return (
      <div>
      </div>
    );
  }
}

export default withStyles(useStyles)(Frame);

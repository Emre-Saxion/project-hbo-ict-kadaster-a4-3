const isDev = window.require('electron-is-dev');

const pdf2base64 = require('pdf-to-base64');

/**
 * Make all the api calls to the main database
 */
class Api {
  /**
   * Get api url.
   */
  get apiUrl() {
    return isDev ? 'http://localhost:8080' : 'http://84.241.175.139:8080';
  }

  /**
   * Call for the login api request
   * @param {int} userCode usercode
   * @param {String} password user password
   * @param {Function} callback callback when the call sucess
   * @param {Function} errorCallback callback when the call fails
   */
  loginRequest(userCode, password, callback, errorCallback) {
    const data = {
      'loginCode': `${userCode}`,
      'password': `${password}`,
    };

    fetch(`${this.apiUrl}/employee/login`, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    })
        .then((response) => response.json())
        .then((data) => {
          if (data.status >= 400) {
            throw new Error();
          }
          callback(data);
        })
        .catch(function(err) {
          errorCallback(err);
        });
  }

  /**
   * Get all the appointments that are not planned
   * @param {Object} userData the userdata
   * @param {*} callback call when succes
   * @param {*} errorCallback call when fail
   */
  getAppointments(userData, callback, errorCallback) {

    fetch(`${this.apiUrl}/admin/appointments/unplanned`, {
      method: 'GET',
      headers: {
        'Authorization': `${userData.token}`,
      },
    })
        .then((response) => response.json())
        .then((data) => {
          if (data.status >= 400) {
            throw new Error();
          }
          callback(data);
        })
        .catch(function(err) {
          errorCallback(err);
        });
  }

  /**
   * Get the map pdf and encode it to base64
   * @param {*} userData the userdata for the logged in user
   * @param {*} appointment the selected appointment
   * @param {*} callback call when succes
   * @param {*} errorCallback call when fails
   */
  getMapPicture(userData, appointment, callback, errorCallback) {

    pdf2base64(`${this.apiUrl}/download/file/${appointment.mapFile}`, {
      method: 'GET',
      headers: {
        'Authorization': `${userData.token}`,
        'id': appointment.id,
      },
    })
        .then((blob) => {
          callback(blob);
        })
        .catch(function(err) {
          errorCallback(err);
        });
  }

  /**
   * Call for getting the profile image
   * @param {Object} userData the logged in user
   * @param {*} callback call when data sucees
   * @param {*} errorCallback call whendata fails
   */
  getProfilePicture(userData, callback, errorCallback) {

    fetch(`${this.apiUrl}/download/profile/${userData.loginCode}`, {
      method: 'GET',
      headers: {
        'Authorization': `${userData.token}`,
      },
    })
        .then((res) => {
          return res.blob();
        })
        .then((blob) => {
          const img = URL.createObjectURL(blob);
          callback(img);
        })
        .catch(function(err) {
          errorCallback(err);
        });
  }

  /**
   * Call for getting all employees of the organisation
   * @param {Object} userData the logged in user
   * @param {*} callback call when data sucees
   * @param {*} errorCallback call whendata fails
   */
  getEmployees(userData, callback, errorCallback) {

    fetch(`${this.apiUrl}/admin/employees/all`, {
      method: 'GET',
      headers: {
        'Authorization': `${userData.token}`,
      },
    })
        .then((response) => response.json())
        .then((data) => {
          if (data.status >= 400) {
            throw new Error();
          }
          callback(data);
        })
        .catch(function(err) {
          errorCallback(err);
        });
  }

  /**
   * Save the appointment
   * @param {*} employee the employee id
   * @param {*} appointment The appointment
   * @param {*} userData the userdata of logged in user
   * @param {*} callback the callback
   * @param {*} errorCallback error callback
   */
  saveAppointmentEmployee(employee, appointment, userData, callback, errorCallback) {
    fetch(`${this.apiUrl}/admin/employees/all`, {
      method: 'POST',
      headers: {
        'Authorization': `${userData.token}`,
      },
    })
        .then((response) => response.json())
        .then((data) => {
          if (data.status >= 400) {
            throw new Error();
          }
          callback(data);
        })
        .catch(function(err) {
          errorCallback(err);
        });
  }

  /**
   * 
   * @param {*} userData 
   * @param {*} callback 
   * @param {*} errorCallback 
   */
  getProfilePicture(userData,callback, errorCallback) {
         
    fetch(`${this.apiUrl}/download/profile/${userData.loginCode}`, {
        method: 'GET',
        headers: {
          'Authorization': `${userData.token}`
        }
      })
          .then(res =>{return res.blob()})
          .then((blob) => {
            let img = URL.createObjectURL(blob);
            callback(img);
          })
          .catch(function(err) { 
              errorCallback(err);
          });
  }

  /**
   * 
   * @param {*} userData 
   * @param {*} stakeholders 
   * @param {*} note 
   * @param {*} appointmentDate 
   * @param {*} parcel 
   * @param {*} callback 
   * @param {*} errorCallback 
   */
  createAppointment(userData, stakeholders, note, appointmentDate, parcel, callback, errorCallback) {

    const data = {
      'stakeholders': `${stakeholders}`,
      'note': `${note}`,
      'appointmentDate': `${appointmentDate}`,
      'parcel': `${parcel}`,
    };

    fetch(`${this.apiUrl}/admin/appointment/create`, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Authorization': `${userData.token}`,
          'Content-Type': 'application/json'
        }
      })
          .then(res =>{return res.blob()})
          .then((blob) => {
            let img = URL.createObjectURL(blob);
            callback(img);
          })
          .catch(function(err) { 
            errorCallback(err);
          });
  }


  

}

export default (new Api());

import React, {useState} from 'react';
import Login from '../login';
import Home from '../home';
import Frame from '../../components/frame/';

/**
 * Main component call
 * @return {JSX} the app render
 */
export default function App() {

  const [loggedIn, setloggedIn] = useState(false);

  /**
   * Logout the user
   */
  function logoutUser() {
    setloggedIn(false);
  }

  return (
    <div>
      <Frame/>

      <div>
        {loggedIn ?
        <Home logout={() => logoutUser()}/> :
        // eslint-disable-next-line max-len
        <Login logout={() => logoutUser()} updateLogin={() => setloggedIn(true)} />}
      </div>
    </div>
  );

}

import React, { Component } from 'react';
import AppointmentList from '../../components/appointments/appointmentsList';
import { Grid, createStyles, withStyles, Fab, Dialog, DialogTitle, DialogContent, DialogContentText, Typography, FormControl, InputLabel, Select, DialogActions, Button } from '@material-ui/core';
import Api from '../../lib/api';
import AppointmentContent from '../../components/appointments/appointmentContent';
import AppointmentCalender from '../../components/appointments/appointmentCalender';
import PaperComponent from '../../components/appointments/PaperComponent';

const Store = window.require('electron-store');
const store = new Store();

const useStyles = createStyles((theme) => ({
  rightContent: {
  },
  fabAdd: {
    position: 'absolute',
    right: 20,
    bottom: 20,
  }
}));

/**
 * Render the appointments page
 */
class Appointments extends Component {

  /**
   * Constructor
   * @param {Object} props the props
   */
  constructor(props) {
    super(props);

    this.state = {
      userData: [],
      appointments: [],
      selectedAppointment: 0,
      addAppointmentModal: false,
    };

    this.callback = this.callback.bind(this);
    this.errorCallback = this.errorCallback.bind(this);
    this.selectedAppointment = this.selectedAppointment.bind(this);
    this.createAppointment = this.createAppointment.bind(this);
    this.errorCallbackCreate = this.errorCallbackCreate.bind(this);
    this.callbackCreate = this.callbackCreate.bind(this);

  }

  /**
   * When the component first renders
   */
  componentDidMount() {
    Api.getAppointments(store.get('userData'), this.callback, this.errorCallback);
    this.createAppointment();
  }

  /**
   * Call when api call succeeds
   * @param {Object} data the api data
   */
  callback(data) {
    this.setState({appointments: data});
  }

  /**
   * When the api call fails
   * @param {Object} data the api data
   */
  errorCallback(data) {
    // eslint-disable-next-line no-console
    console.log(data);
  }

  /**
   * The appointment id
   * @param {Integer} id appointment id
   */
  selectedAppointment(id) {
    // eslint-disable-next-line no-console
    console.log(id);

    this.state.appointments.map(function(app) {
      if (app.id === id) {
        this.setState({selectedAppointment: app});
      }
    }.bind(this));
  }

  createAppointment() {
    Api.createAppointment(store.get('userData'), {}, "test", "2020-05-15", "plaats", this.callbackCreate, this.errorCallbackCreate);
  }

  callbackCreate(data) {
    console.log(data)
  }

  errorCallbackCreate(data) {
    console.log(data);
  }

  /**
   * Main render
   * @return {JSX} main content render
   */
  render() {
    const {classes} = this.props; // import classes and set constants
    return (
      <div>
        <Grid container spacing={5}>
          <Grid item xs={3}>
            <AppointmentList appointments={this.state.appointments} selectedAppointment={this.selectedAppointment}/>
          </Grid>
          <Grid item xs={9} spacing={5} className={classes.rightContent}>
            {this.state.selectedAppointment && <AppointmentContent appointments={this.state.appointments} selectedAppointment={this.state.selectedAppointment}/>}
            <AppointmentCalender/>
          </Grid>
        </Grid>
        <Fab className={classes.fabAdd} onClick={() => this.setState({addAppointmentModal: true})}>+</Fab>
        <Dialog
          open={this.state.addAppointmentModal}
          onClose={() => this.setState({addAppointmentModal: false})}
          PaperComponent={PaperComponent}
          aria-labelledby="draggable-dialog-title"
        >
          <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title" className={classes.mainTitle}>
            Afspraak aanmaken
          </DialogTitle>
          <DialogContent className={classes.content}>
            <DialogContentText>
              test
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={() => this.setState({addAppointmentModal: false})} color="primary" >
              Cancel
            </Button>
            <Button onClick={this.createAppointment} color="primary">
              Opslaan
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(useStyles)(Appointments);

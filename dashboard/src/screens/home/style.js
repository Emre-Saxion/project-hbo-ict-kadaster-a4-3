import {createStyles} from '@material-ui/core';

const styles = createStyles((theme) => ({
  drawerPaper: {
    minWidth: 300,
    overflowY: 'visible',
    visibility: 'visible !important',
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    backgroundColor: 'transparant',
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: 0,
  },
  contentShift: {
    backgroundColor: 'transparant',
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 300,
  },
  fab: {
    position: 'absolute',
    top: 'calc(50% - 24px)',
    left: 273,
  },
}));

export default styles;

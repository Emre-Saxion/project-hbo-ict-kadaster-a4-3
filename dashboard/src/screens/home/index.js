/* eslint-disable max-len */
import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import clsx from 'clsx';
import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';
import {
  CssBaseline,
  Drawer,
  Divider,
  withStyles,
  Fab,
} from '@material-ui/core';

import blueGrey from '@material-ui/core/colors/grey';

import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';

import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

import Navigation from './navigation';
import Appointments from '../appointments';
import Profile from '../../components/drawer/profile';
import DrawerSlideButton from '../../components/drawer/drawerSlideButton';
import useStyles from './style';
import LogoutDialog from '../../components/logoutDialog';

const muiTheme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    type: 'light',
    primary: blueGrey,
    secondary: blueGrey,
  },
});

/**
 * Default page after login
 */
class Home extends React.Component {
  /**
   * Constructor
   * @param {Object} props React props
   * @version 1.0.0
   */
  constructor(props) {
    super(props);

    this.state = {
      open: true,
      logoutDialogOpen: false,
    };

    this.logout = this.logout.bind(this);
    this.handleDrawerOpen = this.handleDrawerOpen.bind(this);
    this.handleDrawerClose = this.handleDrawerClose.bind(this);
    this.handleLogoutDialogClose = this.handleLogoutDialogClose.bind(this);
    this.handleLogoutDialogOpen = this.handleLogoutDialogOpen.bind(this);
  }

  /**
   * Open the Drawer using the react state
   * @version 1.0.0
   */
  handleDrawerOpen() {
    this.setState({
      open: true,
    });
  };

  /**
   * Close the drawer using the react state
   * @version 1.0.0
   */
  handleDrawerClose() {
    this.setState({
      open: false,
    });
  };

  /**
   * Set the prop to log out the user
   */
  logout() {
    this.props.logout();
  }

  /**
   * Close the logout dialog
   */
  handleLogoutDialogClose() {
    this.setState({logoutDialogOpen: false});
  }

  /**
   * Open the logout dialog
   */
  handleLogoutDialogOpen() {
    this.setState({logoutDialogOpen: true});
  }

  /**
   * React render
   * @version 1.0.0
   * @return {JSX} the render
   */
  render() {
    const {classes} = this.props;
    const {open} = this.state;
    // const open = true;

    // eslint-disable-next-line no-console
    console.log(this.state.open);
    console.log(window.location.href);

    return (
      <MuiThemeProvider theme={muiTheme}>
        <Router>
          <div className={classes.root}>
            <CssBaseline />
            <Drawer
              className={classes.drawer}
              variant="persistent"
              anchor="left"
              open={open}
              classes={{
                paper: classes.drawerPaper,
              }}
            >

              <Profile/>
              <Divider />
              <Navigation logout={this.handleLogoutDialogOpen}/>
              {!this.state.open ? <Fab color="primary" aria-label="add" onClick={() => this.handleDrawerOpen()} className={classes.fab}><ArrowForwardIosIcon/></Fab> : <Fab color="primary" aria-label="add" onClick={() => this.handleDrawerClose()} className={classes.fab}><ArrowBackIosIcon/></Fab>}
            </Drawer>
            <LogoutDialog logout={this.props.logout} open={this.state.logoutDialogOpen} handleClose={this.handleLogoutDialogClose}/>

            <main
              className={clsx(classes.content, {
                [classes.contentShift]: open,
              })}
            >
              <Switch>
                <Route exact path="/appointments">
                  <Appointments/>
                </Route>
                <Route exact path="/history">
                </Route>
              </Switch>
            </main>
          </div>
        </Router>
      </MuiThemeProvider>
    );
  }
}

export default withStyles(useStyles)(Home);

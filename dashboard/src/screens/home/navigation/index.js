/* eslint-disable no-unused-vars */
import React from 'react';
import { List, ListItem, ListItemText, Divider } from '@material-ui/core';
import { Link } from 'react-router-dom';
import AssessmentIcon from '@material-ui/icons/Assessment';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import SettingsIcon from '@material-ui/icons/Settings';

const Store = window.require('electron-store');

/**
 * Class that generates the menu items in the left main menu
 */
class Navigation extends React.Component {
  /**
   * Constructor
   * @param {Object} props React.component props
   * @version 1.0.0
   */
  constructor(props) {
    super(props);

    this.logout = this.logout.bind(this);
    this.store = new Store();
  }

  /**
   * Invalidate the JWT token and go to the login page
   */
  logout() {
    this.store.delete('token');
    this.props.logout();
  }

  /**
   * @return {JSX} React components
   */
  render() {
    const routesTop = [
      {
        name: 'Afspraken',
        path: '/appointments',
        icon: AssessmentIcon,
      },
      {
        name: 'Geschiedenis',
        path: '/history',
        icon: AssessmentIcon,
      },
    ];

    const routesMid = [

    ];

    let routesMidBottom;
    // eslint-disable-next-line prefer-const
    routesMidBottom = [

    ];

    // const routesBottom = [
    //
    // ];


    return (
      <div>
        <List>
          {routesTop.map((route, index) => (
            <ListItem button key={index} {...{ to: route.path }} component={Link}>
              <route.icon />
              <ListItemText primary={route.name} />
            </ListItem>
          ))}
        </List>
        <Divider />
        {/* <List>
          {routesMid.map((route, index) => (
            <ListItem button key={index} {...{ to: route.path }} component={Link}>
              <route.icon />
              <ListItemText primary={route.name} />
            </ListItem>
          ))}
        </List> */}
        {/* <Divider /> */}
        {/* <List>
          {routesMidBottom.map((route, index) => (
            <ListItem button key={index} {...{ to: route.path }} component={Link}>
              <route.icon />
              <ListItemText primary={route.name} />
            </ListItem>
          ))}
        </List> */}
        {/* <Divider /> */}
        <List>
          {/* {routesBottom.map((route, index) => (*/}
          {/*  <ListItem button key={index} {...{ to: route.path }} component={Link}>*/}
          {/*    <route.icon />*/}
          {/*    <ListItemText primary={route.name} />*/}
          {/*  </ListItem>*/}
          {/* ))}*/}
          <ListItem button onClick={this.logout}>
            <ExitToAppIcon />
            <ListItemText primary={'Uitloggen'} />
          </ListItem>
        </List>
      </div>
    );
  }
}

export default Navigation;

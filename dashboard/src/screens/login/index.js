/* eslint-disable max-len */
/* eslint-disable no-console */
import React from 'react';
import {
  withStyles,
  Paper,
  TextField,
  Button,
  LinearProgress,
} from '@material-ui/core';
import logo from '../../assets/img/logo.jpg';

import useStyles from './style';

import Api from '../../lib/api.js';

const Store = window.require('electron-store');
const store = new Store();

/**
 * Main login page
 */
class Login extends React.Component {

  /**
   * The constructor
   * @param {Object} props props
   */
  constructor(props) {
    super(props);

    this.state = {
      isloggedIn: false,
      isLoggingIn: false,
      password: '',
      userCode: '',
      isError: false,
    };

    this.login = this.login.bind(this);
    this.changeUsercode = this.changeUsercode.bind(this);
    this.callbackLoginError = this.callbackLoginError.bind(this);
    this.callbackLogin = this.callbackLogin.bind(this);
    this.gotoDashboard = this.gotoDashboard.bind(this);
    this.checkSubmitForm = this.checkSubmitForm.bind(this);
  }

  /**
   * Make the login request and get user data
   */
  login() {
    this.setState({isLoggingIn: true});
    setTimeout(function() {
      Api.loginRequest(this.state.userCode, this.state.password, this.callbackLogin, this.callbackLoginError);
    }.bind(this), 700); // Sleep, otherwise the app is to fast :P Lol //TODO add back delay 700
  }

  /**
   * The api data when success
   * @param {Object} data the api data
   */
  callbackLogin(data) {
    store.set('userData', data);
    this.setState({isloggedIn: true, isLoggingIn: false}, this.gotoDashboard());
  }

  /**
   * Goto the dashboard after a succesfull login
   */
  gotoDashboard() {
    setTimeout(function() {
      this.props.updateLogin();
    }.bind(this), 1400); // TODO add back delay 1400
  }

  /**
   * The call when the api fails
   * @param {Object} error api error
   */
  callbackLoginError(error) {
    console.log(error);
    // this.setState({isloggedIn: true, isLoggingIn: false}, this.gotoDashboard())
    this.setState({isloggedIn: false, isLoggingIn: false, isError: true});
  }

  /**
   * When the user types
   * @param {Int} e The vlaue o the texfield
   */
  changeUsercode(e) {
    this.setState({isError: false});
    if (e.keyCode === 13) {
      this.login();
    } else {
      this.setState({userCode: e.target.value});
    }
  }

  /**
   * Change the field when the user types
   * @param {String} e the password
   */
  changePassword(e) {
    this.setState({isError: false});
    this.setState({password: e.target.value});
  }

  /**
   * Check if thew user pressed enter in the login form
   * @param {Event} e event of the keydown
   */
  checkSubmitForm(e) {
    // this.setState({userCode: 123456, password: 'password'}, this.login());
    if (e.keyCode === 13) {
      this.login();
    }
  }
  /**
   * Mian render
   * @return {JSX} content
   */
  render() {
    const {classes} = this.props;

    return (
      <div className={`${classes.mainDiv} ${this.state.isloggedIn ? classes.fullView : ''}`}>
        <img src={logo} alt="logo" className={classes.logoImg}/>
        <Paper className={classes.loginForm}>
          <TextField error={this.state.isError} id="outlined-basic" label="Inlogcode" variant="outlined" className={classes.loginField} onChange={(event) => this.changeUsercode(event)} onKeyDown={(event) => this.checkSubmitForm(event)}/>
          <TextField error={this.state.isError} id="outlined-basic" label="Wachtwoord" variant="outlined" type="password" className={classes.loginField} onChange={(event) => this.changePassword(event)} onKeyDown={(event) => this.checkSubmitForm(event)}/>
          <Button variant="contained" color="primary" className={classes.loginButton} onClick={() => this.login()}>
            Login
          </Button>
          {this.state.isLoggingIn && <LinearProgress color="primary" className={classes.logginLoading}/>}
        </Paper>
      </div>
    );
  }
}

export default withStyles(useStyles)(Login);

import {createStyles} from '@material-ui/core';

const styles = createStyles((theme) => ({
  mainDiv: {
    '&::before': {
      content: '""',
      display: 'block',
      position: 'absolute',
      height: '70%',
      width: '100%',
      borderRadius: '50%',
      background: '#00889E',
      top: '-44%',
      left: '-30%',
      transition: '1s all ease-in-out',
    },
  },
  fullView: {
    '&::before': {
      height: '100%',
      width: '100%',
      borderRadius: '0%',
      top: 0,
      left: 0,
    },
  },
  loginForm: {
    paddingTop: 50,
    paddingBottom: 50,
    paddingLeft: 150,
    paddingRight: 150,
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%,-50%)',
    border: '1px solid #707070',
  },
  loginField: {
    display: 'block',
    paddingBottom: 15,
  },
  loginButton: {
    width: '100%',
  },
  logoImg: {
    width: '10%',
    position: 'absolute',
    top: 10,
    right: 10,
    border: '2px solid #eee',
    borderRadius: 10,
    padding: 5,
    backgroundColor: '#FFF',
  },
  logginLoading: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    left: 0,
  },
}));

export default styles;
